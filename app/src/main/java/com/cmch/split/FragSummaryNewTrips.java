package com.cmch.split;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.cmch.split.dbref.Trip;
import com.cmch.split.adapters.CardViewTripsAdapter;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by CC on 05-Mar-18.
 */

public class FragSummaryNewTrips extends Fragment {

    private ArrayList<Trip> availableTrips;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

		/* Inflate the layout for this fragment */
        final View view = inflater.inflate(R.layout.frag_summary_new_trips, container, false);
        RecyclerView recyclerView = view.findViewById(R.id.recyclerView_Summary_Trips);

        // use a linear layout manager
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(view.getContext());
        recyclerView.setLayoutManager(layoutManager);
        availableTrips = new ArrayList<>();

        Bundle bundle = getArguments();
        if(bundle != null)
        {
            //availableTrips = bundle.getParcelableArrayList("trips");
            Gson gson = new Gson();
            availableTrips = gson.fromJson(bundle.getString("trips"), new TypeToken<List<Trip>>(){}.getType());
            Log.d("VIAJES", availableTrips.toString());
        }

        // specify an adapter (see also next example)

        RecyclerView.Adapter cardViewTripsAdapter;
        cardViewTripsAdapter = new CardViewTripsAdapter(availableTrips);
        recyclerView.setAdapter(cardViewTripsAdapter);

        return view;
    }


}
