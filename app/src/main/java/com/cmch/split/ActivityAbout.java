package com.cmch.split;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.cmch.split.baseactivity.ActivitySplit;
import com.cmch.split.tools.SplitTools;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.StorageReference;
import com.squareup.picasso.Picasso;

public class ActivityAbout extends ActivitySplit {

    private TextView txbtn_AboutSplit_Web;
    private TextView txbtn_AboutSplit_AppVersion;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);

        txbtn_AboutSplit_Web = findViewById(R.id.txbtn_AboutSplit_Web);
        txbtn_AboutSplit_AppVersion = findViewById(R.id.txbtn_AboutSplit_AppVersion);

        txbtn_AboutSplit_Web.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent();
                intent.setAction(Intent.ACTION_VIEW);
                intent.addCategory(Intent.CATEGORY_BROWSABLE);
                intent.setData(Uri.parse("http://www.s-plit.com"));
                startActivity(intent);
            }
        } );

        txbtn_AboutSplit_AppVersion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(v.getContext(), ActivityVersionApp.class));
            }
        });


    }

}
