package com.cmch.split;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.cmch.split.dbref.User;
import com.cmch.split.tools.EmailAuth;
import com.cmch.split.tools.PhoneAuth;
import com.cmch.split.tools.SimpleAlertDialog;
import com.cmch.split.tools.WaitingDialog;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseException;
import com.google.firebase.FirebaseTooManyRequestsException;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;

import java.util.concurrent.TimeUnit;

/**
 * Created by alex on 13/12/17.
 */

public class ActivityPhoneVerification extends AppCompatActivity implements View.OnClickListener {

    EditText editText_PhoneVerification_Code;
    Button button_PhoneVerificaction_Verify;
    Button button_PhoneVerification_Back;

    private PhoneAuth phoneAuth;
    private static String phoneNumber;
    private static FirebaseUser frbPhoneUser;

    private WaitingDialog waitingDialog;


    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_phone_verification);

        editText_PhoneVerification_Code = (EditText) findViewById(R.id.editText_PhoneVerification_Code);
        button_PhoneVerificaction_Verify = (Button) findViewById(R.id.button_PhoneVerification_Verify);
        button_PhoneVerification_Back = (Button) findViewById(R.id.button_PhoneVerification_Back);

        button_PhoneVerification_Back.setOnClickListener(this);
        button_PhoneVerificaction_Verify.setOnClickListener(this);

        waitingDialog = new WaitingDialog(this);

    }

    @Override
    public void onResume(){
        super.onResume();
        if (phoneAuth == null){
            phoneAuth = new PhoneAuth(this) {
                @Override
                public void OnVerificationCompleted(PhoneAuthCredential phoneAuthCredential) {
                    editText_PhoneVerification_Code.setText(phoneAuthCredential.getSmsCode());
                }

                @Override
                public void OnVerificationFailed(Exception exception) {
                    Toast.makeText(getApplicationContext(), exception.getLocalizedMessage(), Toast.LENGTH_LONG).show();
                }

                @Override
                public void OnCodeSent(String verificationId, PhoneAuthProvider.ForceResendingToken resendingToken) {
                    waitingDialog.Show();
                }

                @Override
                public void OnSignInSucces(@NonNull Task<AuthResult> task) {
                    frbPhoneUser = task.getResult().getUser();
                    waitingDialog.Dismiss();
                    startActivity(new Intent(getApplicationContext(), ActivityRegister.class));
                }

                @Override
                public void OnSignInFailure(Exception exception) {
                    Toast.makeText(getApplicationContext(), exception.getLocalizedMessage(), Toast.LENGTH_LONG);
                }

                int timesResendCode = 0;
                @Override
                public void OnCodeAutoRetrievalTimeOut(String s) {

                    // Solo mandamos 3 verificaciones de telefono como maximo
                    if(timesResendCode <= 2) {
                        Toast.makeText(getApplicationContext(), s.toString(), Toast.LENGTH_LONG);
                        phoneAuth.resendVerificationCode(phoneNumber, phoneAuth.getResendingToken());
                        timesResendCode++;
                    }else
                        Toast.makeText(SplitApp.getAppContext(), "Se agoto los intentos de autenticacion", Toast.LENGTH_LONG).show();
                }
            };
        }
    }



    public static FirebaseUser getFrbPhoneUser(){
        return frbPhoneUser;
    }

    public static String getPhoneNumber(){
        return phoneNumber;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.button_PhoneVerification_Verify:
                phoneNumber = editText_PhoneVerification_Code.getText().toString();
                phoneAuth.sendCode(phoneNumber);
                break;

            case R.id.button_PhoneVerification_Back:
                 finish();
               // new SimpleAlertDialog("Desea agregar un nuevo auto", ActivityAddVehicle, ActivityHome);
                break;
        }
    }


}
