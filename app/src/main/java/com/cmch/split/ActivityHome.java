package com.cmch.split;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.util.Log;

import com.cmch.split.baseactivity.ActivitySplit;

/**
 * Created by Gabriel Polanco on 24/03/18.
 */

public class ActivityHome extends ActivitySplit {

    private static final String TAG = "ActivityHome";

    //Usado para agregar los tabs en el activity
    private static FragmentManager fragmentManagerHome;
    private ViewPager mViewPager;
    private ActivityHome.SectionsPagerAdapter mSectionsPagerAdapter;

    private static final int MY_PERMISSIONS_REQUEST_LOCATION = 1;
    private Context context = this;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        Log.d(TAG, "onCreate");

        if (SplitApp.getFrbAuth().getCurrentUser() == null){
            startActivity(new Intent(getApplicationContext(), ActivityLogin.class));
            Log.d(TAG, "User not logged");
        }else{
            initToolBarAndDrawer(R.id.toolbar_home, R.id.drawer_layout_home, R.id.nav_view_home);

            // Crea un adaptador que regresa el fragmento seleccionado en
            // el tablayout
            mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

            // Configura el view pager con los tabs
            mViewPager = findViewById(R.id.container_viewPager_home);
            mViewPager.setAdapter(mSectionsPagerAdapter);

            //Agrega listeners para el viewPager y el tablayout, esto crea el efecto de toque y deslizamiento.
            TabLayout tabLayout = findViewById(R.id.tabs_home);
            mViewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
            tabLayout.addOnTabSelectedListener(new TabLayout.ViewPagerOnTabSelectedListener(mViewPager));

            checkPermission();

        }
    }

    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
            fragmentManagerHome = fm;
        }

        @Override
        public Fragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.
            // Return a PlaceholderFragment (defined as a static inner class below).
            switch (position){
                case 0:
                    return new FragRootHomeNotifications();
                case 1:
                    return new FragRootHomeNotifications();
                default:
                    return null;
            }
        }

        @Override
        public int getCount() {
            // Show 2 total pages.
            return 2;
        }

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }


    private void checkPermission() {

        if (ContextCompat.checkSelfPermission(context, android.Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            if (ActivityCompat.shouldShowRequestPermissionRationale(ActivityHome.this,
                    android.Manifest.permission.ACCESS_FINE_LOCATION)){
                //Mensaje de porque se necesita el permiso
            }
            else {
                ActivityCompat.requestPermissions(ActivityHome.this,
                        new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_LOCATION);
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_LOCATION:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    //Todo bien
                } else {
                    //Solicionar problema
                }
                return;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        }
    }


    @Override
    protected void onResume() {
        super.onResume();
        Log.d(TAG, "onResume");

        //loadUserData();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        //Quitamos agentes de escucha para los datos del usuario
       // removeEvenListeners();
    }

    public static FragmentManager getFm(){
        return fragmentManagerHome;
    }
}

