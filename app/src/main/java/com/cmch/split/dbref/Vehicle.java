package com.cmch.split.dbref;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.firebase.database.IgnoreExtraProperties;

/**
 * Created by alex on 27/11/17.
 */

@IgnoreExtraProperties
public class Vehicle implements Parcelable {

    private String id;
    private String owner;
    private String brand;
    private String model;
    private String state;
    private String year;
    private String color;
    private String plates;
    private String availableSeats;
    private String insuranceCompany;
    private String insurancePolicyNumber;
    private String serialNumber;

    public Vehicle(){

    }

    public String getId() {
        return id;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getPlates() {
        return plates;
    }

    public void setPlates(String plates) {
        this.plates = plates;
    }

    public String getAvailableSeats() {
        return availableSeats;
    }

    public void setAvailableSeats(String availableSeats) {
        this.availableSeats = availableSeats;
    }

    public String getInsuranceCompany() {
        return insuranceCompany;
    }

    public void setInsuranceCompany(String insuranceCompany) {
        this.insuranceCompany = insuranceCompany;
    }

    public String getInsurancePolicyNumber() {
        return insurancePolicyNumber;
    }

    public void setInsurancePolicyNumber(String insurancePolcyNumber) {
        this.insurancePolicyNumber = insurancePolcyNumber;
    }

    public String getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(String serialNumbre) {
        this.serialNumber = serialNumbre;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.id);
        dest.writeString(this.owner);
        dest.writeString(this.brand);
        dest.writeString(this.model);
        dest.writeString(this.state);
        dest.writeString(this.year);
        dest.writeString(this.color);
        dest.writeString(this.plates);
        dest.writeString(this.availableSeats);
        dest.writeString(this.insuranceCompany);
        dest.writeString(this.insurancePolicyNumber);
        dest.writeString(this.serialNumber);
    }

    protected Vehicle(Parcel in) {
        this.id = in.readString();
        this.owner = in.readString();
        this.brand = in.readString();
        this.model = in.readString();
        this.state = in.readString();
        this.year = in.readString();
        this.color = in.readString();
        this.plates = in.readString();
        this.availableSeats = in.readString();
        this.insuranceCompany = in.readString();
        this.insurancePolicyNumber = in.readString();
        this.serialNumber = in.readString();
    }

    public static final Parcelable.Creator<Vehicle> CREATOR = new Parcelable.Creator<Vehicle>() {
        @Override
        public Vehicle createFromParcel(Parcel source) {
            return new Vehicle(source);
        }

        @Override
        public Vehicle[] newArray(int size) {
            return new Vehicle[size];
        }
    };
}
