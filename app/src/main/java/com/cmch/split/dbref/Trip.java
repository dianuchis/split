package com.cmch.split.dbref;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.firebase.database.IgnoreExtraProperties;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Gabriel Polanco on 18/06/18.
 */


@IgnoreExtraProperties
public class Trip implements Parcelable {

    private String id;
    private User driver;
    private PointLocation origin;
    private PointLocation destination;
    private boolean isFrequent;
    private List<PointLocation> layovers;
    private String type;
    private List<String> date;
    private List<String> time;
    private double price;
    private String options;
    private Vehicle vehicle;
    private int seats;
    private String baggage;
    private int baggagePerPerson;
    private String status;
    private List<User> passengers;
    private boolean inProgress;
    private boolean finished;

    public Trip() {

        //Default values of the trip
        this.status = "not scheduled";
        this.inProgress = false;
        this.finished = false;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public User getDriver() {
        return driver;
    }

    public void setDriver(User driver) {
        this.driver = driver;
    }

    public PointLocation getOrigin() {
        return origin;
    }

    public void setOrigin(PointLocation origin) {
        this.origin = origin;
    }

    public PointLocation getDestination() {
        return destination;
    }

    public void setDestination(PointLocation destination) {
        this.destination = destination;
    }

    public boolean isFrequent() {
        return isFrequent;
    }

    public void setFrequent(boolean frequent) {
        isFrequent = frequent;
    }

    public List<PointLocation> getLayovers() {
        return layovers;
    }

    public void setLayovers(List<PointLocation> layovers) {
        this.layovers = layovers;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public List<String> getDate() {
        return date;
    }

    public void setDate(List<String> date) {
        this.date = date;
    }

    public List<String> getTime() {
        return time;
    }

    public void setTime(List<String> time) {
        this.time = time;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getOptions() {
        return options;
    }

    public void setOptions(String options) {
        this.options = options;
    }

    public Vehicle getVehicle() {
        return vehicle;
    }

    public void setVehicle(Vehicle vehicle) {
        this.vehicle = vehicle;
    }

    public int getSeats() {
        return seats;
    }

    public void setSeats(int seats) {
        this.seats = seats;
    }

    public String getBaggage() {
        return baggage;
    }

    public void setBaggage(String baggage) {
        this.baggage = baggage;
    }

    public int getBaggagePerPerson() {
        return baggagePerPerson;
    }

    public void setBaggagePerPerson(int baggagePerPerson) {
        this.baggagePerPerson = baggagePerPerson;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<User> getPassengers() {
        return passengers;
    }

    public void setPassengers(List<User> passengers) {
        this.passengers = passengers;
    }

    public boolean isInProgress() {
        return inProgress;
    }

    public void setInProgress(boolean inProgress) {
        this.inProgress = inProgress;
    }

    public boolean isFinished() {
        return finished;
    }

    public void setFinished(boolean finished) {
        this.finished = finished;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.id);
        dest.writeParcelable(this.driver, flags);
        dest.writeParcelable(this.origin, flags);
        dest.writeParcelable(this.destination, flags);
        dest.writeByte(this.isFrequent ? (byte) 1 : (byte) 0);
        dest.writeTypedList(this.layovers);
        dest.writeString(this.type);
        dest.writeStringList(this.date);
        dest.writeStringList(this.time);
        dest.writeDouble(this.price);
        dest.writeString(this.options);
        dest.writeParcelable(this.vehicle, flags);
        dest.writeInt(this.seats);
        dest.writeString(this.baggage);
        dest.writeInt(this.baggagePerPerson);
        dest.writeString(this.status);
        dest.writeTypedList(this.passengers);
        dest.writeByte(this.inProgress ? (byte) 1 : (byte) 0);
        dest.writeByte(this.finished ? (byte) 1 : (byte) 0);
    }

    protected Trip(Parcel in) {
        this.id = in.readString();
        this.driver = in.readParcelable(User.class.getClassLoader());
        this.origin = in.readParcelable(PointLocation.class.getClassLoader());
        this.destination = in.readParcelable(PointLocation.class.getClassLoader());
        this.isFrequent = in.readByte() != 0;
        this.layovers = in.createTypedArrayList(PointLocation.CREATOR);
        this.type = in.readString();
        this.date = in.createStringArrayList();
        this.time = in.createStringArrayList();
        this.price = in.readDouble();
        this.options = in.readString();
        this.vehicle = in.readParcelable(Vehicle.class.getClassLoader());
        this.seats = in.readInt();
        this.baggage = in.readString();
        this.baggagePerPerson = in.readInt();
        this.status = in.readString();
        this.passengers = in.createTypedArrayList(User.CREATOR);
        this.inProgress = in.readByte() != 0;
        this.finished = in.readByte() != 0;
    }

    public static final Parcelable.Creator<Trip> CREATOR = new Parcelable.Creator<Trip>() {
        @Override
        public Trip createFromParcel(Parcel source) {
            return new Trip(source);
        }

        @Override
        public Trip[] newArray(int size) {
            return new Trip[size];
        }
    };
}
