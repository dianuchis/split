package com.cmch.split.dbref;

import com.google.firebase.database.IgnoreExtraProperties;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by CC on 14-Feb-18.
 */
@IgnoreExtraProperties
public class Country {

    private String code;
    private String name;
    private List<String> states;

    public Country()
    {
        states = new ArrayList<>();
    }

    public String getCode() { return code; }

    public void setCode(String code) { this.code = code; }

    public String getName() { return name; }

    public void setName(String name) { this.name = name; }

    public List<String> getStates() { return states;}

    public void setStates(List<String> states) { this.states = states; }
}
