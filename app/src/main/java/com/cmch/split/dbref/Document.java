package com.cmch.split.dbref;

import com.google.firebase.database.IgnoreExtraProperties;

/**
 * Created by alex on 15/11/17.
 */

@IgnoreExtraProperties
public class Document {

    private String id;
    private String type;
    private User user;


    public Document(){

    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
