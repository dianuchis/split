package com.cmch.split.dbref;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.firebase.database.IgnoreExtraProperties;

/**
 * Created by alex on 15/11/17.
 */

@IgnoreExtraProperties
public class User implements Parcelable {

    private String id;
    private String fullName;
    private String gender;
    private String birthday;
    private String workplace;
    private String email;
    private String password;
    private String cellphone;
    private String country;
    private String state;
    private String city;
    private String streetAndNumber;
    private String interiorNumber;
    private String zipcode;
    private String neighborhood;
    private String description;
    private String memberSince;
    private String fcmRegisterToken;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public String getWorkplace() {
        return workplace;
    }

    public void setWorkplace(String workplace) {
        this.workplace = workplace;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getCellphone() {
        return cellphone;
    }

    public void setCellphone(String cellphone) {
        this.cellphone = cellphone;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getStreetAndNumber() {
        return streetAndNumber;
    }

    public void setStreetAndNumber(String streetAndNumber) {
        this.streetAndNumber = streetAndNumber;
    }

    public String getInteriorNumber() {
        return interiorNumber;
    }

    public void setInteriorNumber(String interiorNumber) {
        this.interiorNumber = interiorNumber;
    }

    public String getZipcode() {
        return zipcode;
    }

    public void setZipcode(String zipcode) {
        this.zipcode = zipcode;
    }

    public String getNeighborhood() {
        return neighborhood;
    }

    public void setNeighborhood(String neighborhood) {
        this.neighborhood = neighborhood;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getMemberSince() {
        return memberSince;
    }

    public void setMemberSince(String memberSince) {
        this.memberSince = memberSince;
    }

    public String getFcmRegisterToken() {
        return fcmRegisterToken;
    }

    public void setFcmRegisterToken(String fcmRegisterToken) {
        this.fcmRegisterToken = fcmRegisterToken;
    }

    public String asString(){
        String result = null;

        try{
            result = String.format("User {id:%s, fullname: %s, gender: %s}", id, fullName, gender);
        }catch (Exception ex){

        }

        return result;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.id);
        dest.writeString(this.fullName);
        dest.writeString(this.gender);
        dest.writeString(this.birthday);
        dest.writeString(this.workplace);
        dest.writeString(this.email);
        dest.writeString(this.password);
        dest.writeString(this.cellphone);
        dest.writeString(this.country);
        dest.writeString(this.state);
        dest.writeString(this.city);
        dest.writeString(this.streetAndNumber);
        dest.writeString(this.interiorNumber);
        dest.writeString(this.zipcode);
        dest.writeString(this.neighborhood);
        dest.writeString(this.description);
        dest.writeString(this.memberSince);
        dest.writeString(this.fcmRegisterToken);
    }

    public User() {
    }

    protected User(Parcel in) {
        this.id = in.readString();
        this.fullName = in.readString();
        this.gender = in.readString();
        this.birthday = in.readString();
        this.workplace = in.readString();
        this.email = in.readString();
        this.password = in.readString();
        this.cellphone = in.readString();
        this.country = in.readString();
        this.state = in.readString();
        this.city = in.readString();
        this.streetAndNumber = in.readString();
        this.interiorNumber = in.readString();
        this.zipcode = in.readString();
        this.neighborhood = in.readString();
        this.description = in.readString();
        this.memberSince = in.readString();
        this.fcmRegisterToken = in.readString();
    }

    public static final Parcelable.Creator<User> CREATOR = new Parcelable.Creator<User>() {
        @Override
        public User createFromParcel(Parcel source) {
            return new User(source);
        }

        @Override
        public User[] newArray(int size) {
            return new User[size];
        }
    };
}
