package com.cmch.split.dbref;

import com.google.firebase.database.IgnoreExtraProperties;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by CC on 14-Feb-18.
 */
@IgnoreExtraProperties
public class Insurance {

    private String country;
    private List<String> companies;

    public Insurance()
    {
        companies = new ArrayList<>();
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public List<String> getCompanies() {
        return companies;
    }

    public void setCompanies(List<String> companies) {
        this.companies = companies;
    }
}
