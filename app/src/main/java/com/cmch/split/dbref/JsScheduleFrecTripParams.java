package com.cmch.split.dbref;

import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by CC on 23-Mar-18.
 */

public class JsScheduleFrecTripParams {

    private float distance;
    private List<String> origin;
    private List<String> destination;
    private String date;
    private String time;
    private String type;
    //private float timeRange;
    private String timeRange;

    public JsScheduleFrecTripParams(){
        origin = new ArrayList<>();
        destination = new ArrayList<>();
    }

    public float getDistance() {
        return distance;
    }

    public void setDistance(float distance) {
        this.distance = distance;
    }

    public List<String> getOrigin() {
        return origin;
    }

    public void setOrigin(List<String> origin) {
        this.origin = origin;
    }

    public List<String> getDestination() {
        return destination;
    }

    public void setDestination(List<String> destination) {
        this.destination = destination;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    /*public float getTimeRange() {
        return timeRange;
    }

    public void setTimeRange(float timeRange) {
        this.timeRange = timeRange;
    }*/

    public String getTimeRange() {
        return timeRange;
    }

    public void setTimeRange(String timeRange) {
        this.timeRange = timeRange;
    }
}
