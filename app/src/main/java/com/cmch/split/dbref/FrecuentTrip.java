package com.cmch.split.dbref;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by CC on 21-Mar-18.
 */

public class FrecuentTrip implements Parcelable {

    private String id;
    private PointLocation origin;
    private PointLocation destination;
    private String date;
    private String time;
    private String idPassenger;
    private String type;
    private float distance;
    private String price;
    //private float timeRange;
    private String timeRange;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public PointLocation getOrigin() {
        return origin;
    }

    public void setOrigin(PointLocation origin) {
        this.origin = origin;
    }

    public PointLocation getDestination() {
        return destination;
    }

    public void setDestination(PointLocation destination) {
        this.destination = destination;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getIdPassenger() {
        return idPassenger;
    }

    public void setIdPassenger(String idPassenger) {
        this.idPassenger = idPassenger;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public float getDistance() {
        return distance;
    }

    public void setDistance(float distance) {
        this.distance = distance;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

   /* public float getTimeRange() {
        return timeRange;
    }

    public void setTimeRange(float timeRange) {
        this.timeRange = timeRange;
    }*/

    public String getTimeRange() {
        return timeRange;
    }

    public void setTimeRange(String timeRange) {
        this.timeRange = timeRange;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.id);
        dest.writeParcelable(this.origin, flags);
        dest.writeParcelable(this.destination, flags);
        dest.writeString(this.date);
        dest.writeString(this.time);
        dest.writeString(this.idPassenger);
        dest.writeString(this.type);
        dest.writeFloat(this.distance);
        dest.writeString(this.price);
        //dest.writeFloat(this.timeRange);
        dest.writeString(this.timeRange);
    }

    public FrecuentTrip() {
    }

    protected FrecuentTrip(Parcel in) {
        this.id = in.readString();
        this.origin = in.readParcelable(PointLocation.class.getClassLoader());
        this.destination = in.readParcelable(PointLocation.class.getClassLoader());
        this.date = in.readString();
        this.time = in.readString();
        this.idPassenger = in.readString();
        this.type = in.readString();
        this.distance = in.readFloat();
        this.price = in.readString();
        //this.timeRange = in.readFloat();
        this.timeRange = in.readString();
    }

    public static final Parcelable.Creator<FrecuentTrip> CREATOR = new Parcelable.Creator<FrecuentTrip>() {
        @Override
        public FrecuentTrip createFromParcel(Parcel source) {
            return new FrecuentTrip(source);
        }

        @Override
        public FrecuentTrip[] newArray(int size) {
            return new FrecuentTrip[size];
        }
    };
}
