package com.cmch.split.dbref;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

import com.google.firebase.database.IgnoreExtraProperties;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by alex on 16/11/17.
 *
 *
 * jsDirection.put("Direction", textDirection);
 jsDirection.put("Locality", direccion.getLocality());
 jsDirection.put("Locale", direccion.getLocale());
 */


@IgnoreExtraProperties
public class PointLocation implements Parcelable {

    private String latitude;
    private String longitude;
    private String address;
    private String locality;
    private String sublocality;
    private String postalCode;
    private String description;


    public PointLocation(){

    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getLocality() {
        return locality;
    }

    public void setLocality(String locality) {
        this.locality = locality;
    }

    public String getSublocality() {
        return sublocality;
    }

    public void setSublocality(String sublocality) {
        this.sublocality = sublocality;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }


    public JSONObject toJSon(){
        JSONObject result = null;

        try{
            result = new JSONObject();
            result.put("latitude", latitude);
            result.put("longitude", longitude);
            result.put("address", address);
            result.put("locality", locality);
            result.put("sublocality", sublocality);
        }catch (JSONException ex){
            Log.d("PointLocation", ex.getMessage());
        }

        return result;
    }

    public static PointLocation fromString(String string){
        PointLocation result = null;

        try{
            JSONObject jsonObject = new JSONObject(string);
            result = new PointLocation();
            result.setLatitude(jsonObject.getString("latitude"));
            result.setLongitude(jsonObject.getString("longitude"));
            result.setAddress(jsonObject.getString("address"));
            result.setLocality(jsonObject.getString("locality"));
            result.setSublocality(jsonObject.getString("sublocality"));
            result.setDescription(jsonObject.getString("descripcion"));
        }catch (Exception ex){
            Log.d("PointLocation", ex.getMessage());
        }

        return result;
    }

    public String asString(){
        String result = "";
        try{
            result += address; // != null? address + ", " : "";
            //result += sublocality != null? sublocality + ", " : "";
            //result += locality != null? locality : "";

            /*
            JSONObject tmp = toJSon();
            tmp.put("latitude", null);
            tmp.put("longitude", null);
            result = tmp.toString().replace("{", "").replace("}", "");
            */
        }
        catch (Exception ex){
            Log.d("PointLocation", ex.getMessage());
        }
        return result;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.latitude);
        dest.writeString(this.longitude);
        dest.writeString(this.address);
        dest.writeString(this.locality);
        dest.writeString(this.sublocality);
        dest.writeString(this.postalCode);
        dest.writeString(this.description);
    }

    protected PointLocation(Parcel in) {
        this.latitude = in.readString();
        this.longitude = in.readString();
        this.address = in.readString();
        this.locality = in.readString();
        this.sublocality = in.readString();
        this.postalCode = in.readString();
        this.description = in.readString();
    }

    public static final Parcelable.Creator<PointLocation> CREATOR = new Parcelable.Creator<PointLocation>() {
        @Override
        public PointLocation createFromParcel(Parcel source) {
            return new PointLocation(source);
        }

        @Override
        public PointLocation[] newArray(int size) {
            return new PointLocation[size];
        }
    };
}
