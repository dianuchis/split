package com.cmch.split.dbref;

/**
 * Created by CC on 14-Mar-18.
 *
 * Descripción: Esta clase representa un método de pago (tarjeta de crédito,
 *              con la información de contacto del propietario.)
 *
 */

public class PaymentMethod {

    private String cardType;
    private String cardTypeBank;
    private String accountHolder;
    private String cardNumber;
    private String expirationYear;
    private String expirationMonth;
    private String cvv;
    private String streetAndNumber;
    private String interiorNumber;
    private String zipcode;
    private String state;
    private String city;
    private int idImagePayMethod;

    public int getIdImagePayMethod() {
        return idImagePayMethod;
    }

    public void setIdImagePayMethod(int idImagePayMethod) {
        this.idImagePayMethod = idImagePayMethod;
    }

    public PaymentMethod(){ }

    public String getCardType() { return cardType; }

    public void setCardType(String cardType) { this.cardType = cardType; }

    public String getCardTypeBank() { return cardTypeBank; }

    public void setCardTypeBank(String cardTypeBank) { this.cardTypeBank = cardTypeBank; }

    public String getAccountHolder() { return accountHolder; }

    public void setAccountHolder(String accountHolder) { this.accountHolder = accountHolder; }

    public String getCardNumber() { return cardNumber; }

    public void setCardNumber(String cardNumber) { this.cardNumber = cardNumber; }

    public String getExpirationYear() { return expirationYear; }

    public void setExpirationYear(String expirationYear) { this.expirationYear = expirationYear; }

    public String getExpirationMonth() { return expirationMonth; }

    public void setExpirationMonth(String expirationMonth) { this.expirationMonth = expirationMonth; }

    public String getCvv() { return cvv; }

    public void setCvv(String cvv) { this.cvv = cvv; }

    public String getStreetAndNumber() { return streetAndNumber; }

    public void setStreetAndNumber(String streetAndNumber) { this.streetAndNumber = streetAndNumber; }

    public String getInteriorNumber() { return interiorNumber; }

    public void setInteriorNumber(String interiorNumber) { this.interiorNumber = interiorNumber; }

    public String getZipcode() { return zipcode; }

    public void setZipcode(String zipcode) { this.zipcode = zipcode; }

    public String getState() { return state; }

    public void setState(String state) { this.state = state; }

    public String getCity() { return city; }

    public void setCity(String city) { this.city = city; }
}
