package com.cmch.split;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.os.Bundle;
import android.util.AttributeSet;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.cmch.split.baseactivity.BaseActivity;
import com.cmch.split.tools.EmailAuth;
import com.cmch.split.tools.WaitingDialog;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;


public class ActivityLogin extends BaseActivity implements View.OnClickListener {

    private static final int MY_PERMISSIONS_REQUEST_LOCATION = 1;

    Button btn_Login_Login;
    Button btn_Login_Register;
    EditText editText_Login_User;
    EditText editText_Login_Pasword;
    WaitingDialog waitingDialog;
    Context context = this;
    View thisView;
    ProgressBar progressBar_login;

    private EmailAuth emailAuth;

    @Override
    public View onCreateView(View parent, String name, Context context, AttributeSet attrs) {
        View v = super.onCreateView(parent, name, context, attrs);
        thisView = v;
        return v;

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        editText_Login_User = (EditText)findViewById(R.id.editText_Login_User);
        editText_Login_Pasword = (EditText) findViewById(R.id.editText_Login_Password);
        btn_Login_Login = (Button)findViewById(R.id.btn_Login_Login);
        btn_Login_Register = (Button)findViewById(R.id.btn_Login_Register);
        progressBar_login = (ProgressBar) findViewById(R.id.progressBar_Login);

        btn_Login_Login.setOnClickListener(this);
        btn_Login_Register.setOnClickListener(this);
        waitingDialog = new WaitingDialog(this);
    }

    @Override
    public void onResume(){
        super.onResume();

        if (emailAuth == null){
            emailAuth = new EmailAuth(this) {

                @Override
                public void OnSuccess(@NonNull Task<AuthResult> task) {
                    progressBar_login.setVisibility(View.VISIBLE);
                    loadUserData();
                }

                @Override
                public void OnFailure(Exception exception) {
                    Toast.makeText(getApplicationContext(), "Please verify the email and pass", Toast.LENGTH_LONG).show();
                }
            };
        }
    }


    private boolean validateEmailAndPass(String email, String password){
        boolean isRegistered = false;

        //verificamos que el correo y contraseña
        //sean los mismos que ingreso el usuario.
        if(email.equals( editText_Login_User.getText().toString())
                && password.equals(editText_Login_Pasword.getText().toString())){
            isRegistered = true;
        }
        return isRegistered;
    }


    private void checkPermission() {
        if (ContextCompat.checkSelfPermission(getApplicationContext(), android.Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            if (ActivityCompat.shouldShowRequestPermissionRationale(ActivityLogin.this,
                    android.Manifest.permission.ACCESS_FINE_LOCATION)){
                //Mensaje de porque se necesita el permiso
            }
            else {
                ActivityCompat.requestPermissions(ActivityLogin.this,
                        new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_LOCATION);
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_LOCATION:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    //Todo bien
                } else {
                    //Solicionar problema
                }
                return;

            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btn_Login_Login:
                String email = editText_Login_User.getText().toString();
                String password = editText_Login_Pasword.getText().toString();
                if (!email.isEmpty() && !password.isEmpty()){
                    emailAuth.signInWithEmail(email, password);

                }else{
                    String message = getResources().getString(R.string.toast_Login_Empty);
                    Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
                }
                break;

            case R.id.btn_Login_Register:
                startActivity(new Intent(getApplicationContext(), ActivityRegister.class));
                break;
        }
    }
}
