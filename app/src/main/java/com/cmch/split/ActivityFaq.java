package com.cmch.split;

import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ExpandableListAdapter;
import android.widget.ExpandableListView;


/**
 * Created by CC on 10-Mar-18.
 */


import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

public class ActivityFaq extends AppCompatActivity {

    ExpandableListAdapter listAdapter;
    ExpandableListView expListView;
    List<String> listDataHeader;
    HashMap<String, List<String>> listDataChild;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_faq);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        // get the listview
        expListView = findViewById(R.id.FAQExpandable);

        // preparing list data
        prepareListData();

        listAdapter = new com.cmch.split.adapters.ExpandableListAdapter(this, listDataHeader, listDataChild);

        // setting list adapter
        expListView.setAdapter(listAdapter);
    }

    /*
     * Preparing the list data
     */

    private void prepareListData() {

        String FAQ1 = getString(R.string.lbl_FAQ_Q1);
        String FAQ2 = getString(R.string.lbl_FAQ_Q2);
        String FAQ3 = getString(R.string.lbl_FAQ_Q3);
        String FAQ4 = getString(R.string.lbl_FAQ_Q4);
        String FAQ5 = getString(R.string.lbl_FAQ_Q5);
        String FAQ6 = getString(R.string.lbl_FAQ_Q6);
        String FAQ7 = getString(R.string.lbl_FAQ_Q7);
        String FAQ8 = getString(R.string.lbl_FAQ_Q8);
        String FAQ9 = getString(R.string.lbl_FAQ_Q9);

        listDataHeader = new ArrayList<String>();
        listDataChild = new HashMap<String, List<String>>();

        // Adding child data
        listDataHeader.add(FAQ1);
        listDataHeader.add(FAQ2);
        listDataHeader.add(FAQ3);
        listDataHeader.add(FAQ4);
        listDataHeader.add(FAQ5);
        listDataHeader.add(FAQ6);
        listDataHeader.add(FAQ7);
        listDataHeader.add(FAQ8);
        listDataHeader.add(FAQ9);

        Iterator ldh = listDataHeader.iterator();
        while (ldh.hasNext()){
            listDataChild.put(ldh.next().toString(),listDataHeader);
        }
    }
}

