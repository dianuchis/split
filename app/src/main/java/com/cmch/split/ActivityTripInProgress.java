package com.cmch.split;
import android.*;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.telephony.SmsManager;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.cmch.split.services.LocationService;
import com.cmch.split.tools.ActivityMap;
import com.google.android.gms.maps.model.LatLng;

import org.w3c.dom.Text;


public class ActivityTripInProgress extends ActivityMap {

    private final static String TAG = "ActivityTripInProgress";
    Button EmergencyButton;
    Button ChangeNumbers;
    String message01 = "EMERGENCY Loc: ";
    String message02 = "Driver: ";
    String prefix = "tel:";
    TextView EmergencyPhone;
    TextView PhoneforLocation;
    TextView txtviewcurrlocation;
    SharedPreferences sharedPreferences;

    //Falta obtener el nombre del conductor
    String drivername = "Francisco Lopez";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //Transparenta la barra de notificacion
        //si el SO android es una version igual o mayor a KitKat
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            getWindow().getDecorView().setSystemUiVisibility(
                    View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                            | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
            getWindow().setStatusBarColor(getResources().getColor(R.color.gradient_gray));
        }
        setContentView(R.layout.activity_trip_in_progress);

        //Pasamos el id del fragmento que contiene el mapa de Google Maps para trazar la ruta del mapa
        fragmentMapId = R.id.progressRouteMap;

        //Obtencion de la ultima ubicacion del usuario
        String currlocation = LocationService.getLatLng().toString();

        if(ContextCompat.checkSelfPermission(ActivityTripInProgress.this,
                android.Manifest.permission.SEND_SMS) != PackageManager.PERMISSION_GRANTED) {
            if(ActivityCompat.shouldShowRequestPermissionRationale(ActivityTripInProgress.this,
                    android.Manifest.permission.SEND_SMS)){
                ActivityCompat.requestPermissions(ActivityTripInProgress.this,
                        new String[]{android.Manifest.permission.SEND_SMS},1);
            } else {
                ActivityCompat.requestPermissions(ActivityTripInProgress.this,
                        new String[]{android.Manifest.permission.SEND_SMS},1);
            }
        } else {
            // Nada por ahora
        }

        if(ContextCompat.checkSelfPermission(ActivityTripInProgress.this,
                android.Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
            if(ActivityCompat.shouldShowRequestPermissionRationale(ActivityTripInProgress.this,
                    android.Manifest.permission.READ_PHONE_STATE)){
                ActivityCompat.requestPermissions(ActivityTripInProgress.this,
                        new String[]{android.Manifest.permission.READ_PHONE_STATE},1);
            } else {
                ActivityCompat.requestPermissions(ActivityTripInProgress.this,
                        new String[]{android.Manifest.permission.READ_PHONE_STATE},1);
            }
        } else {
            // Nada por ahora
        }

        if(ContextCompat.checkSelfPermission(ActivityTripInProgress.this,
                android.Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
            if(ActivityCompat.shouldShowRequestPermissionRationale(ActivityTripInProgress.this,
                    android.Manifest.permission.CALL_PHONE)){
                ActivityCompat.requestPermissions(ActivityTripInProgress.this,
                        new String[]{android.Manifest.permission.CALL_PHONE},1);
            } else {
                ActivityCompat.requestPermissions(ActivityTripInProgress.this,
                        new String[]{android.Manifest.permission.CALL_PHONE},1);
            }
        } else {
            // Nada por ahora
        }

        //Obteniendo Sharedpreferences
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
        //Mostrar Sharedpreferences en la pantalla
        String EmergencyNumber = sharedPreferences.getString("EmergencyPhoneCall", null);
        EmergencyPhone = (TextView) findViewById(R.id.localEmergencyPhone);
        EmergencyPhone.setText(EmergencyNumber);

        String phoneloc = sharedPreferences.getString("EmergencyPhoneSMS", null);
        PhoneforLocation = (TextView) findViewById(R.id.localPhoneforLocation);
        PhoneforLocation.setText(phoneloc);

        //Mostrar current location
        txtviewcurrlocation = (TextView) findViewById(R.id.textviewcurrlocation);
        txtviewcurrlocation.setText(currlocation);

        EmergencyButton = (Button) findViewById(R.id.EmergencyButtonxml);
        EmergencyButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    SmsManager smsManager = SmsManager.getDefault();
                    String phoneloc = sharedPreferences.getString("EmergencyPhoneSMS", null);

                    //Construct SMS Message
                    String currlocation = LocationService.getLatLng().toString(); //Retrieving last known location
                    String textSMSMessage = message01 + currlocation + ". " + message02 + drivername;

                    smsManager.sendTextMessage(phoneloc, null, textSMSMessage, null, null);
                    Toast.makeText(getApplicationContext(), "Emergency SMS sent",Toast.LENGTH_SHORT).show();
                    String EmergencyNumber = sharedPreferences.getString("EmergencyPhoneCall", null);
                    Intent callIntent = new Intent(Intent.ACTION_CALL);
                    Toast.makeText(getApplicationContext(), "Calling emergency phone",Toast.LENGTH_SHORT).show();
                    callIntent.setData(Uri.parse(prefix + EmergencyNumber));
                    startActivity(callIntent);
                } catch (Exception e) {
                    Toast.makeText(getApplicationContext(), "Failed: "+ e,Toast.LENGTH_SHORT).show();;
                }
            }
        });

        ChangeNumbers = (Button) findViewById(R.id.EmergencyChangeNumbers);
        ChangeNumbers.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intentMyProfile = new Intent(SplitApp.getAppContext(), Activity_EmergencyPhones.class);
                startActivity(intentMyProfile);
            }
        });

    }

    @Override
    public void OnConnected(Bundle bundle) {}

    @Override
    public void OnMapReady() {}


}
