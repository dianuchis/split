package com.cmch.split;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.cmch.split.adapters.RecyclerViewAdapter;
import com.cmch.split.modelviews.RecyclerProfileOptions;

import java.util.ArrayList;

/**
 * Created by alex on 27/11/17.
 */

public class TabProfileOptions extends Fragment {

    private RecyclerView recyclerView;
    private static RecyclerView.Adapter recyclerOptionsAdapter;
    private RecyclerView.LayoutManager layoutManager;

    private static LayoutInflater layoutInflater;
    public static LayoutInflater getInflater(){ return layoutInflater; }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        layoutInflater = inflater;

        View myview = inflater.inflate(R.layout.frag_tab_profile_options, container, false);

        recyclerView = myview.findViewById(R.id.recyclerView_profileOptions);
        // use a linear layout manager
        layoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);

        //
        RecyclerProfileOptions option1 = new RecyclerProfileOptions();
        RecyclerProfileOptions option2 = new RecyclerProfileOptions();
        RecyclerProfileOptions option3 = new RecyclerProfileOptions();
        RecyclerProfileOptions option4 = new RecyclerProfileOptions();
        option1.setOption("Payment Methods");
        option1.setIdImage(R.mipmap.ic_payment_method);
        option1.setActivity(ActivityPaymentMethod.class);
        option2.setOption("Account");
        option2.setIdImage(R.mipmap.ic_account);
        option2.setActivity(ActivityMyProfilePassword.class);
        option3.setOption("Add Vehicle");
        option3.setIdImage(R.mipmap.ic_action_directions_car);
        option3.setActivity(ActivityAddVehicle.class);
        option4.setOption("Emergency Phones");
        option4.setIdImage(R.mipmap.cellphone);
        option4.setActivity(Activity_EmergencyPhones.class);


        ArrayList<RecyclerProfileOptions> data = new ArrayList<>();
        data.add(option1);
        data.add(option2);
        data.add(option3);
        data.add(option4);

        // specify an adapter (see also next example)
        recyclerOptionsAdapter = new RecyclerViewAdapter(data);
        recyclerView.setAdapter(recyclerOptionsAdapter);

        return myview;
    }
}
