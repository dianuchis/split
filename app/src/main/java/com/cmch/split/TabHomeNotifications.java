package com.cmch.split;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.cmch.split.dbref.Trip;
import com.cmch.split.adapters.CardViewHomeNotificationsAdapter;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.GenericTypeIndicator;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by CC on 05-Mar-18.
 */

public class TabHomeNotifications extends Fragment {

    private final String TAG = "TabHomeNotifications";

    private ArrayList<Trip> dataScheduledTripsResult;
    private ArrayList<Trip> dataScheduledTrips;
    private RecyclerView.Adapter cardViewNotificationsAdapter;
    private RecyclerView.LayoutManager layoutManager;
    private RecyclerView recyclerView;
    private ValueEventListener listenerSubscribedPassengersforDriver;
    private ValueEventListener listenerSuscriptionsforPassenger;

    private static LayoutInflater layoutInflaterHome;

    public static LayoutInflater getInflater(){
        return layoutInflaterHome;
    };


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        dataScheduledTripsResult = new ArrayList<>();
        dataScheduledTrips = new ArrayList<>();

        //cargamos los dos tipos de viaje agendados una notificacion para el conductor
        // y otra para el viajero (RecyclerView que muestra cardViews como notificaciones).
        loadNotifications();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        layoutInflaterHome = inflater;

		/* Inflate the layout for this fragment */
        final View view = inflater.inflate(R.layout.frag_tab_home_notifications, container, false);
        recyclerView = view.findViewById(R.id.recyclerView_tab_home_notifications);

        //Llenamos el recycler con los datos
        fillRecyclerView(dataScheduledTrips, view.getContext());

        return view;
    }

    private void loadNotifications(){

        dataScheduledTripsResult.clear();
        dataScheduledTrips.clear();

        //Listener para traer los datos de la rama pasajeros
        listenerSubscribedPassengersforDriver = new ValueEventListener() {

            //Indica que castearemos el dataSnapshot a un Arraylist de tipo Viaje
            final GenericTypeIndicator<List<Trip>> genericTypeIndicator = new GenericTypeIndicator<List<Trip>>(){};

            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if(dataSnapshot.exists()){


                    for(DataSnapshot trips: dataSnapshot.getChildren()){
                        dataScheduledTripsResult.add(trips.getValue(Trip.class));
                    }

                    //SE GUARDAN LOS VIAJES QUE TIENEN AL MENOS UN PASAJERO EN LA RAMA PASAJEROS
                    //ESTOS SERAN MOSTRADOS EN EL RECYCLER VIEW
                    for(int i=0; i < dataScheduledTripsResult.size(); i++){
                        if (!(dataScheduledTripsResult.get(i).getPassengers() == null)){
                            dataScheduledTrips.add(dataScheduledTripsResult.get(i));
                        }
                    }

                    //Noficamos que han cambiado los datos
                    cardViewNotificationsAdapter.notifyDataSetChanged();

                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {}
        };

        //DEVUELVE LOS VIAJES QUE TENGO PUBLICADOS COMO CONDUCTOR
        Query queryPassengers = SplitApp.getFrbDatabase().child("trips").orderByChild("driver/id").equalTo(SplitApp.getFrbUserId());
        queryPassengers.addValueEventListener(listenerSubscribedPassengersforDriver);


        //Listener para traer los datos de la rama pasajeros
        listenerSuscriptionsforPassenger = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

               // Log.d(TAG, dataSnapshot.toString());

                if(dataSnapshot.exists()){

                 //   Log.d(TAG, dataSnapshot.toString());

                    //Recorremos el snapshot y tomamos cada uno de los viajes
                    //los guardamos en el arraylist de viajes.
                    for(DataSnapshot tmpTrip: dataSnapshot.getChildren()){
                        Trip tripScheduled = tmpTrip.getValue(Trip.class);

                        //Si el viaje tiene pasajeros
                        if(tripScheduled.getPassengers() != null){

                          //  Log.d(TAG, tripScheduled.getOrigin().getAddress());

                            //Recorro los pasajeros
                            for(int i=0; i < tripScheduled.getPassengers().size(); i++){
                                //Busco al pasajero que coincida con mi id de pasajero
                                if(tripScheduled.getPassengers().get(i).getId().equals(SplitApp.getFrbUserId())){
                                    dataScheduledTrips.add(tripScheduled);
                                    break;
                                }
                            }
                        }

                        //Noficamos que han cambiado los datos
                        cardViewNotificationsAdapter.notifyDataSetChanged();
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {}
        };

        //DEVUELVE LOS VIAJES A LOS QUE ESTOY SUSCRITO COMO PASAJERO
        Query querySuscriptionsPasenger = SplitApp.getFrbDatabase().child("trips");
        querySuscriptionsPasenger.addValueEventListener(listenerSuscriptionsforPassenger);


    }

    private void fillRecyclerView(ArrayList<Trip> data, Context context){
        //Agregamos los viajes al recyclerView para mostrarlo en el tab de notifications.
        layoutManager = new LinearLayoutManager(context);
        recyclerView.setLayoutManager(layoutManager);
        cardViewNotificationsAdapter = new CardViewHomeNotificationsAdapter(data);
        recyclerView.setAdapter(cardViewNotificationsAdapter);
    }

    @Override
    public void onResume() {
        super.onResume();
     //   loadNotifications();
    }
}
