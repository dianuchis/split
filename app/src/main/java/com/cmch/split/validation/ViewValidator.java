package com.cmch.split.validation;

import android.support.annotation.NonNull;
import android.view.View;

public abstract class ViewValidator {

    protected View view;
    protected int successBackGroundId;
    protected int failureBackgroundId;
    private Boolean required = false;
    private String failureMessage;


    public ViewValidator(@NonNull View view, int successBackGroundId, int failureBackgroundId, Boolean required, String failureMessage){
        this.view = view;
        this.successBackGroundId = successBackGroundId;
        this.failureBackgroundId = failureBackgroundId;
        this.required = required;
        this.failureMessage = failureMessage;
    }

    protected abstract void visualValidation();

    public abstract Boolean dataValidation();

    public abstract void disable();

    public Boolean isRequired(){
        return required;
    }

    public String getFailureMessage() {
        return failureMessage;
    }
}
