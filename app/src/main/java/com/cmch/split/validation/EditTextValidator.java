package com.cmch.split.validation;

import android.support.annotation.NonNull;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;

import com.cmch.split.SplitApp;

public final class EditTextValidator extends ViewValidator {

    TextWatcher textWatcher = null;
    private int minLength = 0;
    private int maxLength = 0;

    public EditTextValidator(@NonNull EditText editText, int successBackGroundId, int failedBackgroundId, Boolean required, String failureMessage) {
        super(editText, successBackGroundId, failedBackgroundId, required, failureMessage);

        if (isRequired()){
            textWatcher = new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                    visualValidation();
                }

                @Override
                public void afterTextChanged(Editable editable) {

                }
            };
            ((EditText)view).addTextChangedListener(textWatcher);
        }
    }

    public EditTextValidator(@NonNull EditText editText, int successBackGroundId, int failedBackgroundId, Boolean required,
                             String failureMessage, int minLength, int maxLength) {
        super(editText, successBackGroundId, failedBackgroundId, required, failureMessage);
        this.minLength = minLength;
        this.maxLength = maxLength;

        if (isRequired()){
            textWatcher = new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                    visualValidation();
                }

                @Override
                public void afterTextChanged(Editable editable) {

                }
            };
            ((EditText)view).addTextChangedListener(textWatcher);
        }
    }

    @Override
    protected void visualValidation() {
        EditText editText = (EditText) view;
        if (!dataValidation()){
            editText.setBackground(SplitApp.getResourceSplit().getDrawable(failureBackgroundId));
        }
        else{
            editText.setBackground(SplitApp.getResourceSplit().getDrawable(successBackGroundId));
        }
    }

    @Override
    public Boolean dataValidation() {
        Boolean result = false;
        EditText editText = (EditText) view;
        String text = editText.getText().toString();
        int length = text.isEmpty() ? 0 : text.length();
        if (!text.isEmpty() && maxLength == 0){
            result = true;
        }else{
            if (!text.isEmpty() && (length > maxLength && length < minLength ))
                result = true;
        }

        return result;
    }

    @Override
    public void disable(){
        if (textWatcher != null){
            EditText editText = (EditText) view;
            editText.removeTextChangedListener(textWatcher);
        }
    }
}
