package com.cmch.split.validation;


import android.content.Context;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.security.PublicKey;
import java.util.ArrayList;
import java.util.List;

public class FormValidator {

    private List<ViewValidator> viewValidators;
    private Context context;
    private int successBackgroundSource;
    private int failureBackgroundSource;

    public FormValidator(Context context, int successBackgroundSource, int failureBackgroundSource){
        viewValidators = new ArrayList<>(0);
        this.context = context;
        this.successBackgroundSource = successBackgroundSource;
        this.failureBackgroundSource = failureBackgroundSource;
    }

    public void clear(){
        for (ViewValidator item: viewValidators){
            item.disable();
        }
        viewValidators.clear();
    }

    public void addViewValidator(View view, Boolean required, String failureMessage){
        String className = view.getClass().getSimpleName();
        switch (className.replace("AppCompat", "")){
            case "RadioGroup":
                viewValidators.add(new RadioGroupValidator((RadioGroup)view, successBackgroundSource, failureBackgroundSource, required, failureMessage));
                break;
            case "Checkbox":
                viewValidators.add(new CheckBoxValidator((CheckBox)view, successBackgroundSource, failureBackgroundSource, required, failureMessage));
                break;
            case "Spinner":
                viewValidators.add(new SpinnerValidator((Spinner)view, successBackgroundSource, failureBackgroundSource, required, failureMessage));
                break;
            case "EditText":
                viewValidators.add(new EditTextValidator((EditText)view, successBackgroundSource, failureBackgroundSource, required, failureMessage));
                break;
            case "TextView":
                viewValidators.add(new TextViewValidator((TextView)view, successBackgroundSource, failureBackgroundSource, required, failureMessage));
                break;
        }
    }



    public boolean getFormValidation(){
        Boolean result = true;
        for(ViewValidator item: viewValidators){
            result = item.dataValidation();
            String failureMessage = item.getFailureMessage();
            if (!result && item.isRequired())
                if (failureMessage != null && !failureMessage.isEmpty()){
                    Toast.makeText(context, failureMessage, Toast.LENGTH_LONG).show();
                    break;
                }
        }
        return  result;
    }

}
