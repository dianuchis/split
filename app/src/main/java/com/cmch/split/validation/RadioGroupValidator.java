package com.cmch.split.validation;

import android.support.annotation.NonNull;
import android.view.View;
import android.widget.RadioGroup;

public class RadioGroupValidator extends ViewValidator {


    public RadioGroupValidator(@NonNull RadioGroup radioGroup, int successBackGroundId, int failureBackgroundId, Boolean required, String failureMessage) {
        super(radioGroup, successBackGroundId, failureBackgroundId, required, failureMessage);
    }

    @Override
    protected void visualValidation() {

    }

    @Override
    public Boolean dataValidation() {
        RadioGroup radioGroup = (RadioGroup)view;
        return radioGroup.getCheckedRadioButtonId() == -1 ? false : true;
    }

    @Override
    public void disable() {

    }
}
