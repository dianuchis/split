package com.cmch.split.validation;

import android.support.annotation.NonNull;
import android.view.View;
import android.widget.Spinner;

public class SpinnerValidator extends ViewValidator {

    public SpinnerValidator(@NonNull Spinner spinner, int successBackGroundId, int failureBackgroundId, Boolean required, String failureMessage) {
        super(spinner, successBackGroundId, failureBackgroundId, required, failureMessage);
    }

    @Override
    protected void visualValidation() {

    }

    @Override
    public Boolean dataValidation() {
        Boolean result = false;
        Spinner spinner = (Spinner) view;
        if ( spinner.getSelectedItemPosition() > 0)
            result = true;

        return result;
    }

    @Override
    public void disable() {

    }
}
