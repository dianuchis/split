package com.cmch.split.validation;

import android.support.annotation.NonNull;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.TextView;

import com.cmch.split.SplitApp;

public final class TextViewValidator extends ViewValidator {

    private TextWatcher textWatcher = null;
    private int minLength = 0;
    private int maxLength = 0;

    public TextViewValidator(@NonNull TextView textView, int successBackGroundId, int failedBackgroundId, Boolean required, String failureMessage) {
        super(textView, successBackGroundId, failedBackgroundId, required, failureMessage);

        if (isRequired()){
            textWatcher = new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                    visualValidation();
                }

                @Override
                public void afterTextChanged(Editable editable) {

                }
            };
            ((TextView)view).addTextChangedListener(textWatcher);
        }
    }

    public TextViewValidator(@NonNull TextView textView, int successBackGroundId, int failedBackgroundId, Boolean required,
                             String failureMessage, int minLength, int maxLength) {
        super(textView, successBackGroundId, failedBackgroundId, required, failureMessage);
        this.minLength = minLength;
        this.maxLength = maxLength;

        if (isRequired()){
            textWatcher = new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                    visualValidation();
                }

                @Override
                public void afterTextChanged(Editable editable) {

                }
            };
            ((TextView)view).addTextChangedListener(textWatcher);
        }
    }

    @Override
    protected void visualValidation() {
        TextView textView = (TextView) view;
        if (!dataValidation()){
            textView.setBackground(SplitApp.getResourceSplit().getDrawable(failureBackgroundId));
        }
        else{
            textView.setBackground(SplitApp.getResourceSplit().getDrawable(successBackGroundId));
        }
    }

    @Override
    public Boolean dataValidation() {
        Boolean result = false;
        TextView textView = (TextView) view;
        String text = textView.getText().toString();
        int length = text.isEmpty() ? 0 : text.length();
        if (!text.isEmpty() && maxLength == 0){
            result = true;
        }else{
            if (!text.isEmpty() && (length > maxLength && length < minLength ))
                result = true;
        }

        return result;
    }

    @Override
    public void disable(){
        if (textWatcher != null){
            TextView textView = (TextView) view;
            textView.removeTextChangedListener(textWatcher);
        }
    }

    @Override
    public String getFailureMessage(){
        String result = super.getFailureMessage();
        if (maxLength > 0)
            result.concat(String.format(" ( de %d a %d caracteres)", minLength, maxLength));
        return  result;
    }
}
