package com.cmch.split.validation;

import android.support.annotation.NonNull;
import android.view.View;
import android.widget.CheckBox;

public class CheckBoxValidator extends ViewValidator{

    public CheckBoxValidator(@NonNull CheckBox checkBox, int successBackGroundId, int failureBackgroundId, Boolean required, String failureMessage) {
        super(checkBox, successBackGroundId, failureBackgroundId, required, failureMessage);
    }

    @Override
    protected void visualValidation() {

    }

    @Override
    public Boolean dataValidation() {
        CheckBox checkBox = (CheckBox)view;
        return checkBox.isChecked();
    }

    @Override
    public void disable() {

    }
}
