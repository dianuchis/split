package com.cmch.split;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.support.v4.app.Fragment;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.cmch.split.dbref.PointLocation;
import com.cmch.split.dbref.Trip;
import com.cmch.split.dbref.Vehicle;
import com.cmch.split.callbacks.GetTextValidation;
import com.cmch.split.adapters.RecyclerViewLayouverAdapter;
import com.cmch.split.tools.SingletonVolley;
import com.cmch.split.adapters.SpinnerStringAdapter;
import com.cmch.split.adapters.SpinnerVehicleAdapter;
import com.cmch.split.tools.SplitTools;
import com.cmch.split.tools.ValidateField;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import static com.cmch.split.ActivityPublishTrips.GET_LAYOVER_LOCATION;
import static com.cmch.split.SplitApp.getAppContext;
import static com.cmch.split.SplitApp.getResourceSplit;

/**
 * Created by kitu on 11/28/17.
 */

public class TabPublishFrequentTrip extends Fragment implements View.OnClickListener, android.text.TextWatcher {

    Trip tmp;
    private String textOrigin;
    private String textDestin;
    private String textDate;
    private String textTime;
    private String textDateReturn;
    private String textTimeReturn;
    private TextView textView_PublishNew_From;
    private TextView textView_PublishNew_Destination;
    private TextView textView_PublishNew_Add;
    private RadioButton radiobtn_PublishNew_Round;
    private RadioButton radiobtn_PublishNew_Single;
    private TextView textView_PublishNew_Date;
    private TextView textView_PublishNew_Time;
    private TextView textView_PublishNew_Date_Return;
    private TextView textView_PublishNew_Time_Return;
    private TextView textView_PublishNew_Dato;
    private TextView textView_PublishNew_Precio;
    private Spinner spin_PublishNew_Vehicle;
    private Spinner spin_PublishNew_Seats;
    private EditText editText_PublishNew_Details;
    private Spinner spin_PublishNew_Bag;
    private Spinner spin_PublishNew_BagNumber;
    private CheckBox checkbox_PublishNew_AcceptTerms;
    private Button button_PublishNew_Continue;


    private List<Vehicle> vehicles;
    SpinnerVehicleAdapter spinnerVehicleAdapter;
    private SpinnerStringAdapter seatsAdapter;
    private SpinnerStringAdapter baggageTypesAdapter;
    private SpinnerStringAdapter baggageNumberAdapter;

    private RecyclerView recyclerView;
    private static RecyclerView.Adapter recyclerLayouversAdapter;
    private RecyclerView.LayoutManager layoutManager;

    private static LayoutInflater layoutInflater;
    public static LayoutInflater getInflater(){ return layoutInflater; }

    //Cargamos el contenedor que nos devolvera el linear donde pondremos los textView
    private ViewGroup insertPoint;

    private ArrayList<PointLocation> layouvers;

    //Se utilizan para obtener hora y fecha actual
    private Date date = new Date();
    private final Calendar calendar = Calendar.getInstance();
    private int year;
    private int month;
    private int dayOfMonth;
    private int hour;
    private int minutes;
    private TimePickerDialog t;
    private DatePickerDialog d;

    private android.text.TextWatcher textWatcher = this;
    private View.OnClickListener contextClickListener = this;

    private boolean validateOrigin, validateDestin, validateDateReturn, validateTimeReturn,
            validateLayouvers, validateTripType, validateDate, validateTime,
            validateVehicle, validateAvaliableSeats, validateBaggage, validateBaggagePerPerson, validateUserCond;

    private GetTextValidation getTextValidation;

    //Agregado 19/02/2018 url para consultar precios
    private String URL_RATE_QUERY = "https://us-central1-fir-plit.cloudfunctions.net/app/getprice";

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View resultView = inflater.inflate(R.layout.frag_tab_publish_frequent_trip, container, false);
        layoutInflater = inflater;

        //Utilizado para inflar el linear que contenera a los textview extra para fecha y hora.
        insertPoint = (ViewGroup) resultView.findViewById(R.id.container_publishRound_date_time);

        layouvers = new ArrayList<>();

        Bundle bundle = getArguments();
        if(bundle != null)
        {
            tmp = bundle.getParcelable("frequentTrip");
            Log.d("VIAJES", tmp.toString());
        }

        //Recycler para los layouvers
        recyclerView = resultView.findViewById(R.id.recyclerView_PublishFrecuent_Layouvers);
        layoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);

        // specify an adapter (see also next example)
        recyclerLayouversAdapter = new RecyclerViewLayouverAdapter(layouvers);
        recyclerView.setAdapter(recyclerLayouversAdapter);

        textView_PublishNew_From = resultView.findViewById(R.id.textView_PublishFrecuent_From);
        textView_PublishNew_Destination = resultView.findViewById(R.id.textView_PublishFrecuent_Destination);
        textView_PublishNew_Add = resultView.findViewById(R.id.textView_PublishFrecuent_Add);

        radiobtn_PublishNew_Round = resultView.findViewById(R.id.radiobtn_PublishFrecuent_Round);
        radiobtn_PublishNew_Single = resultView.findViewById(R.id.radiobtn_PublishFrecuent_Single);
        textView_PublishNew_Date = resultView.findViewById(R.id.textView_PublishFrecuent_Date_Going);
        textView_PublishNew_Time = resultView.findViewById(R.id.textView_PublishFrecuent_Time_Going);

        textView_PublishNew_Dato = resultView.findViewById(R.id.textView_PublishFrecuent_Dato);
        textView_PublishNew_Precio = resultView.findViewById(R.id.textView_PublishFrecuent_Precio);

        spin_PublishNew_Vehicle = resultView.findViewById(R.id.spin_PublishFrecuent_Vehicle);
        spin_PublishNew_Seats = resultView.findViewById(R.id.spin_PublishFrecuent_Seats);
        editText_PublishNew_Details = resultView.findViewById(R.id.editText_PublishFrecuent_Details);
        spin_PublishNew_Bag = resultView.findViewById(R.id.spin_PublishFrecuent_Bag);
        spin_PublishNew_BagNumber =resultView.findViewById(R.id.spin_PublishFrecuent_BagNumber);
        checkbox_PublishNew_AcceptTerms = resultView.findViewById(R.id.checkbox_PublishFrecuent_AcceptTerms);
        button_PublishNew_Continue = resultView.findViewById(R.id.button_PublishFrecuent_Continue);

        //  radiobtn_PublishNew_Round.setOnClickListener(this);
        //  radiobtn_PublishNew_Single.setOnClickListener(this);
        textView_PublishNew_From.setOnClickListener(this);
        textView_PublishNew_From.addTextChangedListener(this);
        textView_PublishNew_Destination.setOnClickListener(this);
        textView_PublishNew_Destination.addTextChangedListener(this);
        textView_PublishNew_Add.setOnClickListener(this);
        textView_PublishNew_Date.setOnClickListener(this);
        textView_PublishNew_Date.addTextChangedListener(this);
        textView_PublishNew_Time.setOnClickListener(this);
        textView_PublishNew_Time.addTextChangedListener(this);

        textOrigin = textView_PublishNew_From.getText().toString();
        textDestin = textView_PublishNew_Destination.getText().toString();
        textDate = textView_PublishNew_Date.getText().toString();
        textTime = textView_PublishNew_Time.getText().toString();

        //Prellenamos los campos de origen y destino, con los datos del viaje frecuente
        textView_PublishNew_From.setText(tmp.getOrigin().getAddress());
        textView_PublishNew_Destination.setText(tmp.getDestination().getAddress());
      //  textView_PublishNew_Add.setText(tmp.getLayovers().get(0));
        if(tmp.getType().equals("Single")){
            radiobtn_PublishNew_Single.setChecked(true);
            validateTripType = true;

          //  textView_PublishNew_Date.setText(tmp.getDate().get(0));
          //  textView_PublishNew_Time.setText(tmp.getTime().get(0));

        }else{
            tmp.setType("Round");
            radiobtn_PublishNew_Round.setChecked(true);
            validateTripType = true;
            generateDateTimeViews();
        }


        //Usado para calcular el precio del viaje
        try {
            getRate();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        radiobtn_PublishNew_Single.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (radiobtn_PublishNew_Single.isChecked()){ validateTripType = true; tmp.setType("Single");
                    //Removemos la vista que se inserto si el usuario
                    //selecciono el radioButton "Round", esto para evitar duplicidad
                    // de los textView date y time.
                    insertPoint.removeAllViews();
                }
                else{ validateTripType = false; }
            }
        });

        radiobtn_PublishNew_Round.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (radiobtn_PublishNew_Round.isChecked()){ validateTripType = true; tmp.setType("Round");
                    generateDateTimeViews();
                }
                else{ validateTripType = false; }
            }
        });

        seatsAdapter = new SpinnerStringAdapter(getContext(), SplitApp.getSeats());
        spin_PublishNew_Seats.setAdapter(seatsAdapter);

        spin_PublishNew_Seats.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                validateAvaliableSeats = true;
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {}
        });

        baggageTypesAdapter = new SpinnerStringAdapter(getContext(), SplitApp.getBaggageTypes());
        spin_PublishNew_Bag.setAdapter(baggageTypesAdapter);

        spin_PublishNew_Bag.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if(spin_PublishNew_Bag.getSelectedItem().equals(getResourceSplit().getString(R.string.msg_Publish_BaggageNotPermited))){
                    spin_PublishNew_BagNumber.setEnabled(false);
                    spin_PublishNew_BagNumber.setVisibility(View.INVISIBLE);
                    validateBaggage = true;
                }else {
                    //El usuario selecciono un tipo de equipaje
                    //Validamos el equipaje
                    validateBaggage = true;
                    //Activamos el spiner, lo mostramos y llenamos el tipo de viaje seleccionado
                    spin_PublishNew_BagNumber.setEnabled(true);
                    spin_PublishNew_BagNumber.setVisibility(View.VISIBLE);
                    //Agregamos el tipo de equipaje en listener de spinner al viaje
                    tmp.setBaggage(spin_PublishNew_Bag.getSelectedItem().toString());
                }
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {}
        });

        baggageNumberAdapter = new SpinnerStringAdapter(getContext(), SplitApp.getBaggageNumber());
        spin_PublishNew_BagNumber.setAdapter(baggageNumberAdapter);

        spin_PublishNew_BagNumber.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                //Este siempre es true porque aunque este invisible
                //siempre esta seleccionado un elemento
                validateBaggagePerPerson = true;
                //Agregamos el equipaje por persona seleccionado
                tmp.setBaggagePerPerson(Integer.parseInt(spin_PublishNew_BagNumber.getSelectedItem().toString()));
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {}
        });

        checkbox_PublishNew_AcceptTerms.setChecked(true);


        Log.d("FIREBASE USER", SplitApp.getFrbUserId());
        Query query = SplitApp.getFrbDatabase().child("vehicles").orderByChild("owner").equalTo(SplitApp.getFrbUserId());
        query.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()){
                    vehicles = new ArrayList<>();
                    for(DataSnapshot item: dataSnapshot.getChildren()){
                        Vehicle tmp = item.getValue(Vehicle.class);
                        vehicles.add(tmp);
                    }
                    spinnerVehicleAdapter = new SpinnerVehicleAdapter(getContext(), vehicles);
                    spin_PublishNew_Vehicle.setAdapter(spinnerVehicleAdapter);
                    spin_PublishNew_Vehicle.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                            validateVehicle = true;
                        }
                        @Override
                        public void onNothingSelected(AdapterView<?> parent) {}
                    });
                }
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {}
        });

        checkbox_PublishNew_AcceptTerms.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean value) {
                if (compoundButton.isChecked()){
                    validateUserCond = true;
                }else{
                    Toast.makeText(compoundButton.getContext(), "Please Acept the terms to continue", Toast.LENGTH_SHORT).show();
                    validateUserCond = false;
                }
            }
        });

        if(checkbox_PublishNew_AcceptTerms.isChecked()){
            validateUserCond = true;
        }

        button_PublishNew_Continue.setOnClickListener(this);

        //Obtenemos la fecha y hora actual
        calendar.setTime(date);

        year       = calendar.get(Calendar.YEAR);
        month      = calendar.get(Calendar.MONTH);
        dayOfMonth = calendar.get(Calendar.DAY_OF_MONTH);

        hour = calendar.get(Calendar.HOUR);
        minutes = calendar.get(Calendar.MINUTE);

        return resultView;
    }

    //Utilizado para generar las dos vistas de hora y fecha para viaje frecuente.
    private void generateDateTimeViews(){
        //Removemos las vistas que haya para que no se dupliquen cada vez que
        //se seleccione el radioButton "Round".
        insertPoint.removeAllViews();

        //Inflamos la fecha y hora desde la plantilla de layout creada
        View v2 = layoutInflater.inflate(R.layout.items_publish_frecuent_new_date_time, null);

        //Agregamos hora y fecha a la vista
        TextView textViewDate = (TextView) v2.findViewById(R.id.textView_PublishFrecuent_Date_Return);
        TextView textViewTime = (TextView) v2.findViewById(R.id.textView_PublishFrecuent_Time_Return);
        textViewDate.setHint("Date");
        textViewTime.setHint("Time");


        TextWatcher tw = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                validateDateReturn = ValidateField.textContent(textView_PublishNew_Date_Return, textDateReturn);
                validateTimeReturn = ValidateField.textContent(textView_PublishNew_Time_Return, textTimeReturn);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        };

        textViewDate.setOnClickListener(contextClickListener);
        textViewTime.setOnClickListener(contextClickListener);
        textViewDate.addTextChangedListener(tw);
        textViewTime.addTextChangedListener(tw);

        ArrayList<TextView> fields = new ArrayList<>();
        fields.add(textViewDate);
        fields.add(textViewTime);

        getTextValidation.onSuccess(fields);

        //Usados para validar el contenido de los views date y time
        textDateReturn = textView_PublishNew_Date_Return.getText().toString();
        textTimeReturn = textView_PublishNew_Time_Return.getText().toString();

        //Validadores
        validateDateReturn = ValidateField.textContent(textView_PublishNew_Date_Return, textDate);
        validateTimeReturn = ValidateField.textContent(textView_PublishNew_Time_Return, textTime);

        // Agregamos la nueva vista al contenedor del linearlayout en nuestro formulario
        insertPoint.addView(v2, 0, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    public void onClick(View view) {
        switch (view.getId()){

            case R.id.textView_PublishFrecuent_Date_Going:

                //Esta es la fecha minima que
                //mostrara el dialogo de fecha
                Date today = new Date();
                Calendar c = Calendar.getInstance();
                c.setTime(today);
                long minDate = c.getTime().getTime();

                d = new DatePickerDialog(getContext(), new DatePickerDialog.OnDateSetListener(){
                    @Override
                    public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {
                        textView_PublishNew_Date.setText(SplitTools.formatDate(i, i1, i2));
                    }
                }, year, month, dayOfMonth);
                d.getDatePicker().setMinDate(minDate);
                d.show();

                break;

            case R.id.textView_PublishFrecuent_Time_Going:

                t = new TimePickerDialog(getContext(), new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker timePicker, int hourOfDay, int minute) {
                        textView_PublishNew_Time.setText(String.format("%s:%s", hourOfDay, minute));
                        timePicker.setHour(15);
                        timePicker.setMinute(12);
                        timePicker.setOnTimeChangedListener(new TimePicker.OnTimeChangedListener() {
                            @Override
                            public void onTimeChanged(TimePicker view, int hourOfDay, int minute) {
                                //view.setHour(15);
                                // view.setMinute(12);
                            }
                        });
                    }
                }, hour, minutes, false);
                t.show();
                break;

            case R.id.textView_PublishFrecuent_Date_Return:

                //Esta es la fecha minima que
                //mostrara el dialogo de fecha
                Date todayReturn = new Date();
                Calendar cReturn = Calendar.getInstance();
                cReturn.setTime(todayReturn);
                long minDateReturn = cReturn.getTime().getTime();

                d = new DatePickerDialog(getContext(), new DatePickerDialog.OnDateSetListener(){
                    @Override
                    public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {
                        textView_PublishNew_Date_Return.setText(SplitTools.formatDate(i, i1, i2));
                    }
                }, year, month, dayOfMonth);
                d.getDatePicker().setMinDate(minDateReturn);
                d.show();

                break;

            case R.id.textView_PublishFrecuent_Time_Return:

                t = new TimePickerDialog(getContext(), new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker timePicker, int hourOfDay, int minute) {
                        textView_PublishNew_Time_Return.setText(String.format("%s:%s", hourOfDay, minute));
                        timePicker.setHour(15);
                        timePicker.setMinute(12);
                        timePicker.setOnTimeChangedListener(new TimePicker.OnTimeChangedListener() {
                            @Override
                            public void onTimeChanged(TimePicker view, int hourOfDay, int minute) {
                                //view.setHour(15);
                                // view.setMinute(12);
                            }
                        });
                    }
                }, hour, minutes, false);
                t.show();
                break;

            case R.id.textView_PublishFrecuent_Add:
                Intent intentLayover = new Intent(getAppContext(), Activity_Map1.class);
                startActivityForResult(intentLayover, GET_LAYOVER_LOCATION);
                break;

            case R.id.button_PublishFrecuent_Continue:

                Log.d("ESTATE", validateOrigin +""+ validateDestin +""+  validateTripType +""+ validateDate +""+ validateTime
                        +""+ validateVehicle +""+ validateAvaliableSeats +""+ validateBaggage +""+ validateBaggagePerPerson +""+ validateUserCond);
                if(validateOrigin && validateDestin &&  validateTripType && validateDate && validateTime
                        && validateVehicle && validateAvaliableSeats && validateBaggage && validateBaggagePerPerson && validateUserCond){
                    finishSaveTrip();
                    //Toast.makeText(view.getContext(), getString(R.string.msg_Publish_Added), Toast.LENGTH_LONG).show();
                }else{

                    if(!validateOrigin){Toast.makeText(view.getContext(), "validate origin", Toast.LENGTH_LONG).show();}
                    if(!validateDestin){Toast.makeText(view.getContext(), "validate destination", Toast.LENGTH_LONG).show();}
                    if(!validateTripType){Toast.makeText(view.getContext(), "validate trip type", Toast.LENGTH_LONG).show();}
                    if(!validateDate){Toast.makeText(view.getContext(), "validate date", Toast.LENGTH_LONG).show();}
                    if(!validateTime){Toast.makeText(view.getContext(), "validate time", Toast.LENGTH_LONG).show();}
                    if(!validateAvaliableSeats){Toast.makeText(view.getContext(), "validate seats", Toast.LENGTH_LONG).show();}
                    if(!validateBaggage){Toast.makeText(view.getContext(), "validate baggage", Toast.LENGTH_LONG).show();}
                    if(!validateBaggagePerPerson){Toast.makeText(view.getContext(), "validate bag per person", Toast.LENGTH_LONG).show();}
                    if(!validateUserCond){Toast.makeText(view.getContext(), "validate user cond", Toast.LENGTH_LONG).show();}

                    //Todo Here type the new Validation for show the errors to user.
                    Toast.makeText(view.getContext(), getString(R.string.msg_Publish_Wrong), Toast.LENGTH_LONG).show();}

//                finishSaveTrip();
//                ActivityPublishTrips.setToPublish(tmp);
//                startActivity(new Intent(getContext(), ActivityPublishSumaryTrip.class));
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data){
        if (resultCode == Activity.RESULT_OK){
            PointLocation pointLocation = PointLocation.fromString(data.getStringExtra("PointLocation"));

            if (requestCode == GET_LAYOVER_LOCATION){
                PointLocation layover = pointLocation;
                //Agregamos el layouver a la lista y al viaje nuevo
                //Layouvers para el recycler
                layouvers.add(layover);
                recyclerLayouversAdapter.notifyDataSetChanged();

                //Layouvers agregados al viaje
                tmp.getLayovers().add(layover);
            }
        }
    }

    public void finishSaveTrip() {
        //vehiculo en listener de spinner
        tmp.setVehicle((Vehicle) spin_PublishNew_Vehicle.getSelectedItem());
        tmp.setOptions(editText_PublishNew_Details.getText().toString());
        tmp.setBaggagePerPerson(Integer.getInteger(spin_PublishNew_BagNumber.getSelectedItem().toString()));
        ActivityPublishTrips.setToPublish(tmp);
        startActivity(new Intent(getContext(), ActivityPublishSumaryTrip.class));
    }


    //Agregado 19/02/2018: Consulta de precio con SingletonVolley
    public void getRate() throws JSONException {

        final String jsonParams = "{\"origin\":{\"latitude\":"+tmp.getOrigin().getLatitude()+", \"longitude\":"+tmp.getOrigin().getLongitude()+"}, "
                +"\"destination\":{\"latitude\":"+tmp.getDestination().getLatitude()+", \"longitude\":"+tmp.getDestination().getLongitude()+"}}";
        final JSONObject jsonObject = new JSONObject(jsonParams);

        final RequestQueue requestQueue = SingletonVolley.getInstance(getAppContext()).getRequestQueue();
        requestQueue.start();

        Log.d("JSON Params: ", jsonParams);
        JsonObjectRequest jsObjRequest = new JsonObjectRequest
                (Request.Method.POST, URL_RATE_QUERY, jsonObject, new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {

                        try {
                            String rate = response.get("price").toString();
                            //textView_PublishNew_Dato.setText(jsonParams);
                            textView_PublishNew_Precio.setText(rate);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        requestQueue.stop();
                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d("onErrorResponse: ", error.toString());
                    }});
        requestQueue.add(jsObjRequest);
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        validateOrigin = ValidateField.textContent(textView_PublishNew_From, textOrigin);
        validateDestin = ValidateField.textContent(textView_PublishNew_Destination, textDestin);
        validateDate = ValidateField.textContent(textView_PublishNew_Date, textDate);
        validateTime = ValidateField.textContent(textView_PublishNew_Time, textTime);

        //Callback que regresa los textViews de fecha y hora adicional para viaje redondo.
        getTextValidation = new GetTextValidation() {
            @Override
            public void onSuccess(ArrayList<TextView> field) {
                textView_PublishNew_Date_Return = field.get(0);
                textView_PublishNew_Time_Return = field.get(1);

            }

            @Override
            public void onFail(String msg) {

            }
        };
    }
    @Override
    public void afterTextChanged(Editable s) {}
}

