package com.cmch.split;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.text.Editable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.cmch.split.dbref.FrecuentTrip;
import com.cmch.split.dbref.JsScheduleFrecTripParams;
import com.cmch.split.dbref.Trip;
import com.cmch.split.callbacks.GetAvailableTrips;
import com.cmch.split.tools.SplitRequest;
import com.cmch.split.tools.SplitTools;
import com.cmch.split.tools.ValidateField;
import com.cmch.split.tools.WaitingDialog;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;


/**
 * Use the {@link FragPublishFrecuentTripNewPublish#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FragPublishFrecuentTripNewPublish extends Fragment implements View.OnClickListener, android.text.TextWatcher {


    private static final String TAG = "FragPublishFrecuentTripNewPublish";

    private TextView textView_FragScheduleNew_FrecuentTrip_Date;
    private TextView textView_FragScheduleNew_FrecuentTrip_Time;
    private Button btn_FragScheduleNew_FrecuentTrip_Search;

    //Usado para validar los campos de fecha y hora
    private boolean valDate = false, valTime = false;

    //Usado para la peticion http de los viajes frecuentes disponibles
    private String URL_QUERY_TRIPS = "https://us-central1-fir-plit.cloudfunctions.net/app/gettrips";
    //private FragSummaryNewTrips fragSummaryNewTrips;

    //Usado para enviar parametros en la peticion de los viajes frecuentes
    JsScheduleFrecTripParams jsScheduleFrecTripParams;

    private Gson gson;

    //Usado para las fechas del DatePicker
    private Date date = new Date();
    private final Calendar calendar = Calendar.getInstance();
    int year;
    int month;
    int dayOfMonth;
    int hour;
    int minute;

    //Args to send newFragmentInstance
    private static final String ARG_PARAM1 = "param1";

    // parameters received
    private FrecuentTrip frecuentTrip;

    public FragPublishFrecuentTripNewPublish() {}

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
    */

    // Rename and change types and number of parameters
    public static FragPublishFrecuentTripNewPublish newInstance(FrecuentTrip param1) {
        FragPublishFrecuentTripNewPublish fragment = new FragPublishFrecuentTripNewPublish();
        Bundle args = new Bundle();
        args.putParcelable(ARG_PARAM1, param1);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            frecuentTrip = getArguments().getParcelable(ARG_PARAM1);
        }

        //Inicializamos el ArrayList de los parametros del viaje frecuente
        jsScheduleFrecTripParams = new JsScheduleFrecTripParams();

        //Obtenemos la fecha y hora actual
        calendar.setTime(date);

        //Asignamos el horario de mañana y tarde
        calendar.set( Calendar.AM_PM, Calendar.AM);

        year        = calendar.get(Calendar.YEAR);
        month       = calendar.get(Calendar.MONTH);
        dayOfMonth = calendar.get(Calendar.DAY_OF_MONTH);

        hour = calendar.get(Calendar.HOUR_OF_DAY);
        minute = calendar.get(Calendar.MINUTE);

        //usado para serializar a JSON los parametros
        //de la consulta de viajes.
        gson = new Gson();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_frag_schedule_frecuent_trip_new_schedule, container, false);

        TextView textView_FragScheduleNew_FrecuentTrip_OriginToDestin  = view.findViewById(R.id.textView_FragScheduleNew_FrecuentTrip_OriginToDestin);
        textView_FragScheduleNew_FrecuentTrip_Date = view.findViewById(R.id.textView_FragScheduleNew_FrecuentTrip_Date);
        textView_FragScheduleNew_FrecuentTrip_Time = view.findViewById(R.id.textView_FragScheduleNew_FrecuentTrip_Time);
        btn_FragScheduleNew_FrecuentTrip_Search = view.findViewById(R.id.btn_FragScheduleNew_FrecuentTrip_Search);
        textView_FragScheduleNew_FrecuentTrip_OriginToDestin = view.findViewById(R.id.textView_FragScheduleNew_FrecuentTrip_OriginToDestin);

        //Agregamos la escucha para el clic de los botones
        textView_FragScheduleNew_FrecuentTrip_Date.setOnClickListener(this);
        textView_FragScheduleNew_FrecuentTrip_Time.setOnClickListener(this);
        btn_FragScheduleNew_FrecuentTrip_Search.setOnClickListener(this);

        textView_FragScheduleNew_FrecuentTrip_Date.addTextChangedListener(this);
        textView_FragScheduleNew_FrecuentTrip_Time.addTextChangedListener(this);

        String origin = SplitTools.getShortAddress(frecuentTrip.getOrigin().asString());
        String destin = SplitTools.getShortAddress(frecuentTrip.getDestination().asString());
        String originDestin = origin+" "+getString(R.string.hint_ScheduleTrip_To)+destin;

        textView_FragScheduleNew_FrecuentTrip_OriginToDestin.setText(originDestin);

        return view;
    }


    @Override
    public void onClick(View v) {

        //Capturamos cual fue el boton que se selecciono
        switch (v.getId()){
            case R.id.textView_FragScheduleNew_FrecuentTrip_Date:
                Date today = new Date();
                Calendar c = Calendar.getInstance();
                c.setTime(today);
                long minDate = c.getTime().getTime();

                DatePickerDialog d = new DatePickerDialog(getContext(), new DatePickerDialog.OnDateSetListener(){
                    @Override
                    public void onDateSet(DatePicker datePicker, int year, int month, int dayOfMonth) {
                        //Agregamos fecha al textview y a los parametros
                        String date = SplitTools.formatDate(year, month+1, dayOfMonth);
                        textView_FragScheduleNew_FrecuentTrip_Date.setText(date);
                        jsScheduleFrecTripParams.setDate(date);

                    }
                }, year,month, dayOfMonth);
                d.getDatePicker().setMinDate(minDate);
                d.show();
                break;
            case R.id.textView_FragScheduleNew_FrecuentTrip_Time:
                new TimePickerDialog(getContext(), new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker timePicker, int hour, int minute) {
                        //Agregamos hora al textview y a los parametros
                        String time = String.format("%s:%s", hour, minute);
                        textView_FragScheduleNew_FrecuentTrip_Time.setText(time);
                        jsScheduleFrecTripParams.setTime(time);
                    }
                }, hour, minute, false).show();
                break;
            case R.id.btn_FragScheduleNew_FrecuentTrip_Search:

                if(valDate && valTime){

                    //Limpio los valores de las banderas de validacion
                    valDate = false;
                    valTime = false;

                    ArrayList listOrigin = new ArrayList();
                    listOrigin.add(frecuentTrip.getOrigin().getLatitude());
                    listOrigin.add(frecuentTrip.getOrigin().getLongitude());

                    ArrayList listDestin = new ArrayList();
                    listDestin.add(frecuentTrip.getDestination().getLatitude());
                    listDestin.add(frecuentTrip.getDestination().getLongitude());

                    jsScheduleFrecTripParams.setOrigin(listOrigin);
                    jsScheduleFrecTripParams.setDestination(listDestin);
                    jsScheduleFrecTripParams.setType(frecuentTrip.getType());
                    jsScheduleFrecTripParams.setDistance(frecuentTrip.getDistance());


                    //Serializamos a JSON
                    String jsonParams = gson.toJson(jsScheduleFrecTripParams, JsScheduleFrecTripParams.class);
                    Log.d("PARAMS NEW TRIP ", jsonParams);


                    final WaitingDialog waitingDialog = new WaitingDialog(getActivity());
                    waitingDialog.Show();
                    try {

                        JSONObject jsonObject = new JSONObject(jsonParams);

                        SplitRequest.getAvailableTrips(new GetAvailableTrips() {
                            @Override
                            public void onSuccess(ArrayList<Trip> availableTripsResult) {

                                //Adding the fragment
                                FragSummaryNewTrips fragSummaryNewTrips = new FragSummaryNewTrips();

                                Bundle args = new Bundle();
                                args.putParcelableArrayList("trips", availableTripsResult);
                                fragSummaryNewTrips.setArguments(args);
                                Log.d("ARGS",fragSummaryNewTrips.getArguments().toString());
                                FragmentTransaction trans = ActivityScheduleTrip.getFm().beginTransaction();
                /*
                 * IMPORTANT: We use the "root frame" defined in
                 * "schedule_root_fragment_new_trip_fragment_new_trip.xml" as the reference to replace fragment
                 */
                                trans.replace(R.id.root_frame_frecuent_trips, fragSummaryNewTrips);

                /*IMPORTANT: The following lines allow us to add the fragment
                 * to the stack and return to it later, by pressing back*/

                                trans.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
                                trans.addToBackStack(null);
                                trans.commit();
                                waitingDialog.Dismiss();
                            }

                            @Override
                            public void onFail(String msg) { }
                        }, URL_QUERY_TRIPS, jsScheduleFrecTripParams, TAG, waitingDialog);
                    }catch (JSONException e) {
                        e.printStackTrace();
                    }
                }else{
                    Toast.makeText(v.getContext(), "Please fill the fields of trip", Toast.LENGTH_SHORT).show();
                }
                break;
        }

    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        valDate = ValidateField.fieldAtLeastFilled(textView_FragScheduleNew_FrecuentTrip_Date);
        valTime = ValidateField.fieldAtLeastFilled(textView_FragScheduleNew_FrecuentTrip_Time);
    }

    @Override
    public void afterTextChanged(Editable s) {

    }
}
