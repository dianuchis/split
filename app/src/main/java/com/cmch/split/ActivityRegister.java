package com.cmch.split;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.cmch.split.adapters.SpinnerImageTextAdapter;
import com.cmch.split.dbref.User;
import com.cmch.split.tools.EmailAuth;
import com.cmch.split.adapters.SpinnerStringAdapter;
import com.cmch.split.tools.SplitTools;
import com.cmch.split.tools.ValidateField;
import com.cmch.split.tools.WaitingDialog;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.iid.FirebaseInstanceId;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 * Created by alex on 27/11/17.
 */

public class ActivityRegister extends AppCompatActivity implements View.OnClickListener, android.text.TextWatcher {

    private static String TAG = "ActivityRegister";

    private boolean name = false ,emailval = false,birthday = false, workplace = false,emailconf = false,passs1 = false,
            passs2 = false,cell = false,city = false,street = false,intnum = false,zip = false,neigh = false, checkPolicy = false,
            spinGender = false, spinCodeCountry= false, spinCountry = false, spinState = false, emphone1 = false, emphone2 = false;

    //Comprueba que se ingreso texto por primera vez en el editText
    //de numero celular
    private boolean cellphoneIsEntered = false;

    private WaitingDialog waitingDialog;
    private static User newUser;
    private String[] states = new String[0];
    private String cellphone = "";
    private EmailAuth emailAuth;

    private SpinnerStringAdapter genderAdapter;
    private SpinnerStringAdapter countriesAdapter;
    private SpinnerImageTextAdapter countryCodesAdapter;
    private Spinner spin_Register_Country;
    private Spinner spin_Register_State;
    private Spinner spin_Register_Gender;

    private SpinnerStringAdapter statesAdapter;

    //E.164 : Plan internacional de numeración
    // hace referencia al código de país
    private Spinner spin_Register_CountryCode;

    private EditText editText_Register_Name;
    private TextView textView_Register_Birthday;
    private EditText editText_Register_WorkPlace;
    private EditText editText_Register_Email;
    private EditText editText_Register_EmailConf;
    private EditText editText_Register_Password;
    private EditText editText_Register_PasswordConf;
    private EditText editText_Register_CellPhone;
    private EditText editText_Register_City;
    private EditText editText_Register_Street;
    private EditText editText_Register_InteriorNumber;
    private EditText editText_Register_ZipCode;
    private EditText editText_Register_Neighborhood;
    private CheckBox checkbox_Register_Policy;
    private Button button_Register_Continue;
    private boolean isCheked = false;
    private Context context = this;
    private EditText EmergencyPhoneOnRegistration;
    private EditText EmergencyPhoneSMSOnRegistration;

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        //Quitamos el focus automatico de la entrada de teclado
        SplitTools.hideKeyboard(this);

        newUser = new User();

        //Initialize arrayAdapters
        genderAdapter = new SpinnerStringAdapter(context, SplitApp.getGenders());

        countriesAdapter = new SpinnerStringAdapter(context, SplitApp.getCountries());
        countryCodesAdapter = new SpinnerImageTextAdapter(context, SplitApp.getCountryCodes());

        editText_Register_Name = findViewById(R.id.editText_Register_Name);
        editText_Register_Name.addTextChangedListener(this);

        spin_Register_Gender = findViewById(R.id.spin_Register_Gender);
        spin_Register_Gender.setAdapter(genderAdapter);

        textView_Register_Birthday =  findViewById(R.id.textView_Register_Birthday);
        textView_Register_Birthday.addTextChangedListener(this);
        textView_Register_Birthday.setOnClickListener(this);

        editText_Register_WorkPlace = findViewById(R.id.editText_Register_Workplace);
        editText_Register_WorkPlace.addTextChangedListener(this);

        editText_Register_Email = findViewById(R.id.editText_Register_Email);
        editText_Register_Email.addTextChangedListener(this);

        editText_Register_EmailConf =  findViewById(R.id.editText_Register_EmailConf);
        editText_Register_EmailConf.addTextChangedListener(this);

        editText_Register_Password = findViewById(R.id.editText_Register_Password);
        editText_Register_Password.addTextChangedListener(this);

        editText_Register_PasswordConf = findViewById(R.id.editText_Register_PasswordConf);
        editText_Register_PasswordConf.addTextChangedListener(this);

        editText_Register_CellPhone = findViewById(R.id.editText_Register_Cellphone);
        editText_Register_CellPhone.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                cell = ValidateField.exactSize(editText_Register_CellPhone, 12);

                //Bandera que indica si se ingreso por primera vez
                //un numero en el campo de número celular
                if(editText_Register_CellPhone.length() > 0){ cellphoneIsEntered = true; }


                //Verifica si se ingreso texto en el EditText de numero telefónico.
                if(cellphoneIsEntered){

                    editText_Register_CellPhone.removeTextChangedListener(this);

                    try {
                        //Texto ingresado en el campo de numero telefonico
                        String originalString = s.toString();

                        //Cuando se ingresan mas números, devolvemos los "-" a un espacio en blanco para darle
                        //un nuevo formato acorde a los nuevos grupos de numeros ingresados
                        Long longNumberPhone;
                        if (originalString.contains("-")) {
                            originalString = originalString.replaceAll("-", "");
                        }
                        //Parseamos el numero original a un tipo long
                        longNumberPhone = Long.parseLong(originalString);

                        //Agregamos el simbolo de "-" para separar los grupos de números
                        DecimalFormatSymbols unusualSymbols = new DecimalFormatSymbols(Locale.getDefault());
                        unusualSymbols.setDecimalSeparator('|');
                        //Este metodo asigna el separador a los grupos de n
                        unusualSymbols.setGroupingSeparator('-');

                        //Creamos una instancia de decimalFormat y agregamos un patron de grupos
                        //Asi como los simbolos personalizados para los grupos
                        String pattern = "###,####,####";
                        DecimalFormat formatter = new DecimalFormat(pattern, unusualSymbols);

                        //Formateamos el número de teléfono
                        String formattedString = formatter.format(longNumberPhone);

                        //setting text after format to EditText
                        editText_Register_CellPhone.setText(formattedString);
                        editText_Register_CellPhone.setSelection(editText_Register_CellPhone.getText().length());
                    } catch (NumberFormatException nfe) {
                        nfe.printStackTrace();
                    }
                    editText_Register_CellPhone.addTextChangedListener(this);
                }

                //Verifica si el celular ya esta siendo utilizado por otro usuario de firebase.
                if(cell){

                    //Concatena el codigo de pais con el número de telefono del usuario
                    cellphone = countryCodesAdapter.getItem(spin_Register_CountryCode.getSelectedItemPosition()).getText()
                            +editText_Register_CellPhone.getText().toString();

                    Log.d(TAG, cellphone);

                    Query queryCellphone = SplitApp.getFrbDatabase().child("users").orderByChild("cellphone").equalTo(cellphone);
                    queryCellphone.addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {

                            if(dataSnapshot.exists()){
                                Toast.makeText(context, getString(R.string.lbl_Register_MessageRegistrationCellphoneRegisteredWrong), Toast.LENGTH_SHORT).show();
                                //Pintamos de rojo el editText y ponemos la bandera de celular a false
                                cell = ValidateField.exactSize(editText_Register_CellPhone, 12);
                                ValidateField.setRed(editText_Register_CellPhone);
                            }
                        }
                        @Override
                        public void onCancelled(DatabaseError databaseError) {}
                    });

                }
            }

            @Override
            public void afterTextChanged(Editable s) {}
        });

        spin_Register_CountryCode = findViewById(R.id.spin_Register_PhoneCountryCode);
        spin_Register_Country = findViewById(R.id.spin_Register_Country);
        spin_Register_State = findViewById(R.id.spin_Register_State);

        editText_Register_City = findViewById(R.id.editText_Register_City);
        editText_Register_City.addTextChangedListener(this);

        editText_Register_Street = findViewById(R.id.editText_Register_Street);
        editText_Register_Street.addTextChangedListener(this);

        editText_Register_InteriorNumber = findViewById(R.id.editText_Register_InteriorNumber);
        editText_Register_InteriorNumber.addTextChangedListener(this);

        editText_Register_ZipCode = findViewById(R.id.editText_Register_ZipCode);
        editText_Register_ZipCode.addTextChangedListener(this);

        editText_Register_Neighborhood = findViewById(R.id.editText_Register_Neighborhood);
        editText_Register_Neighborhood.addTextChangedListener(this);

        checkbox_Register_Policy =  findViewById(R.id.checkbox_Register_Policy);

        EmergencyPhoneOnRegistration = (EditText) findViewById(R.id.EmergencyPhoneOnRegistration);
        EmergencyPhoneOnRegistration.addTextChangedListener(this);

        EmergencyPhoneSMSOnRegistration = (EditText) findViewById(R.id.EmergencyPhoneSMSOnRegistration);
        EmergencyPhoneSMSOnRegistration.addTextChangedListener(this);

        //Agrega la funcionalidad para abrir un vinculo, al dar clic a politicas del usuario
        TextView textView_Register_EndUserPolicy = findViewById(R.id.textView_Register_EndUserPolicy);
        textView_Register_EndUserPolicy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Agrega un intent de tipo Browsable para abrir
                //el URL http://www.s-plit.com/terminos cuando se da clic al texto
                //de politicas de usuario final
                Intent intent = new Intent();
                intent.setAction(Intent.ACTION_VIEW);
                intent.addCategory(Intent.CATEGORY_BROWSABLE);
                intent.setData(Uri.parse("http://www.s-plit.com/terminos"));
                startActivity(intent);
            }
        });

        button_Register_Continue = findViewById(R.id.button_Register_Continue);
        button_Register_Continue.setOnClickListener(this);


        checkbox_Register_Policy.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                //Cambia la bandera de validacion para las politicas del usuario
                //a true cuando se da clic en el checkbox de politicas de usuario.
                if (compoundButton.isChecked()){
                    checkPolicy = true;
                }
            }
        });

        checkbox_Register_Policy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isCheked = !isCheked;
                checkbox_Register_Policy.setChecked(isCheked);
            }
        });

        spin_Register_Gender.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                spinGender = true;
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {}
        });

        spin_Register_CountryCode.setAdapter(countryCodesAdapter);
        spin_Register_CountryCode.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                spinCodeCountry = true;
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {}
        });

        spin_Register_Country.setAdapter(countriesAdapter);
        spin_Register_Country.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int index, long itemCode) {

                states = SplitApp.getStates(spin_Register_Country.getSelectedItem().toString());
                statesAdapter = new SpinnerStringAdapter(context, states);
                spin_Register_State.setAdapter(statesAdapter);
                spinCountry = true;

                spin_Register_State.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        spinState = true;
                    }
                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {}
                });
            }
            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {}
        });

        //Agrega un color verde, para indicar que ese campo no sera validado
        editText_Register_InteriorNumber.setBackground(getResources().getDrawable(R.drawable.edit_text_green_border));
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    public void onClick(View view) {

        if (view.getId() == R.id.textView_Register_Birthday) {

            Calendar calendar = Calendar.getInstance();
            Date today = new Date();
            //Para el tamaño minimo de fecha
            calendar.setTime(today);
            calendar.add(Calendar.YEAR, -68); // Subtract 68 years
            long minDate = calendar.getTime().getTime();

            //Para el tamaño máximo de la fecha
            Calendar calendarMaxDate = Calendar.getInstance();
            calendarMaxDate.setTime(today);
            calendarMaxDate.add( Calendar.YEAR, -18 ); // Subtract 18 years
            long maxDate = calendarMaxDate.getTime().getTime();

            int dia = calendarMaxDate.get(Calendar.DAY_OF_MONTH);
            int mes = calendarMaxDate.get(Calendar.MONTH);
            int año = calendarMaxDate.get(Calendar.YEAR);

            DatePickerDialog d = new DatePickerDialog(view.getContext(), new DatePickerDialog.OnDateSetListener() {
                @Override
                public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                    textView_Register_Birthday.setText(SplitTools.formatDate(year, month+1, dayOfMonth));
                }
            }, año,mes,dia);
            d.getDatePicker().setMinDate(minDate);
            d.getDatePicker().setMaxDate(maxDate);
            d.show();

            //calendar.getTime().getTime() se usa para poner la fecha actual como maxima o minima
        }

        if (view.getId() == R.id.button_Register_Continue) {

            if(checkbox_Register_Policy.isChecked()){
                if (name && birthday && workplace && emailval && emailconf && passs1 && passs2 && cell && city && street && zip && neigh && checkPolicy
                       && spinCodeCountry && spinGender && spinState && spinCountry ){

                    waitingDialog = new WaitingDialog(this);
                    waitingDialog.Show();
                    //Se llenan los datos generales del usuario
                    fillGeneralDataNewUserObject();
                    //Se inicia sesion en firebase con el usuario registrado
                    emailAuth.createUserWithEmail(newUser.getEmail(), newUser.getPassword());
                }else{ Toast.makeText(this, R.string.lbl_Register_MessageRegistrationFieldsWrong, Toast.LENGTH_LONG).show();}
            }else{
                Toast.makeText(this, R.string.lbl_Register_MessageRegistrationEndUserPolicyWrong, Toast.LENGTH_LONG).show();
            }
        }
    }

    private void fillGeneralDataNewUserObject(){
        newUser.setFullName(editText_Register_Name.getText().toString());
        newUser.setGender(spin_Register_Gender.getSelectedItem().toString());
        newUser.setBirthday(textView_Register_Birthday.getText().toString());
        newUser.setWorkplace(editText_Register_WorkPlace.getText().toString());
        newUser.setCellphone(cellphone);
        newUser.setCountry(spin_Register_Country.getSelectedItem().toString());
        newUser.setState(spin_Register_State.getSelectedItem().toString());
        newUser.setCity(editText_Register_City.getText().toString());
        newUser.setCity(editText_Register_City.getText().toString());
        newUser.setStreetAndNumber(editText_Register_Street.getText().toString());
        newUser.setInteriorNumber(editText_Register_InteriorNumber.getText().toString());
        newUser.setZipcode(editText_Register_ZipCode.getText().toString());
        newUser.setNeighborhood(editText_Register_Neighborhood.getText().toString());
        newUser.setEmail(editText_Register_Email.getText().toString());
        newUser.setPassword(editText_Register_Password.getText().toString());

        //Si se le asigno un token al dispositivo, lo agregamos al usuario para que pueda recibir notificaciones posteriormente
        if(FirebaseInstanceId.getInstance().getToken() != null){
            newUser.setFcmRegisterToken(FirebaseInstanceId.getInstance().getToken());
        }

        //Se agregan los numeros de emergencia al SharedPreferences
        String EmPhone1 = EmergencyPhoneOnRegistration.getText().toString();
        String EmPhone2 = EmergencyPhoneSMSOnRegistration.getText().toString();
        SharedPreferences sharedPreferences;
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("EmergencyPhoneCall",EmPhone1);
        editor.putString("EmergencyPhoneSMS",EmPhone2);
        editor.apply();

    }

    @Override
    public void onResume(){
        super.onResume();
        if (emailAuth == null){
            emailAuth = new EmailAuth(this) {
                @Override
                public void OnSuccess(@NonNull Task<AuthResult> task) {

                    String idNewUser = task.getResult().getUser().getUid();
                    final DatabaseReference newUserRef = SplitApp.getFrbDatabase().child("users");

                    //Agrego su id al usuario
                    newUser.setId(idNewUser);

                    Query queryUser = newUserRef.child(idNewUser);
                    queryUser.addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {

                            //Si no existe el usuario
                            if(dataSnapshot.getValue() == null){
                                //Agrego el espacio e inserto el usuario
                                newUserRef.push();
                                newUserRef.child(newUser.getId()).setValue(newUser);

                                waitingDialog.Dismiss();
                                finish();
                                startActivity(new Intent(getApplicationContext(), ActivityUserDocument.class));
                            }else{
                                waitingDialog.Dismiss();
                                Toast.makeText(getApplicationContext(), "The current email has been registered", Toast.LENGTH_LONG).show();
                                editText_Register_Email.setText("");
                                editText_Register_EmailConf.setText("");
                               // SplitApp.getFrbAuth().signOut();
                            }
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {

                        }
                    });
                }

                @Override
                public void OnFailure(Exception exception) {
                    waitingDialog.Dismiss();
                    Toast.makeText(getApplicationContext(), exception.getLocalizedMessage(), Toast.LENGTH_LONG).show();
                    editText_Register_Email.setText("");
                    editText_Register_EmailConf.setText("");

                }
            };
        }
    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

        //Validamos el tamaño de cada campo de acuerdo a tamaños exactos o
        //rangos de valores máximos y minimos
        name =  ValidateField.fieldSize(editText_Register_Name,35);
        workplace = ValidateField.fieldMaxMin(editText_Register_WorkPlace,20,3);
        city = ValidateField.fieldMaxMin(editText_Register_City,20,3);
        street = ValidateField.fieldMaxMin(editText_Register_Street,20,1);
        emphone1 = ValidateField.fieldMaxMin(EmergencyPhoneOnRegistration,10,3);
        emphone2 = ValidateField.fieldMaxMin(EmergencyPhoneSMSOnRegistration,10,3);


        //intnum = ValidateField.fieldSize(editText_Register_InteriorNumber, 3);
        zip = ValidateField.fieldMaxMin(editText_Register_ZipCode,5,5);
        neigh = ValidateField.fieldMaxMin(editText_Register_Neighborhood,15,3);

        if (textView_Register_Birthday.getText().length() > 0){
            textView_Register_Birthday.setBackground(getResources().getDrawable(R.drawable.edit_text_green_border));
            birthday = true;
        } else {
            textView_Register_Birthday.setBackground(getResources().getDrawable(R.drawable.edit_text_red_border));
        }

        //Validation of emails
        String email = editText_Register_Email.getText().toString().trim();
        String email1 = editText_Register_Email.getText().toString();
        String emailConf = editText_Register_EmailConf.getText().toString();
        String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
        if (email.matches(emailPattern) && email.length() > 0)
        {
            emailval = true;
            ValidateField.setGreen(editText_Register_Email);
        } else{
           // Toast.makeText(getApplicationContext(), getResources().getString(R.string.lbl_Register_MessageRegistrationEmailFormatWrong), Toast.LENGTH_LONG).show();
            ValidateField.setRed(editText_Register_Email);
        }

        if (emailConf.equals(email1) && emailConf.matches(emailPattern)){
            emailconf = true;
            ValidateField.setGreen(editText_Register_EmailConf);
        } else {
           // Toast.makeText(getApplicationContext(), getResources().getString(R.string.lbl_Register_MessageRegistrationEmailWrong), Toast.LENGTH_LONG).show();
            ValidateField.setRed(editText_Register_EmailConf);
        }

        //Validation of password match
        String pass1 = editText_Register_Password.getText().toString();
        String passPattern = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=])(?=\\S+$).{8,}$";
        String pass2 = editText_Register_PasswordConf.getText().toString();
        if (pass1.matches(passPattern)){

            passs1 = true;
            ValidateField.setGreen(editText_Register_Password);
        } else {
           // Toast.makeText(getApplicationContext(), getResources().getString(R.string.lbl_Register_MessageRegistrationPasswordFormatWrong), Toast.LENGTH_LONG).show();
            ValidateField.setRed(editText_Register_Password);
        }
        if (pass2.equals(pass1) && pass2.matches(passPattern)){
            passs2 = true;
            ValidateField.setGreen(editText_Register_PasswordConf);
        }else{
           // Toast.makeText(getApplicationContext(), getResources().getString(R.string.lbl_Register_MessageRegistrationPasswordWrong), Toast.LENGTH_LONG).show();
            ValidateField.setRed(editText_Register_PasswordConf);
        }

    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

    @Override
    public void afterTextChanged(Editable s) {}

}