package com.cmch.split.splashscreen;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.android.volley.VolleyError;
import com.cmch.split.ActivityLogin;
import com.cmch.split.R;
import com.cmch.split.SplitApp;
import com.cmch.split.baseactivity.BaseActivity;
import com.cmch.split.tools.SplitJSONRequest;

import org.json.JSONObject;

public class ActivityInitSplit extends BaseActivity {

    //Descripcion: MUESTRA UNA PANTALLA CON EL LOGO DE SPLIT MIENTRAS SE CARGAN
    //             LOS DATOS DEL USUARIO, SI EL USUARIO NO ESTA LOGUEADO MANDA A LOGIN

    private static final String TAG = "ActivityInitSplit";

    // Duración en milisegundos que se mostrará el splash
    private final int DURACION_SPLASH = 10000; // 10 segundos

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_init_split);

        //cargar los datos de catalogos desde servidor.cloudfunctions
        SplitJSONRequest splitJSONRequest = new SplitJSONRequest(SplitApp.CATALOGS_URL, null) {
            @Override
            public void onResponseOrError(JSONObject response, VolleyError error) {
                if (error == null){
                    SplitApp.setCatalogs(response);

                    if (SplitApp.getFrbAuth().getCurrentUser() == null){
                        startActivity(new Intent(getApplicationContext(), ActivityLogin.class));
                        finish();
                        Log.d(TAG, "User not logged");
                    }else {
                        loadUserData();
                        //Cesar: I used code bellow to log out when I had trouble getting in the App
                        startActivity(new Intent(getApplicationContext(), ActivityLogin.class));
                    }

                }
            }
        };

        SplitApp.addSplitRequest(splitJSONRequest);

    }

}
