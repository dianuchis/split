package com.cmch.split;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.cmch.split.dbref.PaymentMethod;
import com.cmch.split.adapters.RecyclerViewPaymentAdapter;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.Collection;
import java.util.List;

public class ActivityPaymentMethod extends AppCompatActivity {

    private final static String TAG = "ActivityPaymentMethod";

    private static RecyclerView recyclerView;
    private static RecyclerView.Adapter recyclerPayMethodsAdapter;
    private RecyclerView.LayoutManager layoutManager;
    private TextView btnAddPayment;

    private static List<PaymentMethod> cards;

    public static List<PaymentMethod> getCards() {
        return cards;
    }

    public static void setCards(List<PaymentMethod> cards) {
        ActivityPaymentMethod.cards = cards;
    }

    public static RecyclerView.Adapter getRecyclerPayMethodsAdapter() {
        return recyclerPayMethodsAdapter;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment_method);

        btnAddPayment = findViewById(R.id.button_PaymentMethod_AddPayMethod);
        btnAddPayment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), ActivityAddPayment.class));
            }
        });

        recyclerView = findViewById(R.id.recyclerView_PaymentMethod);
        // use a linear layout manager
        layoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(layoutManager);

        //Usado para deserializar las tarjetas
        Type collectionType = new TypeToken<Collection<PaymentMethod>>(){}.getType();
        Gson gsonCards = new Gson();

        //Obtenemos el string de lista de tarjetas de sharedPreferences
        SharedPreferences sharedPref = getBaseContext().getSharedPreferences("Cards", Context.MODE_PRIVATE);
        String ListCard = sharedPref.getString("cards", "[]");

        //Deserializamos las tarjetas
        cards = gsonCards.fromJson(ListCard, collectionType);

        Log.d(TAG, cards.toString());

        //Instanciamos el adaptador que mostrara las tarjetas
        //Asignamos el adaptador, lo cual llenara el recycler con las tarjetas
        //si hubiere.
        recyclerPayMethodsAdapter = new RecyclerViewPaymentAdapter(cards);
        recyclerView.setAdapter(recyclerPayMethodsAdapter);

    }

}
