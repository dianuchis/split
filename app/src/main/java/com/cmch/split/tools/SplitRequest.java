package com.cmch.split.tools;

import android.util.Log;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.cmch.split.callbacks.CallbackGetFrecuentTrips;
import com.cmch.split.callbacks.CallbackGetPrice;
import com.cmch.split.callbacks.GetAvailableTrips;
import com.cmch.split.SplitApp;
import com.cmch.split.dbref.FrecuentTrip;
import com.cmch.split.dbref.JsScheduleFrecTripParams;
import com.cmch.split.dbref.Trip;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static com.cmch.split.SplitApp.getAppContext;

/**
 * Created by Eduardo Gabriel Polanco Pereyra on 27-Mar-18.
 */

//Descripcion: Esta clase contiene miembros estáticos que realizan
//              peticiones http a través de Volley y el patrón singleton,
//              para traer datos desde cloud functions en firebase.

public class SplitRequest {

    /**Descripcion: Consulta el precio de un viaje segun su origen y destino
    *@callback callbackGetPrice ->
    *@param jsonParams origin -> {"origin":{"latitude":"", "longitude":""}} , destination -> {"destination":{"latitude":"", "longitude":""}}
    *@param TAG_ACTIVITY imprime errores */

    public static void getPrice(final CallbackGetPrice callbackGetPrice, String jsonParams, final String TAG_ACTIVITY) throws JSONException {

        //URL para obtener el precio de un origen y destino.
        final String URL_PRICE_QUERY = "https://us-central1-fir-plit.cloudfunctions.net/app/getprice";

        //Guarda los parametros para la consulta del precio en un JSONObject
        final JSONObject jsonObject = new JSONObject(jsonParams);

        //Cola de peticiones
        final RequestQueue requestQueue = SingletonVolley.getInstance(getAppContext()).getRequestQueue();
        requestQueue.start();

        //Peticion de tipo JsonObjectRequest
        Log.d("JSON Params: ", jsonParams);
        JsonObjectRequest jsObjRequest = new JsonObjectRequest
                (Request.Method.POST, URL_PRICE_QUERY, jsonObject, new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {

                        try {
                            String price = response.get("price").toString();

                            //Este callback nos devolvera el precio del viaje una
                            //vez creado una instancia del mismo
                            callbackGetPrice.onSuccess(price);
                            //textView_PublishNew_Dato.setText(jsonParams);
                            Log.d(TAG_ACTIVITY+" "+".Price: ",price);

                        } catch (JSONException e) {
                            e.printStackTrace();
                            callbackGetPrice.onFail(TAG_ACTIVITY+" JSON Exception: "+e.toString());
                        }

                        requestQueue.stop();
                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d(TAG_ACTIVITY, "onErrorResponse: "+error.toString());
                    }});
        requestQueue.add(jsObjRequest);
    }



    /**Descripcion: Consulta los viajes frecuentes que ha guardado un pasajero en el TabScheduleNewTrip.
     *@param callbackGetFrecuentTrips: Callback que guarda los viajes frecuentes y los manda en el metodo onSuccess
     *@param TAG: Nombre del activity que invoco esta peticion.
     *@param URL_POST_FRECUENT_TRIPS: URL donde se hara la peticion http*/

    public static void getFrecuentTrips(final CallbackGetFrecuentTrips callbackGetFrecuentTrips, final String TAG,
                                        String URL_POST_FRECUENT_TRIPS) throws JSONException {

        //idPassenger es utilizado para filtrar todos los viajes que el usuario actual a
        //guardado al momento de agendar un viaje y guardarlo como frecuente.


        Log.d(TAG, "URL: "+URL_POST_FRECUENT_TRIPS);

        final RequestQueue requestQueue = SingletonVolley.getInstance(getAppContext()).getRequestQueue();
        requestQueue.start();
        Log.d(TAG, "{\"idPassenger\":"+"\""+ SplitApp.getFrbUserId()+"\"}");


        //Tipo de petición, URL, Parametros en formato JSONObject.
        StringRequest postRequest = new StringRequest(Request.Method.POST, URL_POST_FRECUENT_TRIPS,
                new Response.Listener<String>() {

                    ArrayList<FrecuentTrip> datafrecuentTrips = new ArrayList<>();

                    @Override
                    public void onResponse(String response) {
                        final Gson gson = new Gson();

                        Log.d(TAG, "Frec. Trips:"+response);

                        //Limpio los viajes frecuentes disponibles
                        datafrecuentTrips.clear();

                        try {

                            JSONArray jsonArray = new JSONArray(response);
                            Log.d("TRIPS 2: ",response);

                            //   JSONArray jsonArray = new JSONArray(response.toString());
                            //   JSONObject jsOFrecTrips = jsonArray.getJSONObject(0);

                            for(int i = 0; i < jsonArray.length(); i++){
                                JSONObject trip = jsonArray.getJSONObject(i);

                                FrecuentTrip tmpTrip = gson.fromJson(trip.toString(), FrecuentTrip.class);
                             //   Log.d(TAG,"Frec. Trip: "+tmpTrip.toString());

                                datafrecuentTrips.add(tmpTrip);
                            }

                            callbackGetFrecuentTrips.onSuccess(datafrecuentTrips);

                        } catch (JSONException e){
                            Log.d(TAG, "JSON error: "+e.toString());
                        }

                        Log.d(TAG,"TAM "+datafrecuentTrips.size()+"");
                        requestQueue.stop();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d(TAG,"VOLLEY error: "+error.toString());
                    }
                }
          ) {
            // here is params will add to your url using post method
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("idPassenger", SplitApp.getFrbUserId());
                return params;
            }
        };
        requestQueue.add(postRequest);

    }

    /**Descripcion: Consulta los viajes disponibles por medio de origen, destino, hora, fecha, rango de tiempo y rango de busqueda (Kms).
     *@param getAvailableTrips: Callback que guarda los viajes disponibles obtenidos en la peticion y los manda en el metodo onSuccess.
     *@param url: URL donde se hara la peticion http.
     *@param jsScheduleFrecTripParams: origin -> {"origin":{"latitude":"", "longitude":""}} , destination -> {"destination":{"latitude":"", "longitude":""}}.
     *@param TAG: Nombre del activity que invoco esta peticion.*/

    public static void getAvailableTrips(final GetAvailableTrips getAvailableTrips, String url, final JsScheduleFrecTripParams jsScheduleFrecTripParams,
                                         final String TAG, final WaitingDialog waitingDialog) throws JSONException {
        Log.d(TAG,"URL: "+url);

        final RequestQueue requestQueue = SingletonVolley.getInstance(getAppContext()).getRequestQueue();
        requestQueue.start();

        //Tipo de petición, URL, Parametros en formato JSONObject.
        StringRequest jsObjRequest = new StringRequest
                (Request.Method.POST, url, new Response.Listener<String>() {

                    @Override
                    public void onResponse(String response) {

                        //Utilizado para guardar los viajes disponibles
                        ArrayList<Trip> availableTrips = new ArrayList<>();

                        final Gson gson = new Gson();
                        //Limpio los viajes disponibles
                        availableTrips.clear();

                        try {
                            //Obtengo el JSArray por medio del String response
                            JSONArray jsonArray = new JSONArray(response);
                            Log.d(TAG,"TRIPS : "+response);

                            //Recorro los JSObject contenidos en el array
                            for(int i = 0; i < jsonArray.length(); i++){
                                //Obtengo cada viaje
                                JSONObject trip = jsonArray.getJSONObject(i);
                                Log.d(TAG, "TRIP "+trip.toString());

                                //Deserializo a un objeto de tipo viaje
                                Trip tmpTrip = gson.fromJson(trip.toString(), Trip.class);

                              //  Map<String, Object> map = gson.fromJson(trip.toString(), new TypeToken<Map<String, Object>>(){}.getType());

                                availableTrips.add(tmpTrip);
                            }

                            getAvailableTrips.onSuccess(availableTrips);

                        } catch (JSONException e){
                            Log.d(TAG, "JSON error: "+e.toString());
                            getAvailableTrips.onFail(e.toString());
                            waitingDialog.Dismiss();
                            Toast.makeText(waitingDialog.getContext(), "Well, we have a JSON error", Toast.LENGTH_SHORT).show();
                        }

                        Log.d(TAG, "TAMAÑO DE VIAJES"+availableTrips.size());
                        requestQueue.stop();
                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d(TAG, "VOLLEY ERROR "+error.toString());
                        getAvailableTrips.onFail(error.toString());
                        waitingDialog.Dismiss();
                        Toast.makeText(waitingDialog.getContext(), "Well, we have a error, try again", Toast.LENGTH_SHORT).show();

                    }
                }){
            // here is params will add to your url using post method
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("date", jsScheduleFrecTripParams.getDate());
                params.put("destination", jsScheduleFrecTripParams.getDestination().toString());
                params.put("distance", String.valueOf(jsScheduleFrecTripParams.getDistance()));
                params.put("origin", jsScheduleFrecTripParams.getOrigin().toString());
                params.put("time", jsScheduleFrecTripParams.getTime());
                params.put("type", jsScheduleFrecTripParams.getType());
                params.put("timeRange", String.valueOf(jsScheduleFrecTripParams.getTimeRange()));
                return params;
            }
        };

        requestQueue.add(jsObjRequest);
    }


}
