package com.cmch.split.tools;

import android.Manifest;
import android.app.Activity;
import android.app.FragmentManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Toast;

import com.cmch.split.ActivityTripInProgress;
import com.cmch.split.R;
import com.cmch.split.SplitApp;
import com.cmch.split.services.LocationService;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.FusedLocationProviderApi;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.android.gms.tasks.OnSuccessListener;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by Gabriel Polanco on 09-Apr-18.
 *
 * Descripcion: Esta clase proporciona acceso a:
 *
 * 1.- Inicialización de fragment GoogleMaps (GoogleApiClient.ConnectionCallbacks).
 * 2.- Última localización conocida por el dispositivo (COARSE_LOCATION).
 * 3.- Actualizaciones de ubicacion cada 10 segundos (Servicio: LocationService).
 */

public abstract class ActivityMap extends AppCompatActivity implements GoogleApiClient.ConnectionCallbacks {

    private static final String TAG = "ActivityMap";

    private static GoogleApiClient mGoogleApiClient;
    protected GoogleMap map;
    protected int fragmentMapId;
    protected Context _this;
    protected LocationManager locationManager;
    private LatLng latLng;

    private FusedLocationProviderClient mFusedLocationClient;

    //Muestra un marcador en el mapa que cambia según la ubicación actual del usuario
    protected Marker marker;

    //Intent que crea e inicializa el servicio de localización
    private Intent serviceLocationIntent;

    //Variables para conectarnos al servicio LocationService
    private LocationService locationService;
    private boolean isServiceConnected;
    private ServiceConnection serviceConnection;

    private int cont = 0;
    private LatLng latLngFromService;
    private PolylineOptions polylineOptions;
    private ArrayList points;


    private OnMapReadyCallback onMapReady = new OnMapReadyCallback() {
        @Override
        public void onMapReady(GoogleMap googleMap) {
            map = googleMap;
            if (map != null) {
                setUpMap();
            }

            OnMapReady();
        }
    };


    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient
                .Builder(this)
                .addConnectionCallbacks(this)
                .addApi(LocationServices.API)
                .build();
    }

    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        _this = this;

        locationManager = SplitApp.getLocationManager();

        //--------------------Inicializacion de servicio: LocationService ------------------//

        //Intent que instancia un servicio desde la clase LocationService
        //para capturar la localizacion del usuario cada 5 min.
        serviceLocationIntent = new Intent(SplitApp.getAppContext(), LocationService.class);

        //Inicia el servicio de localización geográfica actual
        startService(serviceLocationIntent);

        //Vincula el activity ActivityTripInProgress con el servicio iniciado
        bindService();

    }

    private void setUpMap() {
        map.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED){
            //Activa el circulo azul de posicion actual en el mapa
            // map.setMyLocationEnabled(true);

        }
    }

    protected void setUpMapIfNeeded() {
        if (map == null) {
            ((SupportMapFragment) this.getSupportFragmentManager()
                    .findFragmentById(fragmentMapId)).getMapAsync(onMapReady);
        }
    }

    //Crea un objeto Handler (en el hilo principal de ejecucion por defecto)
    Handler handler = new Handler();

    //El objeto runnable se asigna al handler para ejecutarse cada 10 segundos,
    //este verifica si se ha conectado al servicio de LocationService y si es asi
    //primero inicializa el marcador y seguidamente mueve el marcador, en la nueva
    //posicion obtenida en el transcurso.
    private Runnable runnableCode = new Runnable() {
        @Override
        public void run() {
            // Do something here on the main thread

            cont++;

                if(isServiceConnected) {

                    if(cont % 2 == 0){

                        latLngFromService = locationService.getLatLng();
                        setNewLocation(latLngFromService);

                        points.add(latLngFromService);

                        Log.d(TAG, cont+" Location: " + latLngFromService.latitude + " " + latLngFromService.longitude);
                    }else{

                        polylineOptions.addAll(points);

                        polylineOptions.width(15);
                        polylineOptions.color(getColor(R.color.colorPrimary));
                        polylineOptions.geodesic(true);

                        map.addPolyline(polylineOptions);
                        points.clear();
                    }

                }else{
                    handler.removeCallbacks(runnableCode);
                }

            // Repeat this the same runnable code block again another 10 seconds
            handler.postDelayed(runnableCode, 4000);
        }
    };

    @Override
    public void onConnected(Bundle bundle) { OnConnected(bundle); }

    @Override
    public void onConnectionSuspended(int i) {}

    @Override
    public void onResume(){
        super.onResume();


        if (mGoogleApiClient == null || !mGoogleApiClient.isConnected()){
            buildGoogleApiClient();
            mGoogleApiClient.connect();
            setUpMapIfNeeded();

            //Asigna la primera posicion del marcador
            //con la ultima ubiación conocida del dispositivo
            setFirstLocation();

            polylineOptions = new PolylineOptions();

            points = new ArrayList<LatLng>();

            // Inicia el codigo del runnable, el cual toma
            // cada 4 segundos la Latlng de el servicio LocationService
            handler.post(runnableCode);

        }

    }

    public Marker addMarker(LatLng latLng, String title){
        return map.addMarker(new MarkerOptions().position(latLng).title(title));
    }

    public void centerInBounds(LatLngBounds latLngBounds){
        map.moveCamera(CameraUpdateFactory.newLatLngBounds(latLngBounds, 50));
    }

    public void center(LatLng latLng){
        if (latLng != null)
            map.moveCamera(CameraUpdateFactory.newLatLng(latLng));
    }

    public void zoom(float zoomValue){
        map.animateCamera(CameraUpdateFactory.zoomTo(zoomValue));
    }

    public Circle addCirce(LatLng latLng, double radius){
        return map.addCircle(new CircleOptions().center(latLng).radius(radius).strokeWidth(0f).fillColor(0x550000FF));
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();

        unBindService();
        //Termina el servicio de localizacion geográfica actual
        //Cuando se cierra por completo la aplicacion
        stopService(serviceLocationIntent);
    }


    //METODOS PARA VINCULAR EL ACTIVITY AL SERVICIO LocationService

    protected void bindService(){

        if(serviceConnection == null){
            serviceConnection = new ServiceConnection() {
                @Override
                public void onServiceConnected(ComponentName name, IBinder service) {

                    LocationService.ServiceBinder serviceBinder = (LocationService.ServiceBinder) service;
                    locationService = serviceBinder.getServiceLocation();
                    isServiceConnected = true;

                    Toast.makeText(_this, "Service Binded", Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onServiceDisconnected(ComponentName name) {
                    //Cuando se desconecta el servicio cambiamos la variable boolean
                    //isServiceConnected a false
                    isServiceConnected = false;
                }
            };
        }

        bindService(serviceLocationIntent, serviceConnection, Context.BIND_AUTO_CREATE);
    }

    //Si estamos conectados al servicio, nos desvincula del servicio de localizacion
    protected void unBindService(){

        if(isServiceConnected){
            unbindService(serviceConnection);
            isServiceConnected = false;
            Toast.makeText(this, "Service unBinded", Toast.LENGTH_LONG).show();
        }
    }


    //Actualiza el marcador del mapa según la posición obtenida
    //@param: latLng
    private void setNewLocation(LatLng latLng){ marker.setPosition(latLng); }


    //Inicializa el marcador del mapa en la posicion actual del usuario
    //@callback: OnSuccessListener<Location>
    public LatLng setFirstLocation(){

        latLng = null;

        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        mFusedLocationClient.getLastLocation()
                .addOnSuccessListener(this, new OnSuccessListener<Location>() {

                    @Override
                    public void onSuccess(Location location) {
                        // Got last known location. In some rare situations this can be null.
                        if (location != null) {
                            latLng = new LatLng(location.getLatitude(), location.getLongitude());

                            marker = addMarker(latLng , "Origin");
                            center(latLng);
                            zoom(20.0f);

                            Log.d(TAG, "Location: "+location.getLatitude()+" "+location.getLongitude());
                        }
                    }
                });


        return latLng;
    }


    public abstract void OnConnected(Bundle bundle);

    public abstract void OnMapReady();
}
