package com.cmch.split.tools;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.view.LayoutInflater;

import com.cmch.split.ActivityUserDocument;
import com.cmch.split.R;
import com.cmch.split.SplitApp;

/**
 * Created by alex on 13/01/18.
 */

public class WaitingDialog  {

    private AlertDialog alertDialog;
    private Activity activity;

    public WaitingDialog(Activity activity){
        this.activity = activity;
        AlertDialog.Builder builder = new AlertDialog.Builder(this.activity);
        builder.setMessage(SplitApp.getResourceSplit().getString(R.string.lbl_WaitingDialog_Message));
        LayoutInflater layoutInflater = this.activity.getLayoutInflater();
        builder.setView(layoutInflater.inflate(R.layout.waiting_dialog, null));
        builder.setCancelable(false);
        alertDialog = builder.create();
    }

    public void Show(){
        alertDialog.show();
    }

    public void Dismiss(){
        alertDialog.dismiss();
    }

    public Context getContext(){
        return activity;
    }

}
