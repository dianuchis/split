package com.cmch.split.tools;

import android.support.annotation.NonNull;
import android.util.SparseArray;

import com.xw.repo.BubbleSeekBar;

import java.util.List;

public class SplitBubbleSeekBar {

    private List<BubbleSeekBarValue> values;

    private int intValue;
    private float floatValue;
    private String stringValue;

    BubbleSeekBar bubbleSeekBar;

    public SplitBubbleSeekBar(BubbleSeekBar bubbleSeekBar) {
        this.bubbleSeekBar = bubbleSeekBar;
    }

    public List<BubbleSeekBarValue> getValues() {
        return values;
    }

    public void setValues(final List<BubbleSeekBarValue> values) {
        this.values = values;
        bubbleSeekBar.setCustomSectionTextArray(new BubbleSeekBar.CustomSectionTextArray() {
            @NonNull
            @Override
            public SparseArray<String> onCustomize(int sectionCount, @NonNull SparseArray<String> array) {
                array.clear();
                for (int i = 0; i < values.size(); i++){
                    array.put(i, values.get(i).getLabel());
                }
                return array;
            }
        });


        bubbleSeekBar.setOnProgressChangedListener(new BubbleSeekBar.OnProgressChangedListener() {
            @Override
            public void onProgressChanged(BubbleSeekBar bubbleSeekBar, int progress, float progressFloat) {

            }

            @Override
            public void getProgressOnActionUp(BubbleSeekBar bubbleSeekBar, int progress, float progressFloat) {
                int range = (int) (bubbleSeekBar.getMax() / values.size() -1);
                int index = 0;
                if (progress > 0){
                    index = progress/range;
                }
                intValue = values.get(index).getIntValue();
                floatValue = values.get(index).getFloatValue();
                stringValue = values.get(index).getStringValue();
            }

            @Override
            public void getProgressOnFinally(BubbleSeekBar bubbleSeekBar, int progress, float progressFloat) {
                int range = (int) (bubbleSeekBar.getMax() / values.size() -1);
                int index = 0;
                if (progress > 0){
                    index = progress/range;
                }
                intValue = values.get(index).getIntValue();
                floatValue = values.get(index).getFloatValue();
                stringValue = values.get(index).getStringValue();
            }
        });

    }

    public int getIntValue() {
        return intValue;
    }

    public float getFloatValue() {
        return floatValue;
    }

    public String getStringValue() {
        return stringValue;
    }

    public void setProgres(float value){
        bubbleSeekBar.setProgress(value);
    }

}
