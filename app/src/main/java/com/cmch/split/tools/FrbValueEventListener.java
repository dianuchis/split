package com.cmch.split.tools;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;

public abstract class FrbValueEventListener implements ValueEventListener {
    @Override
    public void onDataChange(DataSnapshot dataSnapshot) {
        onDateChangeOrError(dataSnapshot, null);
    }

    @Override
    public void onCancelled(DatabaseError databaseError) {
        onDateChangeOrError(null, databaseError);
    }

    public abstract void onDateChangeOrError(DataSnapshot dataSnapshot, DatabaseError databaseError);

}
