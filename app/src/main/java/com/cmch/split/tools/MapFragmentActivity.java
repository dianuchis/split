package com.cmch.split.tools;

import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.ContextCompat;

import com.cmch.split.SplitApp;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.places.GeoDataClient;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

/**
 * Created by alex on 14/10/17.
 */
public abstract class MapFragmentActivity extends FragmentActivity implements GoogleApiClient.ConnectionCallbacks {

    private static GoogleApiClient mGoogleApiClient;
    protected GoogleMap map;
    protected int fragmentMapId;
    protected Context _this;
    protected LocationManager locationManager;
    private LatLng latLng;


    public static GoogleApiClient getmGoogleApiClient() {
        return mGoogleApiClient;
    }

    private OnMapReadyCallback onMapReady = new OnMapReadyCallback() {
        @Override
        public void onMapReady(GoogleMap googleMap) {
            map = googleMap;
            if (map != null) {
                setUpMap();
            }
            OnMapReady();
        }
    };

    private LocationListener locationListener = new LocationListener() {
        @Override
        public void onLocationChanged(Location location) {
            OnLocationChanged(location);
        }
    };


    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient
                .Builder(this)
                .addConnectionCallbacks(this)
                .addApi(LocationServices.API)
                .addApi(Places.GEO_DATA_API)
                .addApi(Places.PLACE_DETECTION_API)
                .build();
    }

    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        _this = this;
        //locationManager = (LocationManager)getSystemService(Context.LOCATION_SERVICE);
        locationManager = SplitApp.getLocationManager();
    }

    private void setUpMap() {
        map.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED)
            map.setMyLocationEnabled(true);
    }

    private void setUpMapIfNeeded() {
        if (map == null) {
            ((SupportMapFragment) getSupportFragmentManager().findFragmentById(fragmentMapId)).getMapAsync(onMapReady);
        }
    }

    @Override
    public void onConnected(Bundle bundle) {
        new FineLocationNeeds() {
            @Override
            public void toDo() {
                //LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, locationListener);
                LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, SplitApp.getLocationRequest(), locationListener);
            }
        }.execute(this);

        OnConnected(bundle);
    }

    @Override
    public void onConnectionSuspended(int i) {}

    @Override
    public void onResume(){
        super.onResume();
        if (mGoogleApiClient == null || !mGoogleApiClient.isConnected()){
            buildGoogleApiClient();
            mGoogleApiClient.connect();
        }
        setUpMapIfNeeded();
    }

    public Marker addMarker(LatLng latLng, String title){
        return map.addMarker(new MarkerOptions().position(latLng).title(title));
    }

    public void centerInBounds(LatLngBounds latLngBounds){
        map.moveCamera(CameraUpdateFactory.newLatLngBounds(latLngBounds, 50));
    }

    public void center(LatLng latLng){
        if (latLng != null)
            map.moveCamera(CameraUpdateFactory.newLatLng(latLng));
    }

    public void zoom(float zoomValue){
        map.animateCamera(CameraUpdateFactory.zoomTo(zoomValue));
    }

    public Circle addCirce(LatLng latLng, double radius){
        return map.addCircle(new CircleOptions().center(latLng).radius(radius).strokeWidth(0f).fillColor(0x550000FF));
    }

    public LatLng getMylocation(){
        //final LatLng result = null;
        latLng = null;

        new FineLocationNeeds(){
            @Override
            public void toDo() {
                Location myLocation = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
                if (myLocation == null) {
                    Criteria criteria = new Criteria();
                    criteria.setAccuracy(Criteria.ACCURACY_COARSE);
                    String provider = locationManager.getBestProvider(criteria, true);
                    myLocation = locationManager.getLastKnownLocation(provider);
                }
                if (myLocation != null)
                    latLng = new LatLng(myLocation.getLatitude(), myLocation.getLongitude());
            }
        }.execute(this);

        return latLng;
    }

    public abstract void OnConnected(Bundle bundle);

    public abstract void OnLocationChanged(Location location);

    public abstract void OnMapReady();

}
