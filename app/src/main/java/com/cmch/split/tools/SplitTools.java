package com.cmch.split.tools;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.Log;
import android.view.WindowManager;

import com.cmch.split.SplitApp;
import com.squareup.picasso.Transformation;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import com.cmch.split.modeltools.DateForDatePicker;
import com.cmch.split.modeltools.TimeForTimePicker;

/**
 * Created by alex on 28/11/17.
 */

public class SplitTools {


    public static double calculatePrice (double km, double min){
        //Estas son las variables de reglas de negocio
        double minfactor = 0.75; //Factor de relación precio entre min y km
        double adjfactor = 0.97;  //Factor de ajuste por incremento unidad de precio
        double minprice = 20; //Precio mínimo por Viajero

        //The formula creates a double adjustment of the price to minimize variance and "soften" the price curve in long trips
        double priceunit = ((km + (min * minfactor))/(1+minfactor));
        double adjpriceunit = priceunit*(adjfactor+(priceunit/1000));
        double tripprice = adjpriceunit / (adjfactor+((adjpriceunit/10)/100));

        if(tripprice < minprice){
            return minprice;
        } else {
            return tripprice;
        }
    }

    //Quita el focus automatico de la entrada de texto en el teclado
    public static void hideKeyboard(Activity activity){
        activity.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
    }

    //Obtiene el nombre corto de una direccion (Address)proveniente de un PointLocation
    public static String getShortAddress(String address){
        String result = "";

        String[] tmpAddresses = address.split(",");

        /*
        if (tmpAddresses.length == 1){
            result = tmpAddresses[0];
        }*/
        if (tmpAddresses.length >=1 && tmpAddresses.length <= 5){
            result = tmpAddresses[1];
        }
        if (tmpAddresses.length > 5){
            result = tmpAddresses[2];
        }

        return result;
    }

    public static String getShortName(String address){

        int contSpace=0;
        int inicio=0;
        int fin=0;
        String newShortName;

        for(int i = 0; i < address.length(); i++){
            char x = address.charAt(i);
            if(Character.toString(x).equals(" ")){
                contSpace++;
                if (contSpace == 2){
                    fin  = i;
                }
            }
        }

        newShortName = address.substring(inicio, fin);
        return  newShortName;
    }

    //Aplica una transformacion circular a la imagen del sidebar
    private static class CircleTransform implements Transformation {
        @Override
        public Bitmap transform(Bitmap source) {
            int size = Math.min(source.getWidth(), source.getHeight());

            int x = (source.getWidth() - size) / 2;
            int y = (source.getHeight() - size) / 2;

            Bitmap squaredBitmap = Bitmap.createBitmap(source, x, y, size, size);
            if (squaredBitmap != source) {
                source.recycle();
            }

            Bitmap bitmap = Bitmap.createBitmap(size, size, source.getConfig());

            Canvas canvas = new Canvas(bitmap);
            Paint paint = new Paint();
            BitmapShader shader = new BitmapShader(squaredBitmap,
                    BitmapShader.TileMode.CLAMP, BitmapShader.TileMode.CLAMP);
            paint.setShader(shader);
            paint.setAntiAlias(true);

            float r = size / 2f;
            canvas.drawCircle(r, r, r, paint);

            squaredBitmap.recycle();
            return bitmap;
        }
        @Override
        public String key() {
            return "circle";
        }
    }

    //Regresa la instancia de una nueva clase Circle Transform
    public static CircleTransform newInstaceCircleTransform(){
        CircleTransform circleTransform = new CircleTransform();

        return circleTransform;
    }

    public static String formatDate(int YY, int MM, int DD) {

        String year = String.valueOf(YY);
        String month = String.valueOf(MM);
        String day = String.valueOf(DD);

        if(MM < 10){
            month = "0" + month;
        }
        if(DD < 10){
            day  = "0" + day ;
        }

        Log.d("DATE: ", day+month+year);

        //%s devuelve un string formateado
        return String.format("%s / %s / %s", day, month, year);
    }


    //Regresa un formato de fecha en el formato Date
    public static String formatDateString(Date date) {
        // E devuelve el nombre abreviado del dia de la semana.
        // MMM devuelve el nombre abreviado del mes.
        // yy devuelve el año formateado con dos dígitos.
        SimpleDateFormat format = new SimpleDateFormat("E, MMM yy", SplitApp.getResourceSplit().getConfiguration().locale);
        String DateToStr = format.format(date);
        System.out.println(DateToStr);


        return DateToStr;
    }

    //Regresa la fecha minima para un DatePicker
    public static DateForDatePicker getDate(){
        Date today = new Date();
        Calendar c = Calendar.getInstance();
        c.setTime(today);

        int year        = c.get(Calendar.YEAR);
        int month       = c.get(Calendar.MONTH);
        int dayOfMonth = c.get(Calendar.DAY_OF_MONTH);
        long minDate = c.getTime().getTime();

        DateForDatePicker dateForDatePicker = new DateForDatePicker();
        dateForDatePicker.setYear(year);
        dateForDatePicker.setMonth(month);
        dateForDatePicker.setDayOfMonth(dayOfMonth);
        dateForDatePicker.setMinDate(minDate);

        return dateForDatePicker;

    }

    //Regresa la hora minima para un TimePicker
    public static TimeForTimePicker getTime(){
        Date today = new Date();
        Calendar calendar2 = Calendar.getInstance();
        //Obtenemos la fecha y hora actual
        calendar2.setTime(today);
        //Asignamos el horario de mañana y tarde
        calendar2.set( Calendar.AM_PM, Calendar.AM_PM);
        int hour = calendar2.get(Calendar.HOUR_OF_DAY);
        int minute = calendar2.get(Calendar.MINUTE);

        TimeForTimePicker tm = new TimeForTimePicker();
        tm.setHour(hour);
        tm.setMinute(minute);

        return tm;
    }

    public static String getCurrentDate(){

        return "";
    }

    public static String getCurrentTime(){

        return "";
    }

}
