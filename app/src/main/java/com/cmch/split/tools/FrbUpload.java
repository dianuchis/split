package com.cmch.split.tools;

import android.graphics.Bitmap;
import android.support.annotation.NonNull;

import com.cmch.split.SplitApp;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.ByteArrayOutputStream;

/**
 * Created by alex on 14/01/18.
 */

public abstract class FrbUpload {

    UploadTask uploadTask = null;

    private byte[] data;
    private String path;

    public abstract void OnStart();

    public abstract void OnSuccesOrError(Exception exception, UploadTask.TaskSnapshot taskSnapshot);

    public byte[] getData() {
        return data;
    }

    public void setData(byte[] data) {
        this.data = data;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public FrbUpload(){

    }

    public void start(){
        StorageReference storageReference = SplitApp.getFirebaseStorage().getReference().child(path);
        uploadTask = storageReference.putBytes(data);
        OnStart();
        uploadTask.addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception exception) {
                OnSuccesOrError(exception, null);
            }
        }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                OnSuccesOrError(null, taskSnapshot);
            }
        });
    }

}
