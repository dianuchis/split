package com.cmch.split.tools;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.widget.Toast;

import com.cmch.split.ActivityUserDocument;
import com.cmch.split.SplitApp;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.FirebaseAuthInvalidUserException;

/**
 * Created by alex on 12/01/18.
 */

public abstract class EmailAuth {

    Activity activity;

    public abstract void OnSuccess(@NonNull Task<AuthResult> task);

    public abstract void OnFailure(Exception exception);


    public EmailAuth(Activity activity){
        this.activity = activity;
    }

    public void signInWithEmail(String email, String password){
        SplitApp.getFrbAuth().signInWithEmailAndPassword(email, password).addOnCompleteListener(activity, new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if (task.isSuccessful()) {
                    OnSuccess(task);
                } else {
                    OnFailure(task.getException());
                }
            }
        });
    }

    public void createUserWithEmail(String email, String password){
        SplitApp.getFrbAuth().createUserWithEmailAndPassword(email, password).addOnCompleteListener(activity, new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if (task.isSuccessful()) {
                    OnSuccess(task);
                } else {
                    OnFailure(task.getException());
                }
            }
        });
    }

}
