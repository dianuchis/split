package com.cmch.split.tools;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONObject;

public abstract class SplitJSONRequest {

    private JsonObjectRequest jsonObjectRequest;

    public SplitJSONRequest(String url, final JSONObject jsonObject) {
        jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, url, jsonObject, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                onResponseOrError(response, null);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                onResponseOrError(null, error);
            }
        });
    }

    public JsonObjectRequest getRequest(){
        return jsonObjectRequest;
    }

    public abstract void onResponseOrError(JSONObject response, VolleyError error);

}
