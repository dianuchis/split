package com.cmch.split.tools;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;

import com.cmch.split.ActivityAddVehicle;
import com.cmch.split.ActivityHome;
import com.cmch.split.ActivityRegister;
import com.cmch.split.ActivityUserDocument;
import com.cmch.split.R;
import com.cmch.split.SplitApp;

/**
 * Created by CC on 15-Feb-18.
 */

public class SimpleAlertDialog {

    private AlertDialog dialog;

    public SimpleAlertDialog(final Context context, String tittle, String message,
                             final Class<? extends Activity> Activity1, final Class<? extends Activity> Activity2)
    {

        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage(message);
        builder.setTitle(tittle);
        builder.setPositiveButton(SplitApp.getResourceSplit().getString(R.string.lbl_positive), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent a = new Intent(context, Activity1);
                context.startActivity(a);
            }
        });
        builder.setNegativeButton(SplitApp.getResourceSplit().getString(R.string.lbl_negative), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent b = new Intent(context, Activity2);
                context.startActivity(b);
            }
        });
        dialog = builder.create();
    }

    public void show() {dialog.show();}



}
