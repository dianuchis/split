package com.cmch.split.tools;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.widget.EditText;
import android.widget.TextView;

import com.cmch.split.R;
import com.cmch.split.SplitApp;


/**
 * Created by CC on 17-Mar-18.
 */

public class ValidateField {

    public static boolean fieldSize(EditText field, int size){
        if(field.getText().length() > size || field.length() == 0){
            //si esta mal el campo devolvemos false y pintamos de rojo el campo
            setRed(field);
            return false;
        } else{
            //pintamos de verde el campo
            setGreen(field);
            return true;
        }
    }

    public static void setGreen(EditText field){
        field.setBackground(SplitApp.getResourceSplit().getDrawable(R.drawable.edit_text_green_border));
    }

    public static void setRed(EditText field){
        field.setBackground(SplitApp.getResourceSplit().getDrawable(R.drawable.edit_text_red_border));
    }

    public static boolean exactSize(EditText field, int size){
        if(field.getText().length() == size){
            //Si el campo tiene el tamaño exacto pintamos de verde
            setGreen(field);
            return true;
        } else{
            //Pintamos el campo de rojo
            setRed(field);
            return false;
        }
    }

    public static boolean fieldMaxMin(EditText field, int max, int min){
        if( field.length() >= min && field.getText().length() <= max ){
            //pintamos de verde el campo
            setGreen(field);
            return true;
        } else{
            //si esta mal el campo devolvemos false y pintamos de rojo el campo
            setRed(field);
            return false;
        }
    }

    public static boolean fieldAtLeastFilled(TextView field){
        if( field.length() > 0){
            //pintamos de verde el campo
            field.setBackground(SplitApp.getResourceSplit().getDrawable(R.drawable.edit_text_green_border));
            return true;
        } else{
            //si esta mal el campo devolvemos false y pintamos de rojo el campo
            field.setBackground(SplitApp.getResourceSplit().getDrawable(R.drawable.edit_text_red_border));
            return false;
        }
    }


    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    public static boolean textContent(TextView field, String textContent){
        if( !(field.getText().toString().equals(textContent)) ){
            //Si no tiene el contenido indicado
            //pintamos verde y regresamos true
            field.setBackground(SplitApp.getResourceSplit().getDrawable(R.drawable.edit_text_green_border));
            return true;
        } else{
            //si contiene lo indicado devolvemos false y pintamos de rojo el campo
            field.setBackground(SplitApp.getResourceSplit().getDrawable(R.drawable.edit_text_red_border));
            return false;
        }
    }
}


