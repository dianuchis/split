package com.cmch.split.tools;

import android.app.TimePickerDialog;
import android.content.Context;
import android.widget.TimePicker;

import java.text.SimpleDateFormat;
import java.util.Calendar;


public abstract class SPTimeDialog {

    private Context context;
    String timeFormat = "hh:mm";

    public SPTimeDialog(Context pContext){
        this.context = pContext;
    }

    public SPTimeDialog(Context pContext, String timeFormat){
        this.context = pContext;
        this.timeFormat = timeFormat;
    }

    public void execute(){
        Calendar calendar = Calendar.getInstance();
        int hour = calendar.get(Calendar.HOUR);
        int minutes = calendar.get(Calendar.MINUTE);

        new TimePickerDialog(context, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int pHour, int pMinutes) {
                Calendar tmpCalendar = Calendar.getInstance();
                tmpCalendar.set(Calendar.HOUR, pHour);
                tmpCalendar.set(Calendar.MINUTE, pMinutes);
                SimpleDateFormat dateFormat = new SimpleDateFormat(timeFormat);

                toDo(pHour, pMinutes, dateFormat.format(tmpCalendar.getTime()));
            }
        }, hour, minutes, false).show();

    }

    public abstract void toDo(int hour, int minutes, String formatedTime);

}
