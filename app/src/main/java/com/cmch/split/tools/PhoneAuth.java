package com.cmch.split.tools;

import android.app.Activity;
import android.support.annotation.NonNull;
import com.cmch.split.SplitApp;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseException;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;

import java.util.concurrent.TimeUnit;

/**
 * Created by alex on 12/01/18.
 */

public abstract class PhoneAuth {

    private Activity activity;

    private PhoneAuthProvider.OnVerificationStateChangedCallbacks verificationCallbacks;

    private PhoneAuthProvider.ForceResendingToken resendingToken;

    public abstract void OnVerificationCompleted(PhoneAuthCredential phoneAuthCredential);

    public abstract void OnVerificationFailed(Exception exception);

    public abstract void OnCodeSent(String verificationId, PhoneAuthProvider.ForceResendingToken resendingToken);

    public abstract void OnSignInSucces(@NonNull Task<AuthResult> task);

    public abstract void OnSignInFailure(Exception exception);

    public abstract void OnCodeAutoRetrievalTimeOut(String s);

    public PhoneAuth(Activity activity){

        this.activity = activity;

        verificationCallbacks = new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {
            @Override
            public void onVerificationCompleted(PhoneAuthCredential phoneAuthCredential) {
                OnVerificationCompleted(phoneAuthCredential);
                signInWithPhoneAuthCredential(phoneAuthCredential);
            }

            @Override
            public void onVerificationFailed(FirebaseException exception) {
                OnVerificationFailed(exception);
            }

            @Override
            public void onCodeSent(String verificationId, PhoneAuthProvider.ForceResendingToken resendingToken){
                OnCodeSent(verificationId, resendingToken);
            }

            @Override
            public void onCodeAutoRetrievalTimeOut(String s) {
                super.onCodeAutoRetrievalTimeOut(s);
                OnCodeAutoRetrievalTimeOut(s);
            }
        };
    }


    public void sendCode(String phoneNumber) {
        PhoneAuthProvider.getInstance().verifyPhoneNumber(
                phoneNumber,        // Phone number to verify
                60,                 // Timeout duration
                TimeUnit.SECONDS,   // Unit of timeout
                activity,               // Activity (for callback binding)
                verificationCallbacks);
    }

    public void verifyCode(String phoneVerificationId, String textCode) {
        PhoneAuthCredential credential =
                PhoneAuthProvider.getCredential(phoneVerificationId, textCode);
        signInWithPhoneAuthCredential(credential);
    }

    public void resendVerificationCode(String phoneNumber,
                                        PhoneAuthProvider.ForceResendingToken token) {
        PhoneAuthProvider.getInstance().verifyPhoneNumber(
                phoneNumber,        // Phone number to verify
                60,                 // Timeout duration
                TimeUnit.SECONDS,   // Unit of timeout
                activity,               // Activity (for callback binding)
                verificationCallbacks,         // OnVerificationStateChangedCallbacks
                token);             // ForceResendingToken from callbacks
    }

    private void signInWithPhoneAuthCredential(final PhoneAuthCredential phoneAuthCredential) {
        SplitApp.getFrbAuth().signInWithCredential(phoneAuthCredential)
                .addOnCompleteListener(activity, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            OnSignInSucces(task);
                        } else {
                            OnSignInFailure(task.getException());
                        }
                    }
                });
    }

    public PhoneAuthProvider.ForceResendingToken getResendingToken() {
        return resendingToken;
    }
}
