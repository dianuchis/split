package com.cmch.split.tools;

import android.app.DatePickerDialog;
import android.content.Context;
import android.widget.DatePicker;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

/**
 * Created by alex on 09/12/17.
 */

public abstract class SPDateDialog {

    private Context context;
    String dateFormat = "YYYY-MM-dd";

    public SPDateDialog(Context pContext){
        context = pContext;
    }

    public SPDateDialog(Context pContext, String dateFormat){
        this.dateFormat = dateFormat;
    }

    public abstract void toDo(int day, int month, int year, String formatedDate);

    public void execute(){
        final Calendar calendar = Calendar.getInstance();
        int day = calendar.get(Calendar.DAY_OF_MONTH);
        int month = calendar.get(Calendar.MONTH);
        int year = calendar.get(Calendar.YEAR);

        DatePickerDialog datePickerDialog = new DatePickerDialog(context, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int pYear, int pMonth, int pDay) {
                Date date = new GregorianCalendar(pYear, pMonth, pDay).getTime();
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat(dateFormat);
                toDo(pDay, pMonth, pYear, simpleDateFormat.format(date));
            }
        },year, month, day);
        datePickerDialog.show();
    }

}
