package com.cmch.split.tools;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Iterator;

public class SPJSONObject {

    JSONObject jsonObject = null;

    public SPJSONObject(){
        jsonObject = new JSONObject();
    }

    public SPJSONObject(JSONObject jsonObject){
        this.jsonObject = jsonObject;
    }

    public SPJSONObject(String jsonString){
        try {
            jsonObject = new JSONObject(jsonString);
        } catch (JSONException e) {
            e.printStackTrace();

        }
    }

    public void put(String name, int value){
        try {
            jsonObject.put(name, value);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void put(String name, long value){
        try {
            jsonObject.put(name, value);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void put(String name, double value){
        try {
            jsonObject.put(name, value);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void put(String name, Object value){
        try {
            jsonObject.put(name, value);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void put(String name, boolean value){
        try {
            jsonObject.put(name, value);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public JSONObject get(){
        return jsonObject;
    }

            /*
    public Object get(String name){
       Object result = null;
        try {
            result = jsonObject.get(name);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return  result;
    }*/

    public Boolean getBoolean(String name){
        Boolean result = null;

        try{
            result = jsonObject.getBoolean(name);
        }catch (JSONException e){
            e.printStackTrace();
        }

        return result;
    }

    public Double getDouble(String name){
        Double result = null;

        try{
            result = jsonObject.getDouble(name);
        }catch (JSONException e){
            e.printStackTrace();
        }

        return result;
    }

    public Integer getInt(String name){
        Integer result = null;

        try{
            result = jsonObject.getInt(name);
        }catch (JSONException e){
            e.printStackTrace();
        }

        return result;
    }

    public Long getlong(String name){
        Long result = null;

        try{
            result = jsonObject.getLong(name);
        }catch (JSONException e){
            e.printStackTrace();
        }

        return result;
    }

    public String getString(String name){
        String result = null;

        try{
            result = jsonObject.getString(name);
        }catch (JSONException e){
            e.printStackTrace();
        }

        return result;
    }

    public JSONArray getJSONArray(String name){
        JSONArray result = null;

        try{
            result = jsonObject.getJSONArray(name);
        }catch (JSONException e){
            e.printStackTrace();
        }

        return result;
    }

    public JSONObject getJSONObject(String name){
        JSONObject result = null;

        try{
            result = jsonObject.getJSONObject(name);
        }catch (JSONException e){
            e.printStackTrace();
        }

        return result;
    }

    public Object remove(String name){
        return  jsonObject.remove(name);
    }

    public Iterator<String> keys(){
        return jsonObject.keys();
    }

    @Override
    public String toString(){
        String result = null;
        try{
            result = jsonObject.toString();
        }catch (Exception e){
            e.printStackTrace();
        }
        return result;
    }

}
