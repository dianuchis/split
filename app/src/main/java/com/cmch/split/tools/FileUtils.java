package com.cmch.split.tools;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.Debug;

import java.net.URISyntaxException;

/**
 * Created by alex on 26/12/17.
 */

public class FileUtils {

    public static String getPath(Context context, Uri uri)  {
        if ("content".equalsIgnoreCase(uri.getScheme())) {
            String[] projection = { "_data" };
            Cursor cursor = null;

            try {
                cursor = context.getContentResolver().query(uri, projection, null, null, null);
                int column_index = cursor.getColumnIndexOrThrow("_data");
                if (cursor.moveToNext()) {
                    String path = cursor.getString(column_index);
                    return cursor.getString(column_index);
                }
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        }
        else if ("file".equalsIgnoreCase(uri.getScheme())) {
            return uri.getPath();
        }

        return null;
    }

}
