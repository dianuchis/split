package com.cmch.split.tools;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.support.v4.content.ContextCompat;

/**
 * Created by alex on 14/10/17.
 */
public abstract class FineLocationNeeds {

    public abstract void toDo();

    public void execute(Context context){
        if (ContextCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED){
            toDo();
        }
    }

}
