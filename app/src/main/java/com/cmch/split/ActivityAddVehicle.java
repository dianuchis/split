package com.cmch.split;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.cmch.split.adapters.SpinnerStringAdapter;
import com.cmch.split.baseactivity.ActivitySplit;
import com.cmch.split.dbref.Vehicle;
import com.cmch.split.tools.SplitTools;
import com.cmch.split.tools.ValidateField;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by alex on 27/11/17.
 */

public class ActivityAddVehicle extends AppCompatActivity implements View.OnClickListener, android.text.TextWatcher {

    private boolean brand = false, model = false, state = false, year = false, color = false, plates = false, avaliableSeats = false,
    insurance = false, insurancePolicy = false, serialNumber = false, policyTermins= false;

    private boolean isCheked = false;

    private Spinner spinner_AddVehicle_Brand;
    private EditText editText_AddVehicle_Model;
    private Spinner spinner_AddVehicle_State;
    private Spinner spinner_AddVehicle_Year;
    private Spinner spinner_AddVehicle_Color;
    private EditText editText_AddVehicle_Plates;
    private Spinner spinner_AddVehicle_Seats;
    private Spinner spinner_AddVehicle_Insurance;
    private EditText editText_AddVehicle_PolicyNumber;
    private EditText editText_AddVehicle_SerialNumber;
    private CheckBox checkBox_AddVehicle_AccepConditions;
    private Button button_AddVehicle_Save;

    private SpinnerStringAdapter brandsAdapter;
    private SpinnerStringAdapter statesAdapter;
    private SpinnerStringAdapter yearsAdapter;
    private SpinnerStringAdapter colorsAdapter;
    private ArrayAdapter<CharSequence> seatsAdapter;
    private SpinnerStringAdapter companiesAdapter;

    private String callingActivity;

    //String[] years = {"2018", "2017", "2016", "2015"};

    private static List<Vehicle> newUserVehicles;

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_vehicle);

        SplitTools.hideKeyboard(this);

        if(getIntent().getExtras() != null){
            callingActivity = getIntent().getExtras().getString("callingActivity");
        }

        spinner_AddVehicle_Brand = findViewById(R.id.spinner_AddVehicle_Brand);
        editText_AddVehicle_Model = findViewById(R.id.editText_AddVehicle_Model);
        editText_AddVehicle_Model.addTextChangedListener(this);
        spinner_AddVehicle_State = findViewById(R.id.spinner_AddVehicle_State);
        spinner_AddVehicle_Year = findViewById(R.id.spinner_AddVehicle_Year);
        spinner_AddVehicle_Color =  findViewById(R.id.spinner_AddVehicle_Color);
        editText_AddVehicle_Plates = findViewById(R.id.editText_AddVehicle_Plates);
        editText_AddVehicle_Plates.addTextChangedListener(this);
        spinner_AddVehicle_Seats =  findViewById(R.id.spinner_AddVehicle_Seats);
        spinner_AddVehicle_Insurance = findViewById(R.id.spinner_AddVehicle_Insurance);
        editText_AddVehicle_PolicyNumber = findViewById(R.id.editText_AddVehicle_PolicyNumber);
        editText_AddVehicle_PolicyNumber.addTextChangedListener(this);
        editText_AddVehicle_SerialNumber = findViewById(R.id.editText_AddVehicle_SerialNumber);
        editText_AddVehicle_SerialNumber.addTextChangedListener(this);
        checkBox_AddVehicle_AccepConditions = findViewById(R.id.checkBox_AddVehicle_AcceptConditions);
        button_AddVehicle_Save = findViewById(R.id.button_AddVehicle_Save);

        brandsAdapter = new SpinnerStringAdapter(this, SplitApp.getBrands());
        spinner_AddVehicle_Brand.setAdapter(brandsAdapter);

        spinner_AddVehicle_Brand.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                brand = true;
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {}
        });

        String countryName;

        if(callingActivity != null){
            countryName = ActivitySplit.tmpUser.getCountry();
        }else{ countryName = SplitApp.getAppUser().getCountry();}

        statesAdapter = new SpinnerStringAdapter(this, SplitApp.getStates(countryName));
        spinner_AddVehicle_State.setAdapter(statesAdapter);

        spinner_AddVehicle_State.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                state = true;
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {}
        });

        yearsAdapter = new SpinnerStringAdapter(this, SplitApp.getYears());
        spinner_AddVehicle_Year.setAdapter(yearsAdapter);

        spinner_AddVehicle_Year.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                year = true;
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {}
        });

        colorsAdapter = new SpinnerStringAdapter(this, SplitApp.getColors());
        spinner_AddVehicle_Color.setAdapter(colorsAdapter);

        spinner_AddVehicle_Color.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                color = true;
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {}
        });

        seatsAdapter = ArrayAdapter.createFromResource(this, R.array.AvailableSeats, android.R.layout.simple_spinner_item);
        seatsAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_AddVehicle_Seats.setAdapter(seatsAdapter);

        spinner_AddVehicle_Seats.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                avaliableSeats = true;
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {}
        });
        companiesAdapter = new SpinnerStringAdapter(this, SplitApp.getStates(countryName));
        spinner_AddVehicle_Insurance.setAdapter(companiesAdapter);

        spinner_AddVehicle_Insurance.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                insurance = true;
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {}
        });

        button_AddVehicle_Save.setOnClickListener(this);

        checkBox_AddVehicle_AccepConditions.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                //Cambia la bandera de validacion para las politicas del usuario
                //a true cuando se da clic en el checkbox de politicas de usuario.
                if (compoundButton.isChecked()){
                    policyTermins = true;
                }
            }
        });

        checkBox_AddVehicle_AccepConditions.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isCheked = !isCheked;
                checkBox_AddVehicle_AccepConditions.setChecked(isCheked);
            }
        });

        //Agrega la funcionalidad para abrir un vinculo, al dar clic a politicas del usuario
        TextView textView_Register_EndUserPolicy = findViewById(R.id.textView_Register_EndUserPolicy);
        textView_Register_EndUserPolicy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Agrega un intent de tipo Browsable para abrir
                //el URL http://www.s-plit.com/terminos cuando se da clic al texto
                //de politicas de usuario final
                Intent intent = new Intent();
                intent.setAction(Intent.ACTION_VIEW);
                intent.addCategory(Intent.CATEGORY_BROWSABLE);
                intent.setData(Uri.parse("http://www.s-plit.com/terms.html"));
                startActivity(intent);
            }
        });

        newUserVehicles = new ArrayList<>();
    }

    @Override
    protected void onResume() {
        super.onResume();

        //Limpia todos los campos del formulario
        spinner_AddVehicle_Brand.setSelection(0);
        editText_AddVehicle_Model.setText("");
        spinner_AddVehicle_State.setSelection(0);
        spinner_AddVehicle_Year.setSelection(0);
        spinner_AddVehicle_Color.setSelection(0);
        editText_AddVehicle_Plates.setText("");
        //spinner_AddVehicle_Seats.setSelection(0);
        spinner_AddVehicle_Insurance.setSelection(0);
        editText_AddVehicle_PolicyNumber.setText("");
        editText_AddVehicle_SerialNumber.setText("");
        checkBox_AddVehicle_AccepConditions.setChecked(false);
    }

    private void addVehicle(){
        Vehicle vehicle = new Vehicle();
        String userId = SplitApp.getFrbUserId();
        String vehicleId = SplitApp.getFrbDatabase().child("vehicles").push().getKey();

        vehicle.setId(vehicleId);
        vehicle.setOwner(userId);
        vehicle.setBrand(spinner_AddVehicle_Brand.getSelectedItem().toString());
        vehicle.setModel(editText_AddVehicle_Model.getText().toString());
        vehicle.setState(spinner_AddVehicle_State.getSelectedItem().toString());
        vehicle.setYear(spinner_AddVehicle_Year.getSelectedItem().toString());
        vehicle.setColor(spinner_AddVehicle_Color.getSelectedItem().toString());
        vehicle.setPlates(editText_AddVehicle_Plates.getText().toString());
        vehicle.setAvailableSeats(spinner_AddVehicle_Seats.getSelectedItem().toString());
        vehicle.setInsuranceCompany(spinner_AddVehicle_Insurance.getSelectedItem().toString());
        vehicle.setInsurancePolicyNumber(editText_AddVehicle_PolicyNumber.getText().toString());
        vehicle.setSerialNumber(editText_AddVehicle_SerialNumber.getText().toString());

            newUserVehicles.add(vehicle);
    }

    public static List<Vehicle> getNewUserVehicles(){
        return newUserVehicles;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.button_AddVehicle_Save:
                if (brand&& model && state && year && color && plates && avaliableSeats && insurance && insurancePolicy && serialNumber && policyTermins){
                    addVehicle();
                    Intent intentAddVehicleSummary = new Intent(view.getContext(), ActivityAddVehicleSumary.class);
                    if(callingActivity != null){
                        intentAddVehicleSummary.putExtra("callingActivity2", "TabProfileOptions");
                    }
                    startActivity(intentAddVehicleSummary);
                   // finish();
                    Toast.makeText(this, getString(R.string.lbl_Register_Message_Added), Toast.LENGTH_LONG).show();
                }else{
                    Toast.makeText(this, getString(R.string.lbl_Register_Message_Verify), Toast.LENGTH_LONG).show();
                }
                break;
        }
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        model = ValidateField.fieldMaxMin(editText_AddVehicle_Model, 20,5);
        plates = ValidateField.exactSize(editText_AddVehicle_Plates, 7);
        insurancePolicy = ValidateField.fieldMaxMin(editText_AddVehicle_PolicyNumber,30,10);
        serialNumber = ValidateField.fieldMaxMin(editText_AddVehicle_SerialNumber, 30, 10);
    }

    @Override
    public void afterTextChanged(Editable s) {}
}
