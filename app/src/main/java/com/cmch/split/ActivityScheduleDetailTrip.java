package com.cmch.split;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.cmch.split.dbref.Trip;
import com.cmch.split.dbref.User;
import com.cmch.split.callbacks.CallbackGetSeats;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.GenericTypeIndicator;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Gabriel Polanco on 06/03/2018.
 */

public class ActivityScheduleDetailTrip extends AppCompatActivity {

    private final String TAG = "ActivScheduleDetailTrip";

    private TextView textViewOrigin;
    private TextView textViewDestin;
    private TextView textViewTime;
    private TextView textViewComments;
    private TextView textViewPrice;
    private TextView textViewReserve;
    private Context contextScheduleDetail;

    private CallbackGetSeats callbackGetSeats;

    //Referencia de firebase que apunta al viaje seleccionado.
    DatabaseReference tripReference;

    //Entero que guarda el numero de asientos disponibles en el viaje.
    private int availableSeats;

    //Este objeto guarda el viaje que se selecciono en el resumen de viajes disponibles.
    private Trip tripSelected;

    //booleano que nos permite saber si el pasajero que reservará
    // el viaje actual no se encuentra en la lista de pasajeros.
    private boolean userHasReservedTrip = false;

    //booleano que nos permite saber si el usuario actual que reservará
    //no es conductor del viaje actual, si es asi no se le permite reservar
    //el mismo viaje.
    private boolean userIsDriverOfTrip = false;


    //Guarda la lista de pasajeros actual, para apilar al nuevo pasajero al viaje
    //y sobreescribe la lista actual con una nueva lista y el nuevo pasajero.
    private List<User> currentPassengers;

    @Override
    public View onCreateView(View parent, String name, Context context, AttributeSet attrs) {
        View view = super.onCreateView(parent, name, context, attrs);
        contextScheduleDetail = context;
        return view;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_schedule_detail_trip);

        currentPassengers = new ArrayList<>();

        //Agrego el agente de escucha para el usuario que reservara el viaje
        SplitApp.getFirebaseUser();

        //Obtengo el viaje reservado, proviente del resumen de viajes publicados (recyclerView).
        if(getIntent().getParcelableExtra("trips") != null)
        {
            tripSelected = getIntent().getParcelableExtra("trips");
        }

        textViewOrigin = findViewById(R.id.textView_ScheduleDetail_Origin);
        textViewDestin = findViewById(R.id.textView_ScheduleDetail_Destin);
        textViewTime = findViewById(R.id.textView_ScheduleDetail_Time);
        textViewComments = findViewById(R.id.textView_ScheduleDetail_Comments);
        textViewPrice = findViewById(R.id.textView_ScheduleDetail_Price);
        textViewReserve = findViewById(R.id.textView_ScheduleDetail_Reserve);

        textViewOrigin.setText(tripSelected.getOrigin().asString());
        textViewDestin.setText(tripSelected.getDestination().asString());
        textViewTime.setText(tripSelected.getTime().get(0));
        textViewComments.setText(tripSelected.getOptions());
        textViewPrice.setText(Double.toString(tripSelected.getPrice()));

        textViewReserve.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                AlertDialog dialogIdentification;
                AlertDialog.Builder builder = new AlertDialog.Builder(v.getContext());
                builder.setTitle(R.string.btn_ScheduleTrip_TitleConfirmReserve);
                builder.setMessage(R.string.btn_ScheduleTrip_BodyConfirmReserve);
                builder.setPositiveButton(R.string.lbl_positive, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        toReserve();
                    }
                });
                builder.setNegativeButton(R.string.lbl_negative, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
                dialogIdentification = builder.create();
                dialogIdentification.show();

            }
        });

        //Esta referencia nos devuelve la rama de asientos disponibles en el viaje que se esta reservando.
        tripReference = SplitApp.getFrbDatabase().child("trips")
                .child(tripSelected.getId());


        //Accedemos hasta la rama de usuarios que reservaron viaje
        //esto tambien crea la rama si no existe
        DatabaseReference newReserveRef = SplitApp.getFrbDatabase().child("trips").
                child(tripSelected.getId()).child("passengers");

        final GenericTypeIndicator<List<User>> genericTypeIndicator = new GenericTypeIndicator<List<User>>(){};

        //Verificamos si el usuario no existe en la rama de pasajeros
        newReserveRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                if(dataSnapshot.exists()){

                    //Guarda toda la lista de usuarios actuales en el viaje,
                    //para posteriormente verificar si el usuario que reservará
                    //el viaje, ya se encuentra suscrito al viaje.
                    currentPassengers.addAll(dataSnapshot.getValue(genericTypeIndicator));

                    Log.d(TAG, currentPassengers.toString());

                    //Recorre la lista de pasajeros en firebase
                    for(DataSnapshot passenger: dataSnapshot.getChildren()){

                        //passenger.getValue().toString() imprime: {id=8j7PVQH7TAfmKfCbaUaZ9z0Wfuq1}
                        String id = passenger.child("id").getValue(String.class);
                        //Si el usuario ya se encuentra en la lista de pasajeros cambiamos
                        //la bandera de userHasReservedTrip a true.
                        if(id.equals(SplitApp.getFrbUserId())){
                            userHasReservedTrip = true;
                        }
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    private void toReserve(){

        //Instanciamos un nuevo agente de escucha de un solo evento, para el viaje seleccionado
        //
        tripReference.addListenerForSingleValueEvent( new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if(dataSnapshot.exists()){

                    Trip tripToSchedule = dataSnapshot.getValue(Trip.class);


                    Log.d(TAG, tripToSchedule.toString());

                    if(tripToSchedule.getDriver().getId().equals(SplitApp.getFrbUserId())){
                        userIsDriverOfTrip = true;
                    }

                    //Tomamos los datos de los asientos disponibles
                    availableSeats = tripToSchedule.getSeats();
                    //Guardamos el numero de asientos en el callback
                    callbackGetSeats.onSuccess(availableSeats);

                    //Imprime el número de asientos disponibles en el viaje.
                    Log.d(TAG, "available seats: "+availableSeats+userHasReservedTrip);
                }
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {Log.d(TAG, "ERROR DB"+databaseError.toString());}
        });

        //Este callback nos devolvera el número de asientos disponibles en el viaje
        callbackGetSeats = new CallbackGetSeats() {
            @Override
            public void onSuccess(int seats) {

                //Muestra al usuario si ya reservo el viaje, o es conductor del viaje
                if(userIsDriverOfTrip){
                    Toast.makeText(contextScheduleDetail, "You are the driver of this trip", Toast.LENGTH_LONG).show();
                }else if(userHasReservedTrip){
                    Toast.makeText(contextScheduleDetail,getString(R.string.lbl_ScheduleTrip_MsgTripBooked), Toast.LENGTH_LONG).show();
                }

                //Si los asientos disponibles son al menos 1, y el pasajero no esta registrado
                // en la lista de pasajeros, restamos el asiento y suscribimos al pasajero.
                if(seats >= 1 && !userHasReservedTrip && !userIsDriverOfTrip){
                    //Guardamos los nuevos asientos restandole el requerido por el viajero
                    int newSeats = seats - 1;

                    //Creo un hashmap para actualizar los asientos disponibles
                    Map<String, Object> seatsUpdate = new HashMap<>();
                    seatsUpdate.put("seats", newSeats);

                    //Actualizo los asientos con el nuevo valor de los asientos.
                    //Con la referencia solo del viaje hijo (ej. trips/-L8cxNz..)
                    SplitApp.getFrbDatabase().child("trips")
                            .child(tripSelected.getId()).updateChildren(seatsUpdate);

                    //--------ESTA CONSULTA SUSCRIBE AL PASAJERO AGREGANDOLO A LA RAMA DE PASENGGERS------//
                    String idCurrentUser = SplitApp.getFrbUserId();

                    //Accedemos hasta la rama de usuarios que reservaron viaje
                    //esto tambien crea la rama si no existe
                    DatabaseReference newReserveRef = SplitApp.getFrbDatabase().child("trips").
                            child(tripSelected.getId()).child("passengers/");

                    //La comparacion del if da diferente de null porque se crea
                    //una nueva referencia con el id del usuario
                    if (newReserveRef.child(idCurrentUser) != null) {

                        //Creo un nuevo pasajero con los datos del usuario actual
                        User newTempUser = new User();
                        newTempUser.setId(idCurrentUser);
                        newTempUser.setFullName(SplitApp.getAppUser().getFullName());
                        newTempUser.setCellphone(SplitApp.getAppUser().getCellphone());
                        newTempUser.setEmail(SplitApp.getAppUser().getEmail());
                        newTempUser.setGender(SplitApp.getAppUser().getGender());
                        newTempUser.setCity(SplitApp.getAppUser().getCity());
                        newTempUser.setBirthday(SplitApp.getAppUser().getBirthday());
                        newTempUser.setFcmRegisterToken(SplitApp.getAppUser().getFcmRegisterToken());

                        //Agrego el nuevo pasajero a la lista actual de pasajeros
                        currentPassengers.add(newTempUser);

                        //Agrego un espacio para el usuario guardo el id del viaje y lo inserto en la rama de pasajeros
                        //newReserveRef.push().getKey();
                        newReserveRef.setValue(currentPassengers);

                        Toast.makeText(contextScheduleDetail, "Trip reserved", Toast.LENGTH_LONG).show();
                        finish();
                        startActivity(new Intent(contextScheduleDetail, ActivityHome.class));
                    }
                }else{
                    //Si el usuario no puede reservar el viaje, lo mandamos al fragment
                    //de viajes disponibles
                    finish();
                }
            }
            @Override
            public void onFail(String msg) {}
        };
    }


}
