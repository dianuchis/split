package com.cmch.split;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.cmch.split.dbref.Trip;
import com.cmch.split.adapters.CardViewPublishFrecuentTripsAdapter;
import com.cmch.split.callbacks.GetAvailableTrips;
import com.cmch.split.tools.SingletonVolley;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static com.cmch.split.SplitApp.getAppContext;

/**
 * Created by CC on 05-Mar-18.
 */

public class TabPublishFrecuentTrips extends Fragment {

    private ArrayList<Trip> dataPublishFrecuentTrips;
    private RecyclerView.Adapter cardViewFrecuentTripsAdapter;
    private RecyclerView.LayoutManager layoutManager;
    private RecyclerView recyclerView;
    private String jsonParams;
    private String URL_POST_FRECUENT_TRIPS = "https://us-central1-fir-plit.cloudfunctions.net/app/getFrequentTrip";

    // "https://us-central1-fir-plit.cloudfunctions.net/app/getFrequentTrip";
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

		/* Inflate the layout for this fragment */
        final View view = inflater.inflate(R.layout.frag_tab_frecuent_trips, container, false);
        recyclerView = view.findViewById(R.id.recyclerView_tab_frecuent_Trips);

        try {
            getFrecuentTrips(new GetAvailableTrips() {
                @Override
                public void onSuccess(ArrayList<Trip> frecuentTrips) {
                    dataPublishFrecuentTrips = frecuentTrips;
                    // use a linear layout manager
                    layoutManager = new LinearLayoutManager(view.getContext());
                    recyclerView.setLayoutManager(layoutManager);
                    cardViewFrecuentTripsAdapter = new CardViewPublishFrecuentTripsAdapter(dataPublishFrecuentTrips);
                    recyclerView.setAdapter(cardViewFrecuentTripsAdapter);
                }
                @Override
                public void onFail(String msg) {}
            });
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return view;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    private void getFrecuentTrips(final GetAvailableTrips getAvailableTrips) throws JSONException {

            //idPassenger es utilizado para filtrar todos los viajes que el usuario actual a
            //Guardado como frecuentes.
            jsonParams = "{\"idPassenger\":"+"\""+SplitApp.getFrbUserId()+"\"}";
        dataPublishFrecuentTrips = new ArrayList<>();

            Log.d("URL.------", URL_POST_FRECUENT_TRIPS);

            final RequestQueue requestQueue = SingletonVolley.getInstance(getAppContext()).getRequestQueue();
            requestQueue.start();
            Log.d("PARAMETROS VIAJE FREC: ", jsonParams);


            //Tipo de petición, URL, Parametros en formato JSONObject.
        StringRequest postRequest = new StringRequest(Request.Method.POST, URL_POST_FRECUENT_TRIPS,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        final Gson gson = new Gson();

                        Log.d("TRIPS 2: ",response);

                        //Limpio los viajes frecuentes disponibles
                        dataPublishFrecuentTrips.clear();

                        try {

                            JSONArray jsonArray = new JSONArray(response);
                            Log.d("TRIPS 2: ",response);

                            //   JSONArray jsonArray = new JSONArray(response.toString());
                            //   JSONObject jsOFrecTrips = jsonArray.getJSONObject(0);

                            for(int i = 0; i <= jsonArray.length(); i++){
                                JSONObject trip = jsonArray.getJSONObject(i);

                                Trip tmpTrip = gson.fromJson(trip.toString(), Trip.class);
                                Log.d("VIAJE", tmpTrip.getDate().toString());
                                Log.d("VIAJE2", tmpTrip.toString());
                                //Log.d("VIAJE2", tmp.getUsers().toString());
                                dataPublishFrecuentTrips.add(tmpTrip);

                                getAvailableTrips.onSuccess(dataPublishFrecuentTrips);
                            }

                        } catch (JSONException e){
                            Log.d("JSON error: ", e.toString());
                        }

                        Log.d("TAMAÑO DE VIAJES FREC",dataPublishFrecuentTrips.size()+"");
                        requestQueue.stop();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d("JSON error: ", error.toString());
                    }
                }
        ) {
            // here is params will add to your url using post method
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("idDriver", SplitApp.getFrbUserId());
                return params;
            }
        };
        requestQueue.add(postRequest);

    }

}
