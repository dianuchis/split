package com.cmch.split;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.cmch.split.dbref.FrecuentTrip;
import com.cmch.split.callbacks.CallbackGetFrecuentTrips;
import com.cmch.split.adapters.CardViewFrecuentTripsAdapter;
import com.cmch.split.tools.SplitRequest;

import org.json.JSONException;

import java.util.ArrayList;

/**
 * Created by CC on 05-Mar-18.
 */

public class TabScheduleFrecuentTrips extends Fragment {

    private static final String TAG = "TabScheduleFrecTrips";

    private static ArrayList<FrecuentTrip> datafrecuentTrips;
    private static RecyclerView.Adapter cardViewFrecuentTripsAdapter;
    private RecyclerView.LayoutManager layoutManager;
    private RecyclerView recyclerView;
    private static String URL_POST_FRECUENT_TRIPS = "https://us-central1-fir-plit.cloudfunctions.net/app/frequents";

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        Log.d(TAG,"onAtach");
        //Inicializamos el array para guardar los viajes frecuentes
        datafrecuentTrips = new ArrayList<>();

        //Inicializamos la llamada para obtener los viajes frecuentes
        callGetFrecuentTrips();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(TAG, "onCreate");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        Log.d(TAG, "onCreateView");

		/* Inflate the layout for this fragment */
        final View view = inflater.inflate(R.layout.frag_tab_frecuent_trips, container, false);
        recyclerView = view.findViewById(R.id.recyclerView_tab_frecuent_Trips);
        //Use a linear layout manager
        layoutManager = new LinearLayoutManager(view.getContext());
        recyclerView.setLayoutManager(layoutManager);
        cardViewFrecuentTripsAdapter = new CardViewFrecuentTripsAdapter(datafrecuentTrips);
        recyclerView.setAdapter(cardViewFrecuentTripsAdapter);

        return view;
    }


    //Se ejecuta al cerrar un activity que reemplazo al
    //fragment TabScheduleFrecuentTrips

    //O se ejecuta al momento de crear el activity
    //despues de este ciclo el activity queda activo
    @Override
    public void onResume() {
        super.onResume();

        Log.d(TAG, "onResume");


      //  callGetFrecuentTrips();
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.d(TAG, "onPause");
    }

    //Llama al callback SplitRequest.getFrecuentTrips para que
    //consulte los viajes que ha guardado el usuario como frecuentes
    public static void callGetFrecuentTrips(){
        try {
            //Callback que devuelve los viajes frecuentes de la peticion getFrecuentTrips
            SplitRequest.getFrecuentTrips(new CallbackGetFrecuentTrips() {
                @Override
                public void onSuccess(ArrayList<FrecuentTrip> frecuentTrips) throws JSONException {

                    //Limpiamos los viajes frecuentes anteriores
                    datafrecuentTrips.clear();

                    //Agregamos todos los viajes que obtenimos en el callback
                    datafrecuentTrips.addAll(frecuentTrips);

                    //Si el adaptador para el cardView ya se instancio
                    //notificamos que han cambiado los datos en los viajes frecuentes
                    if(!(cardViewFrecuentTripsAdapter == null)){
                        cardViewFrecuentTripsAdapter.notifyDataSetChanged();
                    }

                }
                @Override
                public void onFail(String msg) {}
            }, TAG, URL_POST_FRECUENT_TRIPS);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


}
