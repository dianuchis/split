package com.cmch.split;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.support.v4.app.Fragment;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.cmch.split.baseactivity.ActivitySplit;
import com.cmch.split.dbref.PointLocation;
import com.cmch.split.dbref.Trip;
import com.cmch.split.dbref.User;
import com.cmch.split.dbref.Vehicle;
import com.cmch.split.callbacks.CallbackGetPrice;
import com.cmch.split.adapters.RecyclerViewLayouverAdapter;
import com.cmch.split.adapters.SpinnerStringAdapter;
import com.cmch.split.adapters.SpinnerVehicleAdapter;
import com.cmch.split.tools.SplitRequest;
import com.cmch.split.tools.SplitTools;
import com.cmch.split.tools.ValidateField;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import org.json.JSONException;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import static com.cmch.split.ActivityPublishTrips.GET_DESTINATION_LOCATION;
import static com.cmch.split.ActivityPublishTrips.GET_LAYOVER_LOCATION;
import static com.cmch.split.ActivityPublishTrips.GET_ORIGIN_LOCATION;
import static com.cmch.split.SplitApp.getAppContext;
import static com.cmch.split.SplitApp.getResourceSplit;

/**
 * Created by kitu on 11/28/17.
 */

public class TabPublishNewTrip extends Fragment implements View.OnClickListener, android.text.TextWatcher {

    //Etiqueta que almacena el nombre del activity,
    //se utiliza para imprimir errores o datos en el Log.
    private final String TAG = "TabPublishNewTrip";

    Trip tmp;
    private String textOrigin;
    private String textDestin;
    private List<String> datesTrip;
    private List<String> timesTrip;
    private TextView textView_PublishNew_From;
    private TextView textView_PublishNew_Destination;
    //private TextView textView_PublishNew_Add;
    private RadioButton radiobtn_PublishNew_Round;
    private RadioButton radiobtn_PublishNew_Single;
    private TextView textView_PublishNew_Date;
    private TextView textView_PublishNew_Time;
    private TextView textView_PublishNew_Date_Return;
    private TextView textView_PublishNew_Time_Return;
    //private TextView textView_PublishNew_Dato;
    private TextView textView_PublishNew_Precio;
    private Spinner spin_PublishNew_Vehicle;
    private Spinner spin_PublishNew_Seats;
    private EditText editText_PublishNew_Details;
    private Spinner spin_PublishNew_Bag;
    private Spinner spin_PublishNew_BagNumber;
    private CheckBox checkbox_PublishNew_AddToFrequents;
    private CheckBox checkbox_PublishNew_AcceptTerms;
    private Button button_PublishNew_Continue;
    private Button button_PublishNew_LessPrice;
    private Button button_PublishNew_MorePrice;
    private int MinPrice = 20;
    public Double calculatedPrice = 20.0;

    //Guarda el precio del viaje
    private String priceTrip;

    private List<Vehicle> vehicles;
    SpinnerVehicleAdapter spinnerVehicleAdapter;
    private SpinnerStringAdapter seatsAdapter;
    private SpinnerStringAdapter baggageTypesAdapter;
    private SpinnerStringAdapter baggageNumberAdapter;

    private RecyclerView recyclerView;
    private static RecyclerView.Adapter recyclerLayouversAdapter;
    private RecyclerView.LayoutManager layoutManager;

    private static LayoutInflater layoutInflater;
    public static LayoutInflater getInflater(){ return layoutInflater; }

    //Cargamos el contenedor que nos devolvera el linear donde pondremos los textView
    private ViewGroup insertPoint;

    private ArrayList<PointLocation> layouvers;

    //Se utilizan para obtener hora y fecha actual
    private Date date = new Date();
    private final Calendar calendar = Calendar.getInstance();
    private int year;
    private int month;
    private int dayOfMonth;
    private int hour;
    private int minutes;
    private TimePickerDialog t;
    private DatePickerDialog d;

    private boolean validateOrigin, validateDestin, validateDateReturn, validateTimeReturn,
            validateLayouvers, validateTripType, validateDate, validateTime, validateDetails,
    validateVehicle, validateAvaliableSeats, validateBaggage, validateBaggagePerPerson, validateUserCond;

    //Agregado 19/02/2018 url para consultar precios
    private String URL_RATE_QUERY = "https://us-central1-fir-plit.cloudfunctions.net/app/getprice";

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View resultView = inflater.inflate(R.layout.frag_tab_publish_new_trip, container, false);
        layoutInflater = inflater;

        //layouvers = new ArrayList<>();



        //Recycler para los layouvers
        /*recyclerView = resultView.findViewById(R.id.recyclerView_PublishNew_Layouvers);
        layoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);*/

        // specify an adapter (see also next example)
        //recyclerLayouversAdapter = new RecyclerViewLayouverAdapter(layouvers);
        //recyclerView.setAdapter(recyclerLayouversAdapter);

        //Utilizado para inflar el linear que contendrá a los textview extra para fecha y hora cuando el viaje es de tipo redondo.
        insertPoint = (ViewGroup) resultView.findViewById(R.id.container_publishNew_Round_date_time);

        tmp = new Trip();
        tmp.setDriver(SplitApp.getAppUser());

        textView_PublishNew_From = resultView.findViewById(R.id.textView_PublishNew_From);
        textView_PublishNew_Destination = resultView.findViewById(R.id.textView_PublishNew_Destination);
        //textView_PublishNew_Add = resultView.findViewById(R.id.textView_PublishNew_Add);
        radiobtn_PublishNew_Round = resultView.findViewById(R.id.radiobtn_PublishNew_Round);
        radiobtn_PublishNew_Single = resultView.findViewById(R.id.radiobtn_PublishNew_Single);
        textView_PublishNew_Date = resultView.findViewById(R.id.textView_PublishNew_Date);
        textView_PublishNew_Time = resultView.findViewById(R.id.textView_PublishNew_Time);
        //textView_PublishNew_Dato = resultView.findViewById(R.id.textView_PublishNew_Dato);
        textView_PublishNew_Precio = resultView.findViewById(R.id.textView_PublishNew_Precio);
        spin_PublishNew_Vehicle = resultView.findViewById(R.id.spin_PublishNew_Vehicle);
        spin_PublishNew_Seats = resultView.findViewById(R.id.spin_PublishNew_Seats);
        editText_PublishNew_Details = resultView.findViewById(R.id.editText_PublishNew_Details);
        spin_PublishNew_Bag = resultView.findViewById(R.id.spin_PublishNew_Bag);
        spin_PublishNew_BagNumber =resultView.findViewById(R.id.spin_PublishNew_BagNumber);
        checkbox_PublishNew_AddToFrequents = resultView.findViewById(R.id.checkbox_PublishNew_AddToFrequents);
        checkbox_PublishNew_AcceptTerms = resultView.findViewById(R.id.checkbox_PublishNew_AcceptTerms);
        button_PublishNew_Continue = resultView.findViewById(R.id.button_Publish_Continue);
        button_PublishNew_LessPrice = resultView.findViewById(R.id.button_PublishNew_LessPrice);
        button_PublishNew_MorePrice = resultView.findViewById(R.id.button_PublishNew_MorePrice);

        //Estado inicial del radioButton para viaje normal
        radiobtn_PublishNew_Single.setChecked(true);
        tmp.setType("Single");
        validateTripType = true;

      radiobtn_PublishNew_Round.setOnClickListener(this);
      radiobtn_PublishNew_Single.setOnClickListener(this);
        checkbox_PublishNew_AddToFrequents.setOnClickListener(this);

        radiobtn_PublishNew_Single.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (radiobtn_PublishNew_Single.isChecked()){ validateTripType = true; tmp.setType("Single");
                    //Removemos la vista que se inserto si el usuario
                    //selecciono el radioButton "Round", esto para evitar duplicidad
                    // de los textView date y time.
                    insertPoint.removeAllViews();
                }
                else{ validateTripType = false; }
            }
        });

        radiobtn_PublishNew_Round.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (radiobtn_PublishNew_Round.isChecked()){ validateTripType = true; tmp.setType("Round");
                //Generamos las vistas adicionales para hora y fecha de viaje redondo.
                generateDateTimeViews();
                }
                else{ validateTripType = false; }
            }
        });

        textView_PublishNew_From.setOnClickListener(this);
        textView_PublishNew_From.addTextChangedListener(this);
        textView_PublishNew_Destination.setOnClickListener(this);
        textView_PublishNew_Destination.addTextChangedListener(this);
        editText_PublishNew_Details.addTextChangedListener(this);
        //textView_PublishNew_Add.setOnClickListener(this);
        textView_PublishNew_Date.setOnClickListener(this);
        textView_PublishNew_Date.addTextChangedListener(this);
        textView_PublishNew_Time.setOnClickListener(this);
        button_PublishNew_LessPrice.setOnClickListener(this);
        button_PublishNew_MorePrice.setOnClickListener(this);
        textView_PublishNew_Time.addTextChangedListener(this);
        textOrigin = textView_PublishNew_From.getText().toString();
        textDestin = textView_PublishNew_Destination.getText().toString();

        //Inicializamos los arrays para hora y fecha de ida y vuelta.
        datesTrip = new ArrayList<>();
        timesTrip = new ArrayList<>();

        seatsAdapter = new SpinnerStringAdapter(getContext(), SplitApp.getSeats());
        spin_PublishNew_Seats.setAdapter(seatsAdapter);

        spin_PublishNew_Seats.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
             validateAvaliableSeats = true;
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {}
        });

        baggageTypesAdapter = new SpinnerStringAdapter(getContext(), SplitApp.getBaggageTypes());
        spin_PublishNew_Bag.setAdapter(baggageTypesAdapter);

        spin_PublishNew_Bag.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                //Si el tipo de viaje seleccionado es no permitido ocultamos el spinner de numero de equipaje
                if(spin_PublishNew_Bag.getSelectedItem().equals(getResourceSplit().getString(R.string.msg_Publish_BaggageNotPermited))){
                    spin_PublishNew_BagNumber.setEnabled(false);
                    spin_PublishNew_BagNumber.setVisibility(View.INVISIBLE);
                    validateBaggage = true;
                }else {
                    //El usuario selecciono un tipo de equipaje

                    //Validamos el equipaje
                    validateBaggage = true;

                    //Activamos el spiner, lo mostramos y llenamos el tipo de viaje seleccionado
                    spin_PublishNew_BagNumber.setEnabled(true);
                    spin_PublishNew_BagNumber.setVisibility(View.VISIBLE);

                    //Agregamos el tipo de equipaje en listener de spinner al viaje
                    tmp.setBaggage(spin_PublishNew_Bag.getSelectedItem().toString());
                }
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {}
        });

        baggageNumberAdapter = new SpinnerStringAdapter(getContext(), SplitApp.getBaggageNumber());
        spin_PublishNew_BagNumber.setAdapter(baggageNumberAdapter);

        spin_PublishNew_BagNumber.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                //Si esta seleccionado el tipo de equipaje no permitido no guardamos el numero de equipaje
                if(spin_PublishNew_Bag.getSelectedItem().equals(getResourceSplit().getString(R.string.msg_Publish_BaggageNotPermited))){
                    validateBaggagePerPerson = true;
                }else{
                    //Si no se encuentra seleccionado entonces guardamos el numero del equipaje
                    validateBaggagePerPerson = true;
                    //Agregamos el equipaje por persona seleccionado
                    tmp.setBaggagePerPerson(Integer.parseInt(spin_PublishNew_BagNumber.getSelectedItem().toString()));
                }

            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {}
        });

        checkbox_PublishNew_AcceptTerms.setChecked(true);


        Log.d("FIREBASE USER", SplitApp.getFrbUserId());
        Query query = SplitApp.getFrbDatabase().child("vehicles").orderByChild("owner").equalTo(SplitApp.getFrbUserId());
        query.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()){
                    vehicles = new ArrayList<>();
                    for(DataSnapshot item: dataSnapshot.getChildren()){
                        Vehicle tmp = item.getValue(Vehicle.class);
                        vehicles.add(tmp);
                    }
                    spinnerVehicleAdapter = new SpinnerVehicleAdapter(getContext(), vehicles);
                    spin_PublishNew_Vehicle.setAdapter(spinnerVehicleAdapter);
                    spin_PublishNew_Vehicle.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                         validateVehicle = true;
                        }
                        @Override
                        public void onNothingSelected(AdapterView<?> parent) {}
                    });
                }
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {}
        });

        checkbox_PublishNew_AcceptTerms.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean value) {
                if (compoundButton.isChecked()){
                    validateUserCond = true;
                }else{
                    Toast.makeText(compoundButton.getContext(), "Please Acept the terms to continue", Toast.LENGTH_SHORT).show();
                    validateUserCond = false;
                }
            }
        });

        if(checkbox_PublishNew_AcceptTerms.isChecked()){
            validateUserCond = true;
        }

        button_PublishNew_Continue.setOnClickListener(this);

        //Obtenemos la fecha y hora actual
        calendar.setTime(date);

        year       = calendar.get(Calendar.YEAR);
        month      = calendar.get(Calendar.MONTH);
        dayOfMonth = calendar.get(Calendar.DAY_OF_MONTH);

        hour = calendar.get(Calendar.HOUR);
        minutes = calendar.get(Calendar.MINUTE);

        return resultView;
    }

    //Utilizado para generar las dos vistas de hora y fecha para viaje frecuente.
    private void generateDateTimeViews(){
        //Removemos las vistas que haya para que no se dupliquen cada vez que se seleccione el radioButton "Round".
        insertPoint.removeAllViews();

        //Inflamos la fecha y hora desde la plantilla de layout creada
        View v2 = layoutInflater.inflate(R.layout.items_publish_frecuent_new_date_time, null);

        //Agregamos hora y fecha a la vista
        TextView textViewDateReturn = (TextView) v2.findViewById(R.id.textView_PublishFrecuent_Date_Return);
        TextView textViewTimeReturn = (TextView) v2.findViewById(R.id.textView_PublishFrecuent_Time_Return);

        textView_PublishNew_Date_Return = textViewDateReturn;
        textView_PublishNew_Time_Return = textViewTimeReturn;

        textViewTimeReturn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                t = new TimePickerDialog(getContext(), new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker timePicker, int hourOfDay, int minute) {
                        textView_PublishNew_Time_Return.setText(String.format("%s:%s", hourOfDay, minute));

                        //Agregamos la hora de regreso del viaje si es redondo.
                        timesTrip.add(textView_PublishNew_Time_Return.getText().toString());

                        timePicker.setHour(15);
                        timePicker.setMinute(12);
                        timePicker.setOnTimeChangedListener(new TimePicker.OnTimeChangedListener() {
                            @Override
                            public void onTimeChanged(TimePicker view, int hourOfDay, int minute) {
                                //view.setHour(15);
                                // view.setMinute(12);
                            }
                        });
                    }
                }, hour, minutes, false);
                t.show();
            }
        });

        textViewDateReturn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Esta es la fecha minima que
                //mostrara el dialogo de fecha
                Date todayReturn = new Date();
                Calendar cReturn = Calendar.getInstance();
                cReturn.setTime(todayReturn);
                long minDateReturn = cReturn.getTime().getTime();

                d = new DatePickerDialog(getContext(), new DatePickerDialog.OnDateSetListener(){
                    @Override
                    public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {
                        textView_PublishNew_Date_Return.setText(SplitTools.formatDate(i, i1, i2));

                        //Agregamos la fecha de regreso del viaje si es redondo
                        datesTrip.add(textView_PublishNew_Date_Return.getText().toString());
                    }
                }, year, month, dayOfMonth);
                d.getDatePicker().setMinDate(minDateReturn);
                d.show();
            }
        });


        //Un nuevo textWatcher para validar y asignar al nuevo viaje, la fecha y hora de retorno.
        TextWatcher tw = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                //Usados para validar el contenido de los views date y time
                                                                        //Ya debe existir hora y fecha de retorno
                validateDateReturn = ValidateField.textContent(textView_PublishNew_Date_Return, "Date");
                validateTimeReturn = ValidateField.textContent(textView_PublishNew_Time_Return, "Time");
            }

            @Override
            public void afterTextChanged(Editable s) {}
        };

        textViewDateReturn.addTextChangedListener(tw);
        textViewTimeReturn.addTextChangedListener(tw);

        // Agregamos la nueva vista al contenedor del linearlayout en nuestro formulario
        insertPoint.addView(v2, 0, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.textView_PublishNew_From:
                Intent intentOrigin = new Intent(getAppContext(), Activity_Map1.class);
                startActivityForResult(intentOrigin, GET_ORIGIN_LOCATION);
                break;

            case R.id.textView_PublishNew_Destination:
                Intent intentDestination = new Intent(getAppContext(), Activity_Map1.class);
                startActivityForResult(intentDestination, GET_DESTINATION_LOCATION);
                break;

            /*case R.id.textView_PublishNew_Add:
                Intent intentLayover = new Intent(getAppContext(), Activity_Map1.class);
                startActivityForResult(intentLayover, GET_LAYOVER_LOCATION);
                break;*/

            case R.id.textView_PublishNew_Date:

                //Esta es la fecha minima que
                //mostrara el dialogo de fecha
                Date today = new Date();
                Calendar c = Calendar.getInstance();
                c.setTime(today);
                long minDate = c.getTime().getTime();

                d = new DatePickerDialog(getContext(), new DatePickerDialog.OnDateSetListener(){
                    @Override
                    public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {
                        textView_PublishNew_Date.setText(SplitTools.formatDate(i, i1, i2));

                        //Agregamos la fecha de ida del viaje
                        datesTrip.add(textView_PublishNew_Date.getText().toString());
                    }
                }, year, month, dayOfMonth);
                d.getDatePicker().setMinDate(minDate);
                d.show();

                break;

            case R.id.textView_PublishNew_Time:

                    t = new TimePickerDialog(getContext(), new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker timePicker, int hourOfDay, int minute) {

                        String hour = Integer.toString(hourOfDay);
                        String minute00 = Integer.toString(minute);

                        if(hour.length() == 1){
                            hour = "0" + hour;
                        }

                        if (minute00.length() == 1){
                            minute00 = "0" + minute00;
                        }

                        textView_PublishNew_Time.setText(String.format("%s:%s", hour, minute00));

                        //Agregamos la hora de ida del viaje
                        timesTrip.add(textView_PublishNew_Time.getText().toString());

                        timePicker.setHour(15);
                        timePicker.setMinute(12);
                        timePicker.setOnTimeChangedListener(new TimePicker.OnTimeChangedListener() {
                            @Override
                            public void onTimeChanged(TimePicker view, int hourOfDay, int minute) {
                                //view.setHour(15);
                               // view.setMinute(12);
                            }
                        });
                    }
                }, hour, minutes, false);
                t.show();
                break;

            case R.id.checkbox_PublishNew_AddToFrequents:


                break;

            case R.id.button_PublishNew_LessPrice:
                Double price = Double.parseDouble(priceTrip);
                if (price > (calculatedPrice*0.75)){
                    if (price > MinPrice){
                        price = Double.parseDouble(priceTrip) - 1;
                        priceTrip = Double.toString(price);
                        textView_PublishNew_Precio.setText("$"+priceTrip+" MXN");
                    }
                }
                break;

            case R.id.button_PublishNew_MorePrice:
                Double price2 = Double.parseDouble(priceTrip);
                if (price2 < (calculatedPrice*1.25)){
                    price2 = Double.parseDouble(priceTrip) + 1;
                    priceTrip = Double.toString(price2);
                    textView_PublishNew_Precio.setText("$"+priceTrip+" MXN");
                    }
                break;

            case R.id.button_Publish_Continue:

                if(radiobtn_PublishNew_Round.isChecked()) {
                    //Este if guarda el viaje de tipo round
                    Log.d(TAG, "ESTATE"+validateOrigin + "" + validateDestin + "" + validateTripType + "" + validateDate + "" + validateTime
                            + "" + validateDateReturn + "" + validateTimeReturn
                            + "" + validateVehicle + "" + validateAvaliableSeats + "" + validateBaggage + "" + validateBaggagePerPerson + "" + validateUserCond);
                    if (validateOrigin && validateDestin && validateTripType && validateDate && validateTime
                            && validateDateReturn && validateTimeReturn && validateDetails
                            && validateVehicle && validateAvaliableSeats && validateBaggage && validateBaggagePerPerson && validateUserCond) {
                      //  Toast.makeText(view.getContext(), getString(R.string.msg_Publish_Added), Toast.LENGTH_LONG).show();

                        //Si se selecciono el viaje como frecuente lo agregamos en el mismo viaje como una bandera
                        //que nos indicara si el viaje es frecuente o no
                        if(checkbox_PublishNew_AddToFrequents.isChecked()){
                            tmp.setFrequent(true);
                        }else{
                            tmp.setFrequent(false);
                        }
                        finishSaveTrip();
                        ActivityPublishTrips.setToPublish(tmp);
                        startActivity(new Intent(getContext(), ActivityPublishSumaryTrip.class));

                    } else {
                        //Muestramos los errores del formulario al usuario
                        showFieldErrors(view);
                    }

                }else{
                //Este else guarda el viaje de tipo "single"
                Log.d(TAG, "ESTATE"+validateOrigin +""+ validateDestin +""+  validateTripType +""+ validateDate +""+ validateTime
                        +""+ validateVehicle +""+ validateAvaliableSeats +""+ validateBaggage +""+ validateBaggagePerPerson +""+ validateUserCond);
                if(validateOrigin && validateDestin &&  validateTripType && validateDate && validateTime
                    && validateVehicle && validateAvaliableSeats && validateBaggage && validateBaggagePerPerson && validateUserCond){
                  //  Toast.makeText(view.getContext(), getString(R.string.msg_Publish_Added), Toast.LENGTH_LONG).show();

                    //Si se selecciono el viaje como frecuente lo
                    //agregamos en el mismo viaje como una bandera
                    //que nos indicara si el viaje es frecuente o no
                    if(checkbox_PublishNew_AddToFrequents.isChecked()){
                        tmp.setFrequent(true);
                    }else{
                        tmp.setFrequent(false);
                    }
                    finishSaveTrip();
                    ActivityPublishTrips.setToPublish(tmp);
                    startActivity(new Intent(getContext(), ActivityPublishSumaryTrip.class));

                }else{
                    //Muestramos los errores del formulario al usuario
                    showFieldErrors(view);
                }
            }
        }
    }

    public void showFieldErrors(View view){
        //Muestra los errores del formulario al usuario
        if(!validateOrigin){Toast.makeText(view.getContext(), "validate origin", Toast.LENGTH_LONG).show();}
        if(!validateDestin){Toast.makeText(view.getContext(), "validate destination", Toast.LENGTH_LONG).show();}
        if(!validateTripType){Toast.makeText(view.getContext(), "validate trip type", Toast.LENGTH_LONG).show();}
        if(!validateDate){Toast.makeText(view.getContext(), "validate date", Toast.LENGTH_LONG).show();}
        if(!validateTime){Toast.makeText(view.getContext(), "validate time", Toast.LENGTH_LONG).show();}
        if(!validateAvaliableSeats){Toast.makeText(view.getContext(), "validate seats", Toast.LENGTH_LONG).show();}
        if(!validateBaggage){Toast.makeText(view.getContext(), "validate baggage", Toast.LENGTH_LONG).show();}
        if(!validateBaggagePerPerson){Toast.makeText(view.getContext(), "validate bag per person", Toast.LENGTH_LONG).show();}
        if(!validateUserCond){Toast.makeText(view.getContext(), "validate user cond", Toast.LENGTH_LONG).show();}
        Toast.makeText(view.getContext(), getString(R.string.msg_Publish_Wrong), Toast.LENGTH_LONG).show();
    }


    //Este metodo termina de guardar los
    //demas datos cargados en los campos del formulario.
    public void finishSaveTrip(){

        //Creamos un nuevo usuario y agregamos su id.
        User driver = new User();

        //Traemos los datos del usuario con el callback
        SplitApp.getFirebaseUser();

        driver.setId(SplitApp.getFrbUserId());
        driver.setFullName(ActivitySplit.tmpUser.getFullName());
        driver.setCity(ActivitySplit.tmpUser.getCity());
        driver.setFcmRegisterToken(ActivitySplit.tmpUser.getFcmRegisterToken());
        driver.setCellphone(ActivitySplit.tmpUser.getCellphone());

        tmp.setDriver(driver);
        tmp.setDate(datesTrip);
        tmp.setTime(timesTrip);
        //vehiculo en listener de spinner
        tmp.setVehicle((Vehicle) spin_PublishNew_Vehicle.getSelectedItem());
        //asientos en listener de spinner
        tmp.setSeats(Integer.parseInt(spin_PublishNew_Seats.getSelectedItem().toString()));
        //opciones en listener
        tmp.setOptions(editText_PublishNew_Details.getText().toString());
        //precio del viaje en Double
        tmp.setPrice(Double.parseDouble(priceTrip));
        //Baggager per person
        tmp.setBaggagePerPerson(Integer.parseInt(spin_PublishNew_BagNumber.getSelectedItem().toString()));

    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data){
        if (resultCode == Activity.RESULT_OK){
            PointLocation pointLocation = PointLocation.fromString(data.getStringExtra("PointLocation"));
            if (requestCode == GET_ORIGIN_LOCATION){
                tmp.setOrigin(pointLocation);
                textView_PublishNew_From.setText(pointLocation.asString());
            }
            if (requestCode == GET_DESTINATION_LOCATION){
                tmp.setDestination(pointLocation);
                textView_PublishNew_Destination.setText(pointLocation.asString());

                //Obtiene el precio de un viaje, al momento de seleccionar
                //un destino, esto quiere decir que el origen ya se ha seleccionado
                try {
                    //Parametros para consultar el precio del viaje
                    final String jsonParams = "{\"origin\":{\"latitude\":"+tmp.getOrigin().getLatitude()
                            +", \"longitude\":"+tmp.getOrigin().getLongitude()+"}, "
                            +"\"destination\":{\"latitude\":"+tmp.getDestination().getLatitude()
                            +", \"longitude\":"+tmp.getDestination().getLongitude()+"}}";

                    SplitRequest.getPrice(new CallbackGetPrice() {
                        //Si recibimos el precio del origen y destino lo agregamos en el texto del precio.
                        @Override
                        public void onSuccess(String price) {
                            calculatedPrice = Double.parseDouble(price);
                            Double priceDouble = Double.parseDouble(price);
                            int pricevar = priceDouble.intValue(); // Para truncar a 2 decimales
                            price = Double.toString(pricevar);
                            textView_PublishNew_Precio.setText("$"+price+" MXN");
                            //Guardamos el precio del viaje para posteriormente asignarlo al viaje.
                            priceTrip = price;
                        }

                        @Override
                        public void onFail(String msg) { Log.d("fail: ", msg); }
                    }, jsonParams, TAG);

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            /*if (requestCode == GET_LAYOVER_LOCATION){
                PointLocation layover = pointLocation;
                //Agregamos el layouver a la lista y al viaje nuevo
                //Layouvers para el recycler
                layouvers.add(layover);
                recyclerLayouversAdapter.notifyDataSetChanged();

                //Layouvers agregados al viaje
                tmp.setLayovers(layouvers);
            }*/
        }
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        //Validamos los campos de acuerdo al contenido, si el contenido ha cambiado quiere decir
        //que se ha ingresado un origen, destino, fecha y hora y la validación es correcta.
        validateOrigin = ValidateField.textContent(textView_PublishNew_From, getString(R.string.hint_Publish_From));
        validateDestin = ValidateField.textContent(textView_PublishNew_Destination, getString(R.string.hint_Publish_Destination));
        validateDate = ValidateField.textContent(textView_PublishNew_Date, getString(R.string.lbl_Publish_Date));
        validateTime = ValidateField.textContent(textView_PublishNew_Time, getString(R.string.lbl_Publish_Time));
        validateDetails = ValidateField.textContent(editText_PublishNew_Details,"");
    }
    @Override
    public void afterTextChanged(Editable s) {}
}
