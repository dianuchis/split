package com.cmch.split;

//hello
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class Activity_EmergencyPhones extends AppCompatActivity {
    EditText EmergencyPhoneSMS;
    EditText EmergencyPhoneCall;
    Button SaveEmergencyPhones;
    SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity__emergency_phones);

        //Declare view items
        EmergencyPhoneCall = (EditText) findViewById(R.id.EmergencyPhoneCall);
        EmergencyPhoneSMS = (EditText) findViewById(R.id.EmergencyPhoneSMS);
        SaveEmergencyPhones = (Button) findViewById(R.id.SaveEmergencyPhones);

        //Set EditTexts with SharedPreferences information
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
        //Update EmergencyPhoneCall
        String EmergencyPhoneCallString = sharedPreferences.getString("EmergencyPhoneCall", null);
        EmergencyPhoneCall.setText(EmergencyPhoneCallString,TextView.BufferType.EDITABLE);
        //Update EmergencyPhoneSMS
        String EmergencyPhoneSMSString = sharedPreferences.getString("EmergencyPhoneSMS", null);
        EmergencyPhoneSMS.setText(EmergencyPhoneSMSString, TextView.BufferType.EDITABLE);

        //Edit SharedPreferences information with onClickListener
        SaveEmergencyPhones.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Get text from EditTexts
                String EmPhone1 = EmergencyPhoneCall.getText().toString();
                String EmPhone2 = EmergencyPhoneSMS.getText().toString();
                //Print in SharedPreferences
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putString("EmergencyPhoneCall",EmPhone1);
                editor.putString("EmergencyPhoneSMS",EmPhone2);
                editor.apply();
                Toast.makeText(getApplicationContext(), "Phones updated",Toast.LENGTH_SHORT).show();
            }
        });


    }

}
