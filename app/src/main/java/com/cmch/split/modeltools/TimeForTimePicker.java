package com.cmch.split.modeltools;

/**
 * Created by CC on 29-Mar-18.
 */

public class TimeForTimePicker {
    private int hour;
    private int minute;

    public int getHour() {
        return hour;
    }

    public void setHour(int hour) {
        this.hour = hour;
    }

    public int getMinute() {
        return minute;
    }

    public void setMinute(int minute) {
        this.minute = minute;
    }
}
