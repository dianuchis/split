package com.cmch.split.modelviews;

import android.app.Activity;

/**
 * Created by CC on 14-Mar-18.
 *
 * Descripcion: Esta clase guarda los elementos de cada item
 *              para el tab TabProfileOptions, que se mostrarán
 *              en el reciclerView del tab.
 */

public class RecyclerProfileOptions {

    private String option;
    private int idImage;
    private Class<? extends Activity> activity;


    public RecyclerProfileOptions(){ }


    public void setActivity(Class<? extends Activity> activity){ this.activity = activity; }

    public Class<? extends Activity> getActivity(){ return this.activity; }

    public String getOption(){ return this.option; }

    public void setOption(String option){ this.option = option; }

    public int getIdImage(){ return this.idImage; }

    public void setIdImage(int idImage){this.idImage = idImage; }

}
