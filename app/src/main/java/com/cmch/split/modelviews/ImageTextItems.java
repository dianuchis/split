package com.cmch.split.modelviews;

/**
 * Created by CC on 23-Apr-18.
 */

public class ImageTextItems {

    private int imageId;
    private String text;

    public int getImageId() {
        return imageId;
    }

    public void setImageId(int imageId) {
        this.imageId = imageId;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
