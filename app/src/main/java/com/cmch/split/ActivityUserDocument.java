package com.cmch.split;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import com.cmch.split.baseactivity.ActivitySplit;
import com.cmch.split.tools.FrbUpload;
import com.cmch.split.tools.SimpleAlertDialog;
import com.cmch.split.adapters.SpinnerStringAdapter;
import com.cmch.split.tools.WaitingDialog;
import com.google.firebase.storage.UploadTask;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

/**
 * Created by alex on 27/11/17.
 */

    public class ActivityUserDocument extends AppCompatActivity implements View.OnClickListener{
    
    private static final int FILE_SELECT_PHOTO_FILECHOOSER = 0;
    private static final int FILE_SELECT_PHOTO_CAMERA = 1;
    private static final int FILE_SELECT_ID_FILECHOOSER = 2;
    private static final int FILE_SELECT_ID_CAMERA = 3;
    private static Bitmap bitesPhotoProfile = null;
    private static Bitmap bitesIdentificationProfile = null;

    private ImageView imageView_Register_Photo;
    private ImageView imageView_Register_Identification;
    private Button button_UserDocuments_Continue;
    private Spinner spin_Register_TypeID;
    private SpinnerStringAdapter adapterTypeId;

    public static boolean isActive;

    private String TAG = "UserDocument";

    private WaitingDialog waitingDialog;

    private FrbUpload uploadPhoto;
    private FrbUpload uploadIdentification;
    private Context context = this;

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_document);

        SplitApp.getFirebaseUser();

        waitingDialog = new WaitingDialog(this);

        uploadIdentification = new FrbUpload() {
            @Override
            public void OnStart() {}

            @Override
            public void OnSuccesOrError(Exception exception, UploadTask.TaskSnapshot taskSnapshot) {
                if (exception != null){
                    Toast.makeText(getApplicationContext(), exception.getMessage(), Toast.LENGTH_LONG).show();
                }else{

                    waitingDialog.Dismiss();
                    new SimpleAlertDialog(context,
                            getResources().getString(R.string.lbl_Register_TitleRegistrationSuccessful),
                            getResources().getString(R.string.lbl_Register_BodyRegistrationSuccessful),
                            ActivityAddVehicle.class, ActivityRegisterSuccessful.class).show();

                }
            }
        };

        uploadPhoto = new FrbUpload() {
            @Override
            public void OnStart() {
                waitingDialog.Show();
            }

            @Override
            public void OnSuccesOrError(Exception exception, UploadTask.TaskSnapshot taskSnapshot) {
                if(exception != null){
                   // SplitApp.getCallbackPhotoProfile().onFail(exception.toString());
                    waitingDialog.Dismiss();
                    Toast.makeText(context, exception.getMessage(), Toast.LENGTH_LONG).show();
                }
                else{
                    uploadIdentification.start();
                }
            }
        };


        imageView_Register_Photo = findViewById(R.id.imageButton_Register_Photo);
        imageView_Register_Identification = findViewById(R.id.imageButton_Register_Identification);
        button_UserDocuments_Continue = (Button) findViewById(R.id.button_UserDocuments_Continue);
        imageView_Register_Photo.setOnClickListener(this);
        imageView_Register_Identification.setOnClickListener(this);
        button_UserDocuments_Continue.setOnClickListener(this);

        spin_Register_TypeID = findViewById(R.id.spin_Register_TypeID);
        adapterTypeId = new SpinnerStringAdapter(this, SplitApp.getTypeId());
        spin_Register_TypeID.setAdapter(adapterTypeId);

    }

    private void showFileChooser(int selectFileCode) {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("*/*");
        intent.addCategory(Intent.CATEGORY_OPENABLE);

        try {
            startActivityForResult(
                    Intent.createChooser(intent, "Select a File to Upload"),
                    selectFileCode);
        } catch (android.content.ActivityNotFoundException ex) {
            // Potentially direct the user to the Market with a Dialog
            Toast.makeText(this, "Please install a File Manager.",
                    Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK){
            //Getting the path
            Uri filePath = data.getData();
            Bitmap bitmap = null;

            String userId = SplitApp.getFrbUserId();
            String photoPath = String.format("documents/%s/photo.jpg", userId);
            String identificationPath = String.format("documents/%s/identification.jpg", userId);

            switch (requestCode) {
                case FILE_SELECT_PHOTO_CAMERA:
                    bitmap = (Bitmap) data.getExtras().get("data");
                    compressAndUpload(bitmap,  data, photoPath, R.id.imageButton_Register_Photo, uploadPhoto);
                    bitesPhotoProfile = bitmap;
                    break;
                case FILE_SELECT_PHOTO_FILECHOOSER:
                    //Getting the bitmap
                    try {
                        bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), filePath);
                        compressAndUpload(bitmap,  data, photoPath, R.id.imageButton_Register_Photo, uploadPhoto);
                        bitesPhotoProfile = bitmap;
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    break;

                case FILE_SELECT_ID_CAMERA:
                    bitmap = (Bitmap) data.getExtras().get("data");
                    compressAndUpload(bitmap,  data, identificationPath, R.id.imageButton_Register_Identification, uploadIdentification);
                    bitesIdentificationProfile = bitmap;
                    break;

                case FILE_SELECT_ID_FILECHOOSER:
                    //Getting the bitmap
                    try {
                        bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), filePath);
                        compressAndUpload(bitmap,  data, identificationPath, R.id.imageButton_Register_Identification, uploadIdentification);
                        bitesIdentificationProfile = bitmap;
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    break;
            }
        }

        super.onActivityResult(requestCode, resultCode, data);
    }

    public void compressAndUpload(Bitmap bitmap, Intent data, String path, int id, FrbUpload frbUpload){
        ((ImageView)findViewById(id)).setImageBitmap(bitmap);
        ((ImageView)findViewById(id)).setTag("Image_added");
        ByteArrayOutputStream baosI = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baosI);
        frbUpload.setPath(path);
        frbUpload.setData(baosI.toByteArray());
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case    R.id.imageButton_Register_Photo:

                AlertDialog dialogPhoto;
                AlertDialog.Builder builderPhoto = new AlertDialog.Builder(view.getContext());
                builderPhoto.setMessage(R.string.lbl_Register_BodySelectPhoto);
                builderPhoto.setTitle(R.string.lbl_Register_TitleSelectPhoto);
                builderPhoto.setPositiveButton("File", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        showFileChooser(FILE_SELECT_PHOTO_FILECHOOSER);
                    }
                });
                builderPhoto.setNegativeButton("Camera", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Intent iCameraPhoto = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                        startActivityForResult(iCameraPhoto, FILE_SELECT_PHOTO_CAMERA);
                    }
                });
                dialogPhoto = builderPhoto.create();
                dialogPhoto.show();
                break;

            case    R.id.imageButton_Register_Identification:
                AlertDialog dialogIdentification;
                AlertDialog.Builder builder = new AlertDialog.Builder(view.getContext());
                builder.setMessage(R.string.lbl_Register_BodySelectPhoto);
                builder.setTitle(R.string.lbl_Register_TitleSelectPhoto);
                builder.setPositiveButton("File", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        showFileChooser(FILE_SELECT_ID_FILECHOOSER);
                    }
                });
                builder.setNegativeButton("Camera", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Intent iCameraPhoto = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                        startActivityForResult(iCameraPhoto, FILE_SELECT_ID_CAMERA);
                    }
                });
                dialogIdentification = builder.create();
                dialogIdentification.show();
                break;

            case R.id.button_UserDocuments_Continue:
                if (imageView_Register_Photo.getTag() != null && imageView_Register_Identification.getTag() != null){
                    uploadPhoto.start();
                }
                else{
                    Toast.makeText(this, getString(R.string.msg_Register_UploadID), Toast.LENGTH_LONG).show();
                }
                break;
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        isActive = true;
    }

    @Override
    protected void onStop() {
        super.onStop();
        isActive = false;
    }

    @Override
    public void onBackPressed() {Toast.makeText(this, getString(R.string.lbl_Register_MessagePhoto), Toast.LENGTH_LONG).show();}

}
