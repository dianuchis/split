package com.cmch.split;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.text.Editable;
import android.util.Log;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.cmch.split.adapters.SpinnerVehicleAdapter;
import com.cmch.split.dbref.FrecuentTrip;
import com.cmch.split.dbref.JsScheduleFrecTripParams;
import com.cmch.split.dbref.PointLocation;
import com.cmch.split.dbref.Trip;
import com.cmch.split.callbacks.GetAvailableTrips;
import com.cmch.split.tools.BubbleSeekBarValue;
import com.cmch.split.tools.FrbValueEventListener;
import com.cmch.split.tools.SPDateDialog;
import com.cmch.split.tools.SPJSONObject;
import com.cmch.split.tools.SPTimeDialog;
import com.cmch.split.tools.SplitBubbleSeekBar;
import com.cmch.split.tools.SplitJSONRequest;
import com.cmch.split.tools.SplitRequest;
import com.cmch.split.tools.SplitTools;
import com.cmch.split.tools.ValidateField;
import com.cmch.split.tools.WaitingDialog;
import com.cmch.split.validation.FormValidator;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.xw.repo.BubbleSeekBar;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import static com.cmch.split.ActivityPublishTrips.GET_DESTINATION_LOCATION;
import static com.cmch.split.ActivityPublishTrips.GET_ORIGIN_LOCATION;
import static com.cmch.split.SplitApp.getAppContext;

/**
 */

public class TabScheduleNewTrip extends Fragment implements View.OnClickListener {

    private static final String TAG = "TabScheduleNewTrip";

    private final FormValidator formValidator = new FormValidator(getAppContext(), R.drawable.edit_text_green_border, R.drawable.edit_text_red_border);

    RadioButton radiobtn_ScheduleNew_Single;
    private TextView txtVFrom;
    private TextView txtVDestination;
    private TextView textView_ScheduleNew_Date;
    private TextView textView_ScheduleNew_Time;
    private CheckBox tgBtn_ScheduleNew_isFrecuentTrip;
    private Button btnFind;
    private RadioGroup radioGroup;

    //Usado para la peticion http de los viajes disponibles
    private String URL_QUERY_TRIPS = "https://us-central1-fir-plit.cloudfunctions.net/app/gettrips2";
    private FragSummaryNewTrips fragSummaryNewTrips;

    //Usado para el parametro de distancia
    private SplitBubbleSeekBar seekBar_Distance;

    //Usado para el parametro de rango de horas
    private SplitBubbleSeekBar seekBar_TimeRange;


    List<BubbleSeekBarValue> distanceValues;
    List<BubbleSeekBarValue> timeValues;

    PointLocation pointLocationOrigin;
    PointLocation pointLocationDestination;

    //Se utilizan para obtener hora y fecha actual
    private Date date = new Date();
    private final Calendar calendar = Calendar.getInstance();
    private int year;
    private int month;
    private int dayOfMonth;
    private int hour;
    private int minutes;
    private TimePickerDialog t;
    private DatePickerDialog d;


    @RequiresApi(api = Build.VERSION_CODES.O)
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View resultView = inflater.inflate(R.layout.frag_tab_schedule_new_trip, container, false);


        distanceValues = new ArrayList<>();
        distanceValues.add(new BubbleSeekBarValue("250 m", 0, 0.25f, "0.25"));
        distanceValues.add(new BubbleSeekBarValue("0.5 km", 0, 0.5f, "0.5"));
        distanceValues.add(new BubbleSeekBarValue("1 km", 1, 1.0f, "1"));
        distanceValues.add(new BubbleSeekBarValue("2 km", 2, 2.0f, "2"));

        timeValues = new ArrayList<>();
        timeValues.add(new BubbleSeekBarValue("30 min", 0, 0.5f, "00:30"));
        timeValues.add(new BubbleSeekBarValue("1 hr", 1, 1.0f, "01:00"));
        timeValues.add(new BubbleSeekBarValue("2 hr", 2, 2.0f, "02:00"));
        timeValues.add(new BubbleSeekBarValue("1 day", 24, 24.0f, "24:00"));



        seekBar_Distance = new SplitBubbleSeekBar( (BubbleSeekBar) resultView.findViewById(R.id.seekBar_Schedule_NewTrip_Distance));
        seekBar_TimeRange = new SplitBubbleSeekBar( (BubbleSeekBar) resultView.findViewById(R.id.seekBar_Schedule_NewTrip_timeRange));
        radiobtn_ScheduleNew_Single = resultView.findViewById(R.id.radiobtn_ScheduleNew_Single);
        txtVFrom = resultView.findViewById(R.id.textView_ScheduleNew_From);
        txtVDestination = resultView.findViewById(R.id.textView_ScheduleNew_Destination);
        btnFind = resultView.findViewById(R.id.button_Schedule_frag_tab_Search);
        textView_ScheduleNew_Date = resultView.findViewById(R.id.textView_ScheduleNew_Date);
        textView_ScheduleNew_Time = resultView.findViewById(R.id.textView_ScheduleNew_Time);

        radioGroup = resultView.findViewById(R.id.radioGroup_Schedule_NewTrip_TripType);

        formValidator.addViewValidator(radioGroup, true, "(100) Debe indicar el tipo de viaje (Sencillo/Redondo)");
        formValidator.addViewValidator(txtVFrom, true, "(200) Es necesario indicar el origen");
        formValidator.addViewValidator(txtVDestination, true, "(300) Es necesario inidicar el destino");
        formValidator.addViewValidator(textView_ScheduleNew_Date, true, "(400) Es necesario indicar la fecha");
        formValidator.addViewValidator(textView_ScheduleNew_Time, true, "(500) Es necesario indicar la hora");

        tgBtn_ScheduleNew_isFrecuentTrip = resultView.findViewById(R.id.tgBtn_ScheduleNew_isFrecuentTrip);

        //Valores de distancia
        seekBar_Distance.setValues(distanceValues);
        //valores de tiempo
        seekBar_TimeRange.setValues(timeValues);

        txtVFrom.setOnClickListener(this);
        txtVDestination.setOnClickListener(this);
        textView_ScheduleNew_Date.setOnClickListener(this);
        textView_ScheduleNew_Time.setOnClickListener(this);

        btnFind.setEnabled(true);
        btnFind.setOnClickListener(this);

        //Obtenemos la fecha y hora actual
        calendar.setTime(date);
        year       = calendar.get(Calendar.YEAR);
        month      = calendar.get(Calendar.MONTH);
        dayOfMonth = calendar.get(Calendar.DAY_OF_MONTH);
        hour = calendar.get(Calendar.HOUR);
        minutes = calendar.get(Calendar.MINUTE);

        return resultView;
    }

    private void saveFrecuentTrip(){

        final DatabaseReference newfrecTripRef = SplitApp.getFrbDatabase().child("scheduledFrequentTrips");

        String passengerId = SplitApp.getFrbUserId();
        String frecuentTripId = newfrecTripRef.push().getKey();
        FrecuentTrip frecuentTrip = new FrecuentTrip();

        //Asignamos al viaje para agendar los parametros obtenidos en el formulario.
        frecuentTrip.setId(frecuentTripId);
        frecuentTrip.setIdPassenger(passengerId);
        frecuentTrip.setDistance(seekBar_Distance.getFloatValue());
        frecuentTrip.setTimeRange(seekBar_TimeRange.getStringValue());
        frecuentTrip.setType( radioGroup.getCheckedRadioButtonId() == R.id.radiobtn_ScheduleNew_Single ? "single" : "round");
        frecuentTrip.setOrigin(pointLocationOrigin);
        frecuentTrip.setDestination(pointLocationDestination);

        //Consultamos si ya existe un viaje frecuente
         if (!newfrecTripRef.child(frecuentTrip.getId()).setValue(frecuentTrip).isSuccessful()){
             Toast.makeText(getAppContext(), "(600) An error was ocurred at saving frecuent trip", Toast.LENGTH_LONG).show();
         }
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    public void onClick(View view) {
        switch (view.getId()){

            case R.id.textView_ScheduleNew_Date:
                //Minimum date to display at calendar
                Date today = new Date();
                Calendar c = Calendar.getInstance();
                c.setTime(today);
                long minDate = c.getTime().getTime();

                d = new DatePickerDialog(getContext(), new DatePickerDialog.OnDateSetListener(){
                    @Override
                    public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {
                        textView_ScheduleNew_Date.setText(SplitTools.formatDate(i, i1, i2));
                    }
                },year, month, dayOfMonth);
                d.getDatePicker().setMinDate(minDate);
                d.show();

                /* This is the simple calendar display to select a trip date
                new SPDateDialog(getContext()) {
                    @Override
                    public void toDo(int day, int month, int year, String formatedDate) {
                        textView_ScheduleNew_Date.setText(formatedDate);
                    }
                }.execute(); */

                break;

            case R.id.textView_ScheduleNew_Time:
                new SPTimeDialog(getContext()) {
                    @Override
                    public void toDo(int hour, int minutes, String formatedTime) {
                        textView_ScheduleNew_Time.setText(formatedTime);
                    }
                }.execute();

                break;
            case R.id.textView_ScheduleNew_From:
                Intent intentOrigin = new Intent(getAppContext(), Activity_Map1.class);
                startActivityForResult(intentOrigin, GET_ORIGIN_LOCATION);
                break;
            case R.id.textView_ScheduleNew_Destination:
                Intent intentDestination = new Intent(getAppContext(), Activity_Map1.class);
                startActivityForResult(intentDestination, GET_DESTINATION_LOCATION);
                break;

            case R.id.button_Schedule_frag_tab_Search:
                Boolean formValidation = formValidator.getFormValidation();
                if (formValidation){
                    //Obtenemos todos los parametros
                    final SPJSONObject spjsonObject = new SPJSONObject();

                    spjsonObject.put("distance", seekBar_Distance.getFloatValue());
                    spjsonObject.put("timeRange", seekBar_TimeRange.getStringValue());
                    spjsonObject.put("type", radioGroup.getCheckedRadioButtonId() == R.id.radiobtn_ScheduleNew_Single ? "single":"round");
                    spjsonObject.put("date", textView_ScheduleNew_Date.getText().toString());
                    spjsonObject.put("time", textView_ScheduleNew_Time.getText().toString());
                    spjsonObject.put("origin", pointLocationOrigin.toJSon());
                    spjsonObject.put("destination", pointLocationDestination.toJSon());

                    //Vefificamos si el pasajero guardo el viaje como frecuente
                    //Si es asi lo guardamos
                    if(tgBtn_ScheduleNew_isFrecuentTrip.isChecked()){
                        saveFrecuentTrip();
                    }

                    //Limpiar formulario
                    //...

                    final WaitingDialog waitingDialog = new WaitingDialog(getActivity());
                    waitingDialog.Show();

                    //Adding the fragment
                    fragSummaryNewTrips = new FragSummaryNewTrips();

                    SplitJSONRequest splitJSONRequest = new SplitJSONRequest(URL_QUERY_TRIPS, spjsonObject.get()) {
                        @Override
                        public void onResponseOrError(JSONObject response, VolleyError error) {
                            waitingDialog.Dismiss();
                            if (error == null){
                                SPJSONObject spResponse = new SPJSONObject(response);
                                if (spResponse.getBoolean("success")){
                                    JSONArray jsonArray = spResponse.getJSONArray("data");
                                    if (jsonArray.length() > 0){
                                        Bundle args = new Bundle();
                                        args.putString("trips", spResponse.getJSONArray("data").toString());
                                        fragSummaryNewTrips.setArguments(args);
                                        FragmentTransaction trans = ActivityScheduleTrip.getFm().beginTransaction();
                                        trans.replace(R.id.root_frame, fragSummaryNewTrips);
                                        trans.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
                                        trans.addToBackStack(null);
                                        trans.commit();
                                    }else{
                                        Toast.makeText(getContext(), "(700) No trips availables in this zone", Toast.LENGTH_LONG).show();
                                    }
                                }
                            }
                        }
                    };

                Log.d(TAG, spjsonObject.toString());
                SplitApp.addSplitRequest(splitJSONRequest);

                }
                break;
        }
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        if(resultCode == Activity.RESULT_OK){
            PointLocation pointLocation = PointLocation.fromString(data.getStringExtra("PointLocation"));
            if (requestCode == GET_ORIGIN_LOCATION){
                txtVFrom.setText(pointLocation.asString());
                pointLocationOrigin = pointLocation;
            }
            if (requestCode == GET_DESTINATION_LOCATION){
                txtVDestination.setText(pointLocation.asString());
                pointLocationDestination = pointLocation;
            }
        }
    }


    @Override
    public void onResume() {
        super.onResume();
        seekBar_Distance.setProgres(50.0f);
        seekBar_TimeRange.setProgres(50.0f);
        radiobtn_ScheduleNew_Single.setChecked(true);
    }

}





