package com.cmch.split;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.cmch.split.adapters.RecyclerViewRouteTripAdapter;
import com.cmch.split.dbref.PointLocation;
import com.cmch.split.dbref.Trip;
import com.cmch.split.adapters.RecyclerViewDriverPassengersAdapter;

import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 * Use the {@link FragHomeNotificationsTripVisualization#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FragHomeNotificationsTripVisualization extends Fragment {

    private RecyclerView.Adapter recyclerViewRouteTripAdapter;
    private RecyclerView recyclerViewRouteTrip;

    private RecyclerView.Adapter recyclerViewDriverPassengersAdapter;
    private RecyclerView.LayoutManager layoutManagerRoute;
    private RecyclerView.LayoutManager layoutManagerPassengers;
    private RecyclerView recyclerViewDriverPassengers;


    //Tag of trip arg: Trip scheduled
    private static final String ARG_TRIP_SCHEDULED = "TripScheduled";

    // TODO: Rename and change types of parameters
    private Trip tripScheduled;

    private ArrayList<PointLocation> pointLocations;


    public FragHomeNotificationsTripVisualization() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param tripSheduled Viaje agendado por el usuario, contiene la lista de pasajeros.
     * @return A new instance of fragment FragHomeNotificationsListPassengers.
     */
    // TODO: Rename and change types and number of parameters
    public static FragHomeNotificationsTripVisualization newInstance(Trip tripSheduled) {
        FragHomeNotificationsTripVisualization fragment = new FragHomeNotificationsTripVisualization();
        Bundle args = new Bundle();
        args.putParcelable(ARG_TRIP_SCHEDULED, tripSheduled);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {

        pointLocations = new ArrayList<>();

        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            tripScheduled = getArguments().getParcelable(ARG_TRIP_SCHEDULED);

            pointLocations.add(tripScheduled.getOrigin());
            pointLocations.add(tripScheduled.getDestination());

            if(tripScheduled.getLayovers() != null){
                for (PointLocation p: tripScheduled.getLayovers()){
                    pointLocations.add(p);
                }
            }
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        final View view = inflater.inflate(R.layout.frag_home_notifications_trip_visualization, container, false);

        TextView textViewnameDriver = view.findViewById(R.id.textView_trip_visualization_NameDriver);
        TextView textViewnamePlates = view.findViewById(R.id.textView_trip_visualization_Plates);
        TextView textViewnameModel = view.findViewById(R.id.textView_trip_visualization_Model);
        TextView textViewnameBrand = view.findViewById(R.id.textView_trip_visualization_Brand);
        TextView textViewnameColor = view.findViewById(R.id.textView_trip_visualization_Color);

        //Agregamos los datos del vehiculo del conductor.
        textViewnamePlates.setText(tripScheduled.getVehicle().getPlates());
        textViewnameModel.setText(tripScheduled.getVehicle().getModel());
        textViewnameBrand.setText(tripScheduled.getVehicle().getBrand());
        textViewnameColor.setText(tripScheduled.getVehicle().getColor());


        layoutManagerRoute = new LinearLayoutManager(getContext());
        layoutManagerPassengers = new LinearLayoutManager(getContext());

        recyclerViewRouteTrip = view.findViewById(R.id.recyclerView_HomeNotifications_TripVisualization_RouteTrip);
        recyclerViewDriverPassengers = view.findViewById(R.id.recyclerView_HomeNotifications_TripVisualization_ListPassengers);

        recyclerViewRouteTrip.setLayoutManager(layoutManagerRoute);
        recyclerViewDriverPassengers.setLayoutManager(layoutManagerPassengers);

        //Muestra la ruta del viaje que agendo el pasajero
        recyclerViewRouteTripAdapter = new RecyclerViewRouteTripAdapter(pointLocations);
        recyclerViewRouteTrip.setAdapter(recyclerViewRouteTripAdapter);


        //Muestra los pasajeros que iran en el viaje agendado.
        recyclerViewDriverPassengersAdapter = new RecyclerViewDriverPassengersAdapter(tripScheduled.getPassengers());
        recyclerViewDriverPassengers.setAdapter(recyclerViewDriverPassengersAdapter);


        return view;
    }

}
