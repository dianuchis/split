package com.cmch.split;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.cmch.split.dbref.Vehicle;
import com.cmch.split.adapters.CardViewVehicleAdapter;
import com.google.firebase.database.DatabaseReference;

import java.util.List;

/**
 * Created by alex on 27/11/17.
 */

public class ActivityAddVehicleSumary extends AppCompatActivity implements View.OnClickListener {

    private RecyclerView recyclerView;
    private static RecyclerView.Adapter cardViewVehicleAdapter;
    private RecyclerView.LayoutManager layoutManager;

    private Button button_AddVehicle_Sumary_Save;
    private TextView txView_AddVehicle_Sumary_AddNew;

    private List<Vehicle> newUserVehicles;
    private String callingActivity;

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_vehicle_sumary);

        button_AddVehicle_Sumary_Save = findViewById(R.id.button_AddVehicle_Sumary_Save);
        button_AddVehicle_Sumary_Save.setOnClickListener(this);
        txView_AddVehicle_Sumary_AddNew = findViewById(R.id.txView_AddVehicle_Sumary_AddNew);
        txView_AddVehicle_Sumary_AddNew.setOnClickListener(this);

        newUserVehicles = ActivityAddVehicle.getNewUserVehicles();

        recyclerView = findViewById(R.id.recyclerView);

        // use a linear layout manager
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);

        // specify an adapter (see also next example)
        cardViewVehicleAdapter = new CardViewVehicleAdapter(newUserVehicles);
        recyclerView.setAdapter(cardViewVehicleAdapter);

    }

    private void saveVehicle(){

        DatabaseReference newVehicleRef = SplitApp.getFrbDatabase().child("vehicles");

        for (Vehicle vehicle: newUserVehicles)
        {
            //Si el vehiculo no existe se agrega
            if(newVehicleRef.child(vehicle.getId()) == null)
                newVehicleRef.setValue(vehicle.getId());
                newVehicleRef.child(vehicle.getId()).setValue(vehicle);
        }

    }

    public static RecyclerView.Adapter getCardViewVehicleAdapter(){
        return cardViewVehicleAdapter;
    }

    @Override
    public void onClick(final View view) {
        switch (view.getId()){
            case R.id.button_AddVehicle_Sumary_Save:
                AlertDialog dialogIdentification;
                AlertDialog.Builder builder = new AlertDialog.Builder(view.getContext());
                builder.setTitle(R.string.title_SummaryAddVehicle_ConfirmVehicleRegister);
                builder.setMessage(R.string.body_SummaryAddVehicle_ConfirmVehicleRegister);
                builder.setPositiveButton(R.string.lbl_positive, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        saveVehicle();
                        Intent intentRegisterSucessful = new Intent(getBaseContext(), ActivityRegisterSuccessful.class);
                        Intent intentHome = new Intent(getBaseContext(), ActivityHome.class);
                        finish();

                        if(callingActivity != null){
                            startActivity(intentHome);
                            Toast.makeText(view.getContext(),
                                    getString(R.string.body_SummaryAddVehicle_RegisterSuccessful),
                                    Toast.LENGTH_LONG).show();
                        }else{
                            startActivity(intentRegisterSucessful);
                            Toast.makeText(view.getContext(),
                                    getString(R.string.body_SummaryAddVehicle_RegisterVehicleSuccessful),
                                    Toast.LENGTH_LONG).show();
                        }
                    }
                });
                builder.setNegativeButton(R.string.lbl_negative, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                        startActivity(new Intent(getBaseContext(), ActivityRegisterSuccessful.class));
                        Toast.makeText(view.getContext(), getString(R.string.body_SummaryAddVehicle_RegisterVehicleSuccessful), Toast.LENGTH_LONG).show();
                    }
                });
                dialogIdentification = builder.create();
                dialogIdentification.show();

                break;

            case R.id.txView_AddVehicle_Sumary_AddNew:
                finish();
                break;
        }
    }

    @Override
    public void onBackPressed() {}

}
