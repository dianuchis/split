package com.cmch.split;

import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.cmch.split.dbref.PointLocation;
import com.cmch.split.tools.FineLocationNeeds;
import com.cmch.split.tools.MapFragmentActivity;
import com.cmch.split.tools.SplitTools;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.firebase.database.DatabaseReference;

/**
 * Created by alex on 07/12/17.
 */

public class ActivityPublishSumaryTrip extends MapFragmentActivity implements View.OnClickListener {

    private Context contextPublishSummary;

    @Override
    public View onCreateView(View parent, String name, Context context, AttributeSet attrs) {
        View view = super.onCreateView(parent, name, context, attrs);
        contextPublishSummary = context;
        return view;

    }

    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_publish_summary_trip);
        fragmentMapId = R.id.sumaryMap;

        TextView button_Sumary_Publish = findViewById(R.id.button_Sumary_Publish);
        TextView button_Sumary_Cancel = findViewById(R.id.button_Sumary_Cancel);
        TextView textViewPublishOrigin = findViewById(R.id.textView_PublishSummary_Origin);
        TextView textViewPublishDestin = findViewById(R.id.textView_PublishSummary_Destin);
        TextView textViewPublishSeats = findViewById(R.id.textView_PublishSummary_Seats);

        textViewPublishOrigin.setText(SplitTools.getShortAddress(ActivityPublishTrips.getToPublish().getOrigin().getAddress()));
        textViewPublishDestin.setText(SplitTools.getShortAddress(ActivityPublishTrips.getToPublish().getDestination().getAddress()));
        textViewPublishSeats.setText(Integer.toString(ActivityPublishTrips.getToPublish().getSeats()));

        button_Sumary_Publish.setOnClickListener(this);
        button_Sumary_Cancel.setOnClickListener(this);
    }

    @Override
    public void OnConnected(Bundle bundle) {}

    @Override
    public void OnLocationChanged(Location location) {}


    //Se encarga de trazar la ruta del viaje cuando el mapa es desplegado en el activity
    @Override
    public void OnMapReady() {
        LatLng origin = new LatLng(Double.parseDouble(ActivityPublishTrips.getToPublish().getOrigin().getLatitude()), Double.parseDouble(ActivityPublishTrips.getToPublish().getOrigin().getLongitude()));
        LatLng destination = new LatLng(Double.parseDouble(ActivityPublishTrips.getToPublish().getDestination().getLatitude()), Double.parseDouble(ActivityPublishTrips.getToPublish().getDestination().getLongitude()));

        LatLngBounds.Builder builder = new LatLngBounds.Builder();
        PolylineOptions route = new PolylineOptions();

        builder.include(origin);
        route.add(origin);
        addMarker(origin, "From");

        if(ActivityPublishTrips.getToPublish().getLayovers() != null){
            for (PointLocation item: ActivityPublishTrips.getToPublish().getLayovers()) {
                LatLng tmpItem = new LatLng(Double.parseDouble(item.getLatitude()), Double.parseDouble(item.getLongitude()));
                builder.include(tmpItem);
                route.add(tmpItem);
                addMarker(tmpItem, "layover");
            }
        }

        builder.include(destination);
        route.add(destination);
        addMarker(destination, "To");

        map.addPolyline(route);

        centerInBounds(builder.build());
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.button_Sumary_Publish:

                toPublishTrip();

                startActivity(new Intent(contextPublishSummary, ActivityHome.class));
                break;

            case R.id.button_Sumary_Cancel:
                finish();
                break;
        }
    }

    public void toPublishTrip(){
        //Referencia para guardar el viaje
        DatabaseReference newTripToPublishRef = SplitApp.getFrbDatabase().child("trips");

        String idTrip = newTripToPublishRef.push().getKey();
        ActivityPublishTrips.getToPublish().setId(idTrip);
        //Guardamos el viaje
        newTripToPublishRef.child(idTrip).setValue(ActivityPublishTrips.getToPublish());

        Toast.makeText(contextPublishSummary,"Trip published", Toast.LENGTH_LONG).show();
    }

}
