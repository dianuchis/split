package com.cmch.split;

import android.content.Context;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.TextView;
import android.widget.Toast;
import com.cmch.split.dbref.PointLocation;
import com.cmch.split.tools.MapFragmentActivity;
import com.cmch.split.adapters.PlaceAutocompleteAdapter;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.location.places.AutocompletePrediction;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.PlaceBuffer;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


/**
 * Created by alex on 16/11/17.
 */

public class Activity_Map1 extends MapFragmentActivity {

    private List<PointLocation> addresses;
    private static Geocoder geocoder;
    private Context contextView;
    private Boolean startingZoom = false;
    private AutoCompleteTextView inputSearch;
    private TextView textCurrentLocation;
    private static LatLngBounds latLngBounds;
    private PlaceAutocompleteAdapter placeAutocompleteAdapter;

    @Override
    public View onCreateView(View parent, String name, Context context, AttributeSet attrs) {
        contextView = context;

        //Transparenta la barra de notificacion
        //si es una version igual o mayor a KitKat
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            getWindow().getDecorView().setSystemUiVisibility(
                    View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                            | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
            getWindow().setStatusBarColor(getResources().getColor(R.color.gradient_gray));
        }
        return super.onCreateView(parent, name, context, attrs);
    }

    @Override
    public void onCreate(Bundle savedIstanceState){
        super.onCreate(savedIstanceState);
        setContentView(R.layout.activity_map1);
        fragmentMapId = R.id.fragmentMap;
        addresses = new ArrayList<>();
        inputSearch = findViewById(R.id.place_autocomplete_search_input);
        inputSearch.setOnItemClickListener(mAutoCompleteClickListener);

        textCurrentLocation = findViewById(R.id.txView_Map1_current_ubication);

        textCurrentLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

               if(!textCurrentLocation.getText().equals("Seleccionar: ")){
                   Intent data = new Intent();
                   data.putExtra("PointLocation", addresses.get(0).toJSon().toString());
                   setResult(RESULT_OK, data);
                   finish();
               }else {
                   Toast.makeText(contextView, "Seleccione un punto", Toast.LENGTH_LONG).show();
               }
            }
        });

        latLngBounds = new LatLngBounds( new LatLng(-50, 0.25), new LatLng(-40, 0.50));
        geocoder = new Geocoder(getApplicationContext());

    }

    private void hideKeyboard(){
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
    }


    private AdapterView.OnItemClickListener mAutoCompleteClickListener = new AdapterView.OnItemClickListener(){
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            hideKeyboard();
            final AutocompletePrediction item = placeAutocompleteAdapter.getItem(position);
            final String placeId = item.getPlaceId();

            PendingResult<PlaceBuffer> placeResult = Places.GeoDataApi.getPlaceById(MapFragmentActivity.getmGoogleApiClient(), placeId);
            placeResult.setResultCallback(mUpdatePlaceDetailsCallBack);
        }
    };


    private ResultCallback<PlaceBuffer> mUpdatePlaceDetailsCallBack = new ResultCallback<PlaceBuffer>() {
        @Override
        public void onResult(@NonNull PlaceBuffer places) {
            if(!places.getStatus().isSuccess()){
                Log.d("ERROR DE LUGAR: ", "La consulta de lugar no se completo correctamente: "+places.getStatus().toString());
                places.release();
                return;
            }

            final Place place = places.get(0);
            Log.d("ONDIRECCION", "onResult place details: "+ place.getAttributions());
            Log.d("ONDIRECCION", "onResult place details: "+ place.getAddress());
            Log.d("ONDIRECCION", "onResult place details: "+ place.getLatLng());

            LatLng l = place.getLatLng();

            getAddresses(l);
            center(l);
            addMarker(l, "Here...");
            zoom(15.0f);

            places.release();

        }
    };


    @Override
    public void OnConnected(Bundle bundle) {}

    @Override
    public void OnLocationChanged(Location location) {}

    @Override
    public void OnMapReady() {

        map.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {
                map.clear();
                addMarker(latLng, "Here...");
                // Animating to the touched position
                //map.animateCamera(CameraUpdateFactory.newLatLng(latLng));

                getAddresses(latLng);

            }
        });

        //Adaptador que muestra texto autocompletado en EditText de búsqueda
        placeAutocompleteAdapter = new PlaceAutocompleteAdapter(
                this,
                MapFragmentActivity.getmGoogleApiClient(),
                latLngBounds, null);

        inputSearch.setAdapter(placeAutocompleteAdapter);

        center(getMylocation());
        zoom(15.0f);
    }


    private void getAddresses(LatLng latLng){
        addresses.clear();
        List<Address> tmpAddresses = null;

        try {
            tmpAddresses = geocoder.getFromLocation(latLng.latitude, latLng.longitude,1);

        } catch (IOException e) {
            Log.d("Error", "Error en geocoder:"+e.toString());
        }

        if(tmpAddresses != null && tmpAddresses.size() > 0 ){
            for (int j = 0; j < tmpAddresses.size(); j++){
                PointLocation address = new PointLocation();
                Address tmpAddress = tmpAddresses.get(j);

                int lineas = tmpAddress.getMaxAddressLineIndex();
                if (lineas >= 0){
                    textCurrentLocation.setText("Seleccionar: "+tmpAddress.getAddressLine(0));
                    address.setAddress(tmpAddress.getAddressLine(0));
                    address.setLatitude(String.valueOf(tmpAddress.getLatitude()));
                    address.setLongitude(String.valueOf(tmpAddress.getLongitude()));
                    address.setLocality(tmpAddress.getLocality());
                    address.setSublocality(tmpAddress.getSubLocality());
                    address.setPostalCode(tmpAddress.getPostalCode());
                    addresses.add(address);
                }
            }
        }
    }

    @Override
    public void onResume(){
        super.onResume();
    }

}
