package com.cmch.split;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.cmch.split.dbref.Trip;
import com.cmch.split.adapters.RecyclerViewPassengersAdapter;


/**
 * A simple {@link Fragment} subclass.
 * Use the {@link FragHomeNotificationsListPassengers#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FragHomeNotificationsListPassengers extends Fragment {

    private RecyclerView.Adapter recyclerViewPassengersAdapter;
    private RecyclerView.LayoutManager layoutManager;
    private RecyclerView recyclerView;


    //Tag of trip arg: Trip scheduled
    private static final String ARG_TRIP_SCHEDULED = "TripScheduled";

    // TODO: Rename and change types of parameters
    private Trip tripScheduled;


    public FragHomeNotificationsListPassengers() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param tripSheduled Viaje agendado por el usuario, contiene la lista de pasajeros.
     * @return A new instance of fragment FragHomeNotificationsListPassengers.
     */
    // TODO: Rename and change types and number of parameters
    public static FragHomeNotificationsListPassengers newInstance(Trip tripSheduled) {
        FragHomeNotificationsListPassengers fragment = new FragHomeNotificationsListPassengers();
        Bundle args = new Bundle();
        args.putParcelable(ARG_TRIP_SCHEDULED, tripSheduled);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            tripScheduled = getArguments().getParcelable(ARG_TRIP_SCHEDULED);
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        final View view = inflater.inflate(R.layout.frag_home_notifications_list_passengers, container, false);

        recyclerView = view.findViewById(R.id.recyclerView_HomeNotifications_ListPassengers);

        layoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);
        recyclerViewPassengersAdapter = new RecyclerViewPassengersAdapter(tripScheduled.getPassengers());
        recyclerView.setAdapter(recyclerViewPassengersAdapter);

        return view;
    }

}
