package com.cmch.split;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.Toast;

import com.cmch.split.baseactivity.ActivitySplit;
import com.cmch.split.dbref.PaymentMethod;
import com.cmch.split.adapters.SpinnerStringAdapter;
import com.cmch.split.tools.ValidateField;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.security.NoSuchAlgorithmException;
import java.util.Collection;
import java.util.List;

import javax.crypto.Cipher;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

public class ActivityAddPayment extends AppCompatActivity implements View.OnClickListener, android.text.TextWatcher{

    private static PaymentMethod p;
    public static PaymentMethod getP() {
        return p;
    }

    RadioButton rbtnCredit;
    RadioButton rbtnDebit;
    Spinner spnTypeCard;
    EditText edtxNameAccount;
    EditText edtxCardNumber;
    Spinner spnYear;
    Spinner spnMonth;
    EditText edtxCVV;
    EditText edtxStreetAndNumber;
    EditText edtxInteriorNumber;
    EditText edtxPostalCode;
    EditText edtxCity;
    EditText edtxState;
    CheckBox ckBoxFavorite;
    Button btnSave;

    SpinnerStringAdapter typeCardAdapter;
    SpinnerStringAdapter yearAdapter;
    SpinnerStringAdapter monthAdapter;

    private boolean validateCardBrand = true, validateCardYear = true, validateCardMoth = true, validateNameAccount,
            validateCardNumer, validateSizeCVV, validateStreet, validateInterior, validatePostal, validateState, validateCity;


    //Alterna el clickado del checkbox tarjeta favorita
    private boolean isCheked = false;

    //Guarda el tamaño maximo de digitos para la tarjeta
    private int sizeCardDigits = 0;

    //Guarda el tamaño maximo de digitos para el CVV
    private int sizeCvvDigits = 0;

    //Indica si todos los campos estan correctos
    private boolean fieldsCompleted = false;

    Cipher cipher;
    private String iv = "fedcba9876543210";
    private IvParameterSpec ivspec;
    private SecretKeySpec keyspec;
    private String SecretKey = "0123456789abcdef";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_payment);

        rbtnCredit = findViewById(R.id.radiobtn_AddPayment_Credit);
        rbtnDebit = findViewById(R.id.radiobtn_AddPayment_Debit);
        spnTypeCard = findViewById(R.id.spin_AddPayment_TypeCard);

        edtxNameAccount = findViewById(R.id.textView_AddPayment_NameAccount);
        edtxNameAccount.setEnabled(false);
        edtxNameAccount.setText(ActivitySplit.tmpUser.getFullName());
        edtxNameAccount.addTextChangedListener(this);

        edtxCardNumber = findViewById(R.id.textView_AddPayment_CardNumber);
        edtxCardNumber.addTextChangedListener(this);

        spnYear = findViewById(R.id.spin_AddPayment_Year);
        spnMonth = findViewById(R.id.spin_AddPayment_Month);

        edtxCVV = findViewById(R.id.textView_AddPayment_CVV);
        edtxCVV.addTextChangedListener(this);

        edtxStreetAndNumber = findViewById(R.id.textView_AddPayment_StreetAndNumber);
        edtxStreetAndNumber.addTextChangedListener(this);

        edtxInteriorNumber = findViewById(R.id.editText_AddPayment_InteriorNumber);
        edtxInteriorNumber.addTextChangedListener(this);

        edtxPostalCode = findViewById(R.id.editText_AddPayment_ZipCode);
        edtxPostalCode.addTextChangedListener(this);

        edtxCity = findViewById(R.id.editText_AddPayment_City);
        edtxCity.addTextChangedListener(this);

        edtxState = findViewById(R.id.editText_AddPayment_State);
        edtxState.addTextChangedListener(this);

        ckBoxFavorite = findViewById(R.id.checkbox_AddPayment_Favorite);
        btnSave = findViewById(R.id.button_AddPayment_Save);

        btnSave.setOnClickListener(this);

        View current = getCurrentFocus();
        if (current != null) current.clearFocus();

        String [] cardType = new String [4];
        cardType[0] = "Card Brand";
        cardType[1] = "Visa";
        cardType[2] = "Master Card";
        cardType[3] = "American Express";

        String [] years = new String [15];
        years[0] = "year";
        years[1] = "2030";
        years[2] = "2029";
        years[3] = "2028";
        years[4] = "2027";
        years[5] = "2026";
        years[6] = "2025";
        years[7] = "2024";
        years[8] = "2023";
        years[9] = "2022";
        years[10] = "2021";
        years[11] = "2020";
        years[12] = "2019";
        years[13] = "2018";
        years[14] = "2017";

        String [] months = new String [13];
        months[0] = "month";
        months[1] = "12";
        months[2] = "11";
        months[3] = "10";
        months[4] = "09";
        months[5] = "08";
        months[6] = "07";
        months[7] = "06";
        months[8] = "05";
        months[9] = "04";
        months[10] = "03";
        months[11] = "02";
        months[12] = "01";

        typeCardAdapter = new SpinnerStringAdapter(this, cardType);
        spnTypeCard.setAdapter(typeCardAdapter);

        yearAdapter = new SpinnerStringAdapter(this, years);
        spnYear.setAdapter(yearAdapter);

        monthAdapter = new SpinnerStringAdapter(this, months);
        spnMonth.setAdapter(monthAdapter);

        rbtnDebit.setChecked(true);

        //Init the payment method
        p = new PaymentMethod();

        rbtnCredit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rbtnCredit.setChecked(true);
                if(rbtnCredit.isChecked()){p.setCardType("Credit");}
            }
        });

        rbtnDebit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rbtnDebit.setChecked(true);
                if(rbtnDebit.isChecked()){p.setCardType("Debit");}
            }
        });


        spnTypeCard.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if(spnTypeCard.getSelectedItem().equals("Visa") || spnTypeCard.getSelectedItem().equals("Master Card")){
                        sizeCardDigits = 16;
                        sizeCvvDigits = 3;
                }else {
                    sizeCardDigits = 15;
                    sizeCvvDigits = 4;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        ckBoxFavorite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isCheked = !isCheked;
                CheckBox r = v.findViewById(R.id.checkbox_AddPayment_Favorite);
                r.setChecked(isCheked);
            }
        });



    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.button_AddPayment_Save:

                if(validatePaymentMethod()){
                    //Guardamos los datos en la tarjeta
                    savePaymentMethod();

                    Type collectionType = new TypeToken<Collection<PaymentMethod>>(){}.getType();
                    Gson gson = new Gson();

                    if(ckBoxFavorite.isChecked()){

                        //Obtengo las tarjetas guardadas
                        SharedPreferences sharedPref = getBaseContext().getSharedPreferences("Cards", Context.MODE_PRIVATE);
                        String ListCard = sharedPref.getString("cards", "[]");

                        //Deserializo a una lista de tarjetas
                        ActivityPaymentMethod.setCards((List<PaymentMethod>) gson.fromJson(ListCard, collectionType));

                        //Como se selecciono que la tarjeta sera favorita
                        //Agregamos la tarjeta favorita al principio
                        ActivityPaymentMethod.getCards().add(0, p);

                        //Serializamos nuevamente las tarjetas
                        String newGsonCards = gson.toJson(ActivityPaymentMethod.getCards(), collectionType);

                        //Guardamos en las preferencias las nuevas tarjetas con la favorita
                        SharedPreferences.Editor editor = sharedPref.edit();
                        editor.putString("cards", newGsonCards);
                        editor.commit();

                        Log.d("Tarjetas", ActivityPaymentMethod.getCards().toString());

                        //Notificamos que cambio el numero de tarjetas
                        ActivityPaymentMethod.getRecyclerPayMethodsAdapter().notifyDataSetChanged();
                        //Notificamos al adaptador sobre el item insertado                      Como es favorita se guardo al principio
                        ActivityPaymentMethod.getRecyclerPayMethodsAdapter().notifyItemInserted(0);
                        finish();

                    }else{

                        //Obtengo las tarjetas guardadas
                        SharedPreferences sharedPref = getBaseContext().getSharedPreferences("Cards", Context.MODE_PRIVATE);
                        String ListCard = sharedPref.getString("cards", "[]");

                        //Deserializo a una lista de tarjetas
                        ActivityPaymentMethod.setCards((List<PaymentMethod>) gson.fromJson(ListCard, collectionType));

                        //Como se selecciono que la tarjeta sera favorita
                        //Agregamos la tarjeta favorita al principio
                        ActivityPaymentMethod.getCards().add(p);

                        //Serializamos nuevamente las tarjetas
                        String newGsonCards = gson.toJson(ActivityPaymentMethod.getCards(), collectionType);

                        //Guardamos en las preferencias las nuevas tarjetas con la favorita
                        SharedPreferences.Editor editor = sharedPref.edit();
                        editor.putString("cards", newGsonCards);
                        editor.commit();

                        Log.d("Tarjetas", ActivityPaymentMethod.getCards().toString());

                        //Notificamos al adaptador sobre el item insertado
                        ActivityPaymentMethod.getRecyclerPayMethodsAdapter().notifyDataSetChanged();
                        //                                                                              Posicion del ultimo elemento insertado
                        ActivityPaymentMethod.getRecyclerPayMethodsAdapter().notifyItemInserted(ActivityPaymentMethod.getRecyclerPayMethodsAdapter().getItemCount());

                        finish();
                    }

                    //Cifrado
//                    initCipher();
//                    try {
//                        Log.d("ENCRYPTED: ------", byteArrayToHexString(encrypt("hola")));
//                    } catch (Exception e) {
//                        e.printStackTrace();
//                    }

                }else Toast.makeText(this, "Verify the fields", Toast.LENGTH_LONG).show();

            break;
        }
    }

    public void initCipher(){
        ivspec = new IvParameterSpec(iv.getBytes());
        keyspec = new SecretKeySpec(SecretKey.getBytes(), "AES");

        try {
            cipher = Cipher.getInstance ("DES/CBC/PKCS5Padding");

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (NoSuchPaddingException e) {
            e.printStackTrace();
        }
    }

    public byte[] encrypt(String text) throws Exception {
        if (text == null || text.length() == 0)
            throw new Exception("Empty string");

        byte[] encrypted = null;
        try {
            // Cipher.ENCRYPT_MODE = Constant for encryption operation mode.
            cipher.init(Cipher.ENCRYPT_MODE, keyspec, ivspec);
            encrypted = cipher.doFinal(padString(text).getBytes());

        } catch (Exception e) {
            throw new Exception("[encrypt] " + e.getMessage());
        }
        return encrypted;
    }

    public byte[] decrypt(String text) throws Exception {
        if (text == null || text.length() == 0)
            throw new Exception("Empty string");

        byte[] decrypted = null;
        try {
            // Cipher.DECRYPT_MODE = Constant for decryption operation mode.
            cipher.init(Cipher.DECRYPT_MODE, keyspec, ivspec);

            decrypted = cipher.doFinal(hexToBytes(text));
        } catch (Exception e) {
            throw new Exception("[decrypt] " + e.getMessage());
        }
        return decrypted;
    }

    public static byte[] hexToBytes(String str) {
        if (str == null) {
            return null;
        } else if (str.length() < 2) {
            return null;
        } else {

            int len = str.length() / 2;
            byte[] buffer = new byte[len];
            for (int i = 0; i < len; i++) {
                buffer[i] = (byte) Integer.parseInt(
                        str.substring(i * 2, i * 2 + 2), 16);

            }
            return buffer;
        }
    }

    public static String byteArrayToHexString(byte[] array) {
        StringBuffer hexString = new StringBuffer();
        for (byte b : array) {
            int intVal = b & 0xff;
            if (intVal < 0x10)
                hexString.append("0");
            hexString.append(Integer.toHexString(intVal));
        }
        return hexString.toString();
    }

    private static String padString(String source) {
        char paddingChar = 0;
        int size = 16;
        int x = source.length() % size;
        int padLength = size - x;
        for (int i = 0; i < padLength; i++) {
            source += paddingChar;
        }
        return source;
    }

    private void savePaymentMethod(){
        //Agregamos el tipo de tarjeta e imagen
        p.setCardTypeBank(spnTypeCard.getSelectedItem().toString());
        if(spnTypeCard.getSelectedItem().toString().equals("Visa")){
            p.setIdImagePayMethod(R.mipmap.ic_payment_visa);}
        else if(spnTypeCard.getSelectedItem().toString().equals("Master Card")){
            p.setIdImagePayMethod(R.mipmap.ic_payment_mastercard);
        }else if(spnTypeCard.getSelectedItem().toString().equals("American Express")){
            p.setIdImagePayMethod(R.mipmap.ic_payment_amex);}
        p.setAccountHolder(edtxNameAccount.getText().toString());
        p.setCardNumber(edtxCardNumber.getText().toString());
        p.setExpirationYear(spnYear.getSelectedItem().toString());
        p.setExpirationYear(spnYear.getSelectedItem().toString());
        p.setCvv(edtxCVV.getText().toString());
        p.setStreetAndNumber(edtxStreetAndNumber.getText().toString());
        p.setInteriorNumber(edtxInteriorNumber.getText().toString());
        p.setZipcode(edtxPostalCode.getText().toString());
        p.setCity(edtxCity.getText().toString());
    }

    //Todo: Cuando funcione, devolver un boolean que nos permita
    //saber si se hizo correctamente
    //el cargo a la tarjeta.
    public boolean validatePaymentMethod(){

        //Toast.makeText(this, "Please select a card brand", Toast.LENGTH_SHORT).show();
        if (spnTypeCard.getSelectedItem().toString().equals("Card Brand")){ validateCardBrand = false; }
        if(spnYear.getSelectedItem().toString().equals("year")){ validateCardYear = false; }
        if(spnMonth.getSelectedItem().toString().equals("month")){ validateCardMoth = false; }

        if(validateCardBrand && validateCardYear && validateCardMoth && validateNameAccount && validateCardNumer &&
           validateSizeCVV && validateStreet && validateInterior && validatePostal && validateState && validateCity){
            return true;
        }else{ return false; }

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

        validateNameAccount =  ValidateField.fieldSize(edtxNameAccount, 25);
        validateCardNumer = ValidateField.exactSize(edtxCardNumber, sizeCardDigits);
        validateSizeCVV = ValidateField.exactSize(edtxCVV, sizeCvvDigits);
        validateStreet = ValidateField.fieldSize(edtxStreetAndNumber, 10);
        validateInterior = ValidateField.fieldSize(edtxInteriorNumber, 3);
        validatePostal = ValidateField.fieldSize(edtxPostalCode, 5);
        validateState = ValidateField.fieldSize(edtxState, 25);
        validateCity = ValidateField.fieldSize(edtxCity, 15);
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

    @Override
    public void afterTextChanged(Editable s) {}
}
