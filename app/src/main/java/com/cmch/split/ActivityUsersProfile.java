package com.cmch.split;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.cmch.split.dbref.User;
import com.cmch.split.tools.SplitTools;

public class ActivityUsersProfile extends AppCompatActivity {

    //Tag of user arg: User Selected
    private static final String ARG_USER_SELECTED = "UserSelected";
    private User tmpUser;

    private TextView textView_UsersProfile_Name;
    private TextView textView_UsersProfile_Description;
    private TextView textView_UsersProfile_State;
    private ImageView img_UsersProfile_imgIdValidated;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_users_profile);

        Bundle bundle = getIntent().getExtras();

        textView_UsersProfile_Name = findViewById(R.id.textView_UsersProfile_Name);
        textView_UsersProfile_Description = findViewById(R.id.textView_UsersProfile_Description);
        textView_UsersProfile_State = findViewById(R.id.textView_UsersProfile_State);
        img_UsersProfile_imgIdValidated = findViewById(R.id.img_UsersProfile_imgIdValidated);

        if( bundle != null){
            tmpUser = bundle.getParcelable(ARG_USER_SELECTED);
            textView_UsersProfile_Name.setText(SplitTools.getShortName(tmpUser.getFullName()));
            textView_UsersProfile_State.setText(tmpUser.getCountry());
        }
    }
}
