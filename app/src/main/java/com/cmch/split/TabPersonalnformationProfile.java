package com.cmch.split;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.cmch.split.baseactivity.ActivitySplit;
import com.cmch.split.tools.SplitTools;
import com.google.firebase.storage.StorageReference;
import com.squareup.picasso.Picasso;

/**
 * Created by CC on 05-Mar-18.
 */

public class TabPersonalnformationProfile extends Fragment {

    Button btn_edit;
    EditText mail;
    EditText password;
    EditText cellphone;
    EditText gender;
    EditText group;
    EditText birthDay;
    EditText address;
    EditText zipCode;
    EditText location;
    ImageView imView_PersonalInf_PhotoProfile;
    TextView textView_Profile_name;
    private StorageReference storagePhotoReference;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View myview = inflater.inflate(R.layout.frag_tab_personal_information, container, false);

        imView_PersonalInf_PhotoProfile = myview.findViewById(R.id.imView_PersonalInf_PhotoProfile);
        btn_edit = myview.findViewById(R.id.btn_Profile_Edit);
        mail = myview.findViewById(R.id.textView_Profile_email);
        password = myview.findViewById(R.id.textView_Profile_password);
        cellphone = myview.findViewById(R.id.textView_Profile_cellphone);
        gender = myview.findViewById(R.id.textView_Profile_gender);
        group = myview.findViewById(R.id.textView_Profile_workplace);
        birthDay = myview.findViewById(R.id.textView_Profile_birthday);
        zipCode = myview.findViewById(R.id.textView_Profile_zipCode);
        location = myview.findViewById(R.id.textView_Profile_location);
        address = myview.findViewById(R.id.textView_Profile_address);
        textView_Profile_name = myview.findViewById(R.id.textView_Profile_name);

        mail.setEnabled(false);
        password.setEnabled(false);
        cellphone.setEnabled(false);
        gender.setEnabled(false);
        group.setEnabled(false);
        birthDay.setEnabled(false);
        address.setEnabled(false);
        zipCode.setEnabled(false);
        location.setEnabled(false);

        cellphone.setText(ActivitySplit.tmpUser.getCellphone());
        mail.setText(ActivitySplit.tmpUser.getEmail());
        gender.setText(ActivitySplit.tmpUser.getGender());
        group.setText(ActivitySplit.tmpUser.getWorkplace());
        birthDay.setText(ActivitySplit.tmpUser.getBirthday());
        zipCode.setText(ActivitySplit.tmpUser.getZipcode());
        location.setText(ActivitySplit.tmpUser.getState()+" "+SplitApp.getAppUser().getCountry());
        address.setText(ActivitySplit.tmpUser.getStreetAndNumber());
        textView_Profile_name.setText(ActivitySplit.tmpUser.getFullName());
        Picasso.get().load(ActivitySplit.uriProfilePhoto).transform(SplitTools.newInstaceCircleTransform()).into(imView_PersonalInf_PhotoProfile);

        btn_edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mail.setEnabled(true);
                password.setEnabled(true);
                cellphone.setEnabled(true);
                gender.setEnabled(true);
                group.setEnabled(true);
                birthDay.setEnabled(true);
                address.setEnabled(true);
                zipCode.setEnabled(true);
                location.setEnabled(true);
            }
        });

        return myview;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        SplitTools.hideKeyboard(this.getActivity());


    }
}