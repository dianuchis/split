package com.cmch.split.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import java.util.List;

/**
 * Created by alex on 14/10/17.
 */
public abstract class ListViewAdapter extends BaseAdapter {

    private List<?> values;
    private Context context;
    private int layoutId;

    public ListViewAdapter(Context context, List<?> values, int layoutId){
        super();
        this.context = context;
        this.values = values;
        this.layoutId = layoutId;
    }


    @Override
    public int getCount() {
        return values.size();
    }

    @Override
    public Object getItem(int index) {
        return values.get(index);
    }

    @Override
    public long getItemId(int index) {
        return 0;
    }

    @Override
    public View getView(int index, View view, ViewGroup viewGroup) {
        LayoutInflater layoutInflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        view = layoutInflater.inflate(layoutId, viewGroup, false);
        onGetView(index, view);
        return view;
    }

    public abstract void onGetView(int index, View view);
}
