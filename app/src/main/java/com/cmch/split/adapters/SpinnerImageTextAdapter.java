package com.cmch.split.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.cmch.split.R;
import com.cmch.split.modelviews.ImageTextItems;

import java.util.ArrayList;

/**
 * Created by alex on 27/01/18.
 */

public class SpinnerImageTextAdapter extends BaseAdapter {


    private ArrayList<ImageTextItems> items;
    private LayoutInflater layoutInflater;

    public SpinnerImageTextAdapter(Context context, ArrayList<ImageTextItems> items){
        this.items = items;
        this.layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup viewGroup){
        if(convertView==null){
            convertView = layoutInflater.inflate(R.layout.item_spinner_image_text_view, viewGroup, false);
        }

        ImageView imageView = convertView.findViewById(R.id.item_image);
        imageView.setImageResource(getItem(position).getImageId());

        TextView textView = convertView.findViewById(R.id.item_text);
        textView.setText(getItem(position).getText());

        return convertView;
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup viewGroup){
        if(convertView==null){
            convertView = layoutInflater.inflate(R.layout.item_spinner_image_text_dropdown_view, viewGroup, false);
        }

        ImageView imageView = convertView.findViewById(R.id.item_dropdown_image);
        imageView.setImageResource(getItem(position).getImageId());

        TextView textView = convertView.findViewById(R.id.item_dropdown_text);
        textView.setText(getItem(position).getText());

        return convertView;
    }

    @Override
    public int getCount(){ return items.size(); }

    @Override
    public ImageTextItems getItem(int position){ return items.get(position); }

    @Override
    public long getItemId(int position){ return position; }
}