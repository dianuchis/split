package com.cmch.split.adapters;

/**
 * Created by CC on 13-Mar-18.
 */

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.cmch.split.R;
import com.cmch.split.TabHomeNotifications;
import com.cmch.split.dbref.User;

import java.util.List;


public class RecyclerViewPassengersAdapter extends RecyclerView.Adapter<RecyclerViewPassengersAdapter.ViewHolder> {
    private static List<User> mDataset;


    public static class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public TextView textViewFullname;
        public TextView textViewCellphone;

        public ViewHolder(View v) {
            super(v);
            textViewFullname = v.findViewById(R.id.textView_item_Passenger_name);
            textViewCellphone = v.findViewById(R.id.textView_item_Passenger_cellphone);


        }
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public RecyclerViewPassengersAdapter(List<User> myDataset) {
        mDataset = myDataset;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public RecyclerViewPassengersAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        View v = TabHomeNotifications.getInflater().inflate(R.layout.item_home_notification_passenger, parent, false);

        TextView textViewFullname = v.findViewById(R.id.textView_item_Passenger_name);
        TextView textViewCellphone = v.findViewById(R.id.textView_item_Passenger_cellphone);


//        if(textView.getParent()!=null)
//            ((ViewGroup)textView.getParent()).removeView(textView);
//            ((ViewGroup)imageView.getParent()).removeView(imageView);// <- fix

        return new ViewHolder(v);
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        holder.textViewFullname.setText(mDataset.get(position).getFullName());
        holder.textViewCellphone.setText(mDataset.get(position).getCellphone());


    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mDataset.size();
    }
}