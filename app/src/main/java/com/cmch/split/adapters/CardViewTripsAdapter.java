package com.cmch.split.adapters;

import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.cmch.split.ActivityScheduleDetailTrip;
import com.cmch.split.R;
import com.cmch.split.dbref.Trip;
import com.cmch.split.tools.SplitTools;

import java.util.ArrayList;

/**
 * Created by CC on 02-Mar-18.
 */

public class CardViewTripsAdapter extends RecyclerView.Adapter<CardViewTripsAdapter.ViewHolder> {
    private static ArrayList<Trip> dataset;


    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    static class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        TextView textViewHeader;
        TextView textViewTime;
        TextView textViewOrigin;
        TextView textViewDestin;
        TextView textViewSeatsNumber;
        TextView textViewRate;

        ViewHolder(View v) {
            super(v);

            textViewHeader = v.findViewById(R.id.txView_register_sumary_header);
            textViewTime = v.findViewById(R.id.textView_Schedule_Summary_Time);
            textViewOrigin = v.findViewById(R.id.textView_Schedule_Summary_Origin);
            textViewDestin = v.findViewById(R.id.textView_Schedule_Summary_Destin);
            textViewSeatsNumber = v.findViewById(R.id.textView_Schedule_Summary_Seats);
            textViewRate = v.findViewById(R.id.textView_Schedule_Summary_Rate);

            v.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(final View v) {

                    Trip tripSelected = dataset.get((Integer) textViewRate.getTag());
                    Intent iDetailTrip = new Intent(v.getContext(), ActivityScheduleDetailTrip.class);
                    iDetailTrip.putExtra("trips", tripSelected);
                    v.getContext().startActivity(iDetailTrip,null);
                }
            });


        }
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public CardViewTripsAdapter(ArrayList<Trip> myDataset) {
        dataset = myDataset;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public CardViewTripsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                                int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_view_summary_new_trips, parent, false);

         return new ViewHolder(v);
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {

        Trip itemTrip = dataset.get(position);

        String origin = SplitTools.getShortAddress(itemTrip.getOrigin().asString());
        String destin = SplitTools.getShortAddress(itemTrip.getDestination().asString());
        int seats = itemTrip.getSeats();

        holder.textViewTime.setText("Trip time: "+itemTrip.getTime()+" Hrs");
        holder.textViewOrigin.setText(origin);
        holder.textViewDestin.setText(destin);

        //Muestra el plural o singular de asiento en el cardView segun sea 1 asiento o mas.
        if (seats > 1) {
            holder.textViewSeatsNumber.setText(Integer.toString(itemTrip.getSeats())+" asientos disponibles");
        }else {
            holder.textViewSeatsNumber.setText(Integer.toString(itemTrip.getSeats()) + " asiento   disponible");
        }
        holder.textViewRate.setText(Double.toString(itemTrip.getPrice())+" MXN");

        holder.textViewRate.setTag(position);

    }

    @Override
    public void onViewRecycled(ViewHolder holder) {
        super.onViewRecycled(holder);
        holder.setIsRecyclable(true);
    }

    @Override
    public long getItemId(int position) {
        return super.getItemId(position);

    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return dataset.size();
    }

}

