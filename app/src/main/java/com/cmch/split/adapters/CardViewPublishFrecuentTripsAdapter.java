package com.cmch.split.adapters;

import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.cmch.split.ActivityPublishTrips;
import com.cmch.split.R;
import com.cmch.split.TabPublishFrequentTrip;
import com.cmch.split.dbref.Trip;
import com.cmch.split.tools.SplitTools;

import java.util.ArrayList;

/**
 * Created by CC on 02-Mar-18.
 */

public class CardViewPublishFrecuentTripsAdapter extends RecyclerView.Adapter<CardViewPublishFrecuentTripsAdapter.ViewHolder> {
    private static ArrayList<Trip> dataset;


    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    static class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        TextView textViewOrigin;
        TextView textViewDestin;
        TextView textViewHeader;
        TextView textViewPrice;

        ViewHolder(View v) {
            super(v);

            textViewOrigin = v.findViewById(R.id.textView_Schedule_FrecuentTrips_Origin);
            textViewDestin = v.findViewById(R.id.textView_Schedule_FrecuentTrips_Destin);
            textViewHeader = v.findViewById(R.id.textView_Schedule_FrecuentTrips_Header);
            textViewPrice = v.findViewById(R.id.textView_Schedule_FrecuentTrips_Price);

            v.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(final View v) {

                    TabPublishFrequentTrip tft = new TabPublishFrequentTrip();
                    Bundle args = new Bundle();
                    args.putParcelable("frequentTrip", getItem((Integer)textViewHeader.getTag()));
                    tft.setArguments(args);
                    Log.d("ARGS",tft.getArguments().toString());

                    //Reemplaza el recyclerView para
                    FragmentTransaction transaction = ActivityPublishTrips.getFm()
                            .beginTransaction();
		/*
		 * When this container fragment is created, we fill it with our first
		 * "real" fragment
		 */
                    transaction.replace(R.id.root_frame_publish_frecuent_trips,
                            tft);

                    //Agregamos una transicion y lo ponemos en la pila de fragments al inicio
                    transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
                    transaction.addToBackStack(null);
                    transaction.commit();

                }
            });
        }

        public Trip getItem(int position){
            return dataset.get(position);
        }
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public CardViewPublishFrecuentTripsAdapter(ArrayList<Trip> myDataset) {
        dataset = myDataset;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public CardViewPublishFrecuentTripsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                                             int viewType) {
        // Esta vista es la que esta duplicada
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_card_view_frecuent_trips, parent, false);

         return new ViewHolder(v);
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {

        Trip itemTrip = dataset.get(position);

        String origin = SplitTools.getShortAddress(itemTrip.getOrigin().asString());
        String destin = SplitTools.getShortAddress(itemTrip.getDestination().asString());

//        holder.textViewOrigin.setText(origin);
//        holder.textViewDestin.setText(destin);
        holder.textViewHeader.setText(origin+" -"+destin);
        holder.textViewHeader.setTag(position);
        holder.textViewPrice.setText("$"+itemTrip.getPrice()+" MXN");

    }

    @Override
    public void onViewRecycled(ViewHolder holder) {
        super.onViewRecycled(holder);
        holder.setIsRecyclable(true);
    }

    @Override
    public long getItemId(int position) {
        return super.getItemId(position);

    }


    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return dataset.size();
    }

}

