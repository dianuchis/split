package com.cmch.split.adapters;

import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.cmch.split.ActivityHome;
import com.cmch.split.FragHomeNotificationsListPassengers;
import com.cmch.split.FragHomeNotificationsTripVisualization;
import com.cmch.split.R;
import com.cmch.split.SplitApp;
import com.cmch.split.dbref.Trip;
import com.cmch.split.tools.SplitTools;

import java.util.ArrayList;

/**
 * Created by CC on 02-Mar-18.
 */

public class CardViewHomeNotificationsAdapter extends RecyclerView.Adapter<CardViewHomeNotificationsAdapter.ViewHolder> {
    private static ArrayList<Trip> dataset;


    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    static class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        TextView textViewMsgNotification;
        TextView textViewDate;
        TextView textViewTime;
        TextView textViewNotificationHeader;

        ViewHolder(View v) {
            super(v);

            textViewMsgNotification = v.findViewById(R.id.textView_Home_TabNotifications_msgNotification);
            textViewDate = v.findViewById(R.id.textView_Home_TabNotifications_Date);
            textViewTime = v.findViewById(R.id.textView_Home_TabNotifications_Time);
            textViewNotificationHeader = v.findViewById(R.id.textView_Home_TabNotifications_Header);

            //Evento que muestra los datos del viaje frecuente seleccionado en un nuevo fragmento.
            v.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(final View v) {
                    FragmentTransaction transaction = ActivityHome.getFm()
                            .beginTransaction();

                    //Reemplazamos el fragmento de viajes frecuentes(RecyclerView con cardViews),
                    //por un fragmento que contiene los datos del viaje frecuente selccionado.

                    if(textViewNotificationHeader.getTag().equals("TripPublished")){
                        transaction.replace(R.id.root_frame_home_notifications,
                                FragHomeNotificationsListPassengers.newInstance(getItem((Integer)textViewMsgNotification.getTag())));
                    }else{
                        transaction.replace(R.id.root_frame_home_notifications,
                                FragHomeNotificationsTripVisualization.newInstance(getItem((Integer)textViewMsgNotification.getTag())));
                    }


                    //Agregamos una transicion y lo ponemos en la pila de fragments al inicio
                    transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
                    transaction.addToBackStack(null);
                    transaction.commit();
                }
            });
        }

        public Trip getItem(int position){
            return dataset.get(position);
        }
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public CardViewHomeNotificationsAdapter(ArrayList<Trip> myDataset) {
        dataset = myDataset;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public CardViewHomeNotificationsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                                          int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_card_view_home_notifications, parent, false);

         return new ViewHolder(v);
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {

        Trip itemTripScheduled = dataset.get(position);

        String origin = SplitTools.getShortAddress(itemTripScheduled.getOrigin().asString());
        String destin = SplitTools.getShortAddress(itemTripScheduled.getDestination().asString());

//        holder.textViewOrigin.setText(origin);
//        holder.textViewDestin.setText(destin);
        holder.textViewDate.setText(itemTripScheduled.getDate().get(0));
        holder.textViewTime.setText(itemTripScheduled.getTime().get(0));

        //Si el viaje tiene mi id de conductor, muestro un mensaje que indica
        //que alguien ha reservado el viaje que publique.
        //Si no muestro que el viaje lo he agendado como pasajero.
        if(itemTripScheduled.getDriver().getId().equals(SplitApp.getFrbUserId())){
            holder.textViewMsgNotification.setText(SplitApp.getResourceSplit().getString(R.string.lbl_TabHomeNotification_BookDriver));
            holder.textViewNotificationHeader.setTag("TripPublished");
        }else{
            holder.textViewMsgNotification.setText(SplitApp.getResourceSplit().getString(R.string.lbl_TabHomeNotification_BookPassenger));
            holder.textViewNotificationHeader.setTag("TripScheduled");
        }

        //En el textView de mensaje, asignamos una etiqueta que lleva la posicion actual
        //del viaje seleccionado, para mandarlo posteriormente al fragmento de usuarios suscritos
        holder.textViewMsgNotification.setTag(position);

    }

    @Override
    public void onViewRecycled(ViewHolder holder) {
        super.onViewRecycled(holder);
        holder.setIsRecyclable(true);
    }

    @Override
    public long getItemId(int position) {
        return super.getItemId(position);

    }


    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return dataset.size();
    }

}

