package com.cmch.split.adapters;

/**
 * Created by CC on 13-Mar-18.
 */

import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.cmch.split.ActivityUsersProfile;
import com.cmch.split.R;
import com.cmch.split.TabHomeNotifications;
import com.cmch.split.dbref.User;

import java.util.List;


public class RecyclerViewDriverPassengersAdapter extends RecyclerView.Adapter<RecyclerViewDriverPassengersAdapter.ViewHolder> {
    private static List<User> mDataset;


    public static class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public TextView textViewFullname;

        public ViewHolder(View v) {
            super(v);
            textViewFullname = v.findViewById(R.id.textView_trip_visualization_Name);

            v.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    //Abre el activity ActivityUsersProfile con los datos del
                    //usuario (pasajero) seleccionado.
                    Intent intentUsersProfile = new Intent(v.getContext(), ActivityUsersProfile.class);
                    intentUsersProfile.putExtra("UserSelected",getUser((Integer)textViewFullname.getTag()));
                    v.getContext().startActivity(intentUsersProfile);
                }
            });

        }
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public RecyclerViewDriverPassengersAdapter(List<User> myDataset) {
        mDataset = myDataset;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public RecyclerViewDriverPassengersAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        View v = TabHomeNotifications.getInflater().inflate(R.layout.item_home_notification_trip_visualization_driver_passengers, parent, false);

        return new ViewHolder(v);
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        holder.textViewFullname.setText(mDataset.get(position).getFullName());
        holder.textViewFullname.setTag(position);


    }

    private static User getUser(int position){
        return mDataset.get(position);
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mDataset.size();
    }
}