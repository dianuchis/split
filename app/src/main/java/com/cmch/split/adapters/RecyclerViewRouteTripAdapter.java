package com.cmch.split.adapters;

/**
 * Created by CC on 13-Mar-18.
 */

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.cmch.split.R;
import com.cmch.split.TabHomeNotifications;
import com.cmch.split.dbref.PointLocation;
import com.cmch.split.tools.SplitTools;

import java.util.ArrayList;


public class RecyclerViewRouteTripAdapter extends RecyclerView.Adapter<RecyclerViewRouteTripAdapter.ViewHolder> {
    private static ArrayList<PointLocation> mDataset;


    public static class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        private TextView textViewOriginDestination;
        private TextView textViewAddress;
        private TextView textViewSchedule;

        public ViewHolder(View v) {
            super(v);
            textViewOriginDestination = v.findViewById(R.id.lbl_home_notifications_routeTrip_origin_destin);
            textViewAddress = v.findViewById(R.id.lbl_home_notifications_routeTrip_address);
            textViewSchedule = v.findViewById(R.id.lbl_home_notifications_routeTrip_schedule);

        }
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public RecyclerViewRouteTripAdapter(ArrayList<PointLocation> myDataset) {
        mDataset = myDataset;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public RecyclerViewRouteTripAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        View v = TabHomeNotifications.getInflater().inflate(R.layout.item_home_notification_route_trip, parent, false);


        return new ViewHolder(v);
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        String originDestination="";

        if(position < getItemCount()-1){
            originDestination = SplitTools.getShortAddress(mDataset.get(position).getAddress())+" - "+
                                SplitTools.getShortAddress(mDataset.get(getItemCount()-1).getAddress());
        }else{
            originDestination = SplitTools.getShortAddress(mDataset.get(position).getAddress());
        }


        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        holder.textViewOriginDestination.setText(originDestination);
        holder.textViewAddress.setText(mDataset.get(position).getAddress());
      //  holder.textViewSchedule.setText(time);



    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mDataset.size();
    }
}