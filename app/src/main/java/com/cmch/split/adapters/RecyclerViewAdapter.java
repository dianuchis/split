package com.cmch.split.adapters;

/**
 * Created by CC on 13-Mar-18.
 */

import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.cmch.split.ActivityAddVehicle;
import com.cmch.split.R;
import com.cmch.split.TabProfileOptions;
import com.cmch.split.modelviews.RecyclerProfileOptions;

import java.util.ArrayList;


public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder> {
    private static ArrayList<RecyclerProfileOptions> mDataset;


    public static class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public TextView mTextView;
        public ImageView mImageView;

        public ViewHolder(View v) {
            super(v);
            mTextView = v.findViewById(R.id.recyler_profileOptions_textview_item);
            mImageView = v.findViewById(R.id.recyler_profileOptions_image_item);

            mTextView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Intent itemIntent = new Intent(v.getContext(), mDataset.get((Integer) mTextView.getTag()).getActivity());

                    RecyclerProfileOptions tmpItem = mDataset.get((Integer) mTextView.getTag());
                    if(tmpItem.getActivity().equals(ActivityAddVehicle.class))
                    {
                        itemIntent.putExtra("callingActivity", "TabProfileOptions");
                    }
                    v.getContext().startActivity(itemIntent);
                }
            });
        }
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public RecyclerViewAdapter(ArrayList<RecyclerProfileOptions> myDataset) {
        mDataset = myDataset;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public RecyclerViewAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        View v = TabProfileOptions.getInflater().inflate(R.layout.tab_profile_options_item, parent, false);

        TextView textView = v.findViewById(R.id.recyler_profileOptions_textview_item);
        ImageView imageView = v.findViewById(R.id.recyler_profileOptions_image_item);


//        if(textView.getParent()!=null)
//            ((ViewGroup)textView.getParent()).removeView(textView);
//            ((ViewGroup)imageView.getParent()).removeView(imageView);// <- fix

        return new ViewHolder(v);
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        holder.mTextView.setText(mDataset.get(position).getOption());
        holder.mImageView.setImageResource(mDataset.get(position).getIdImage());
        //Etiqueta con el id del elemento actual
        holder.mTextView.setTag(position);


    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mDataset.size();
    }
}