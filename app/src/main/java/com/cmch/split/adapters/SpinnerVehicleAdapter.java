package com.cmch.split.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.cmch.split.R;
import com.cmch.split.dbref.Vehicle;

import java.util.List;

/**
 * Created by alex on 27/01/18.
 */

public class SpinnerVehicleAdapter extends ArrayAdapter<Vehicle> {

    Context context;
    List<Vehicle> vehicles;
    LayoutInflater layoutInflater;

    public SpinnerVehicleAdapter(Context context, List<Vehicle> values){
        super(context, android.R.layout.simple_spinner_item, values);
        this.context = context;
        this.layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.vehicles = values;
    }

    @Override
    public int getCount(){
        return vehicles.size();
    }

    @Override
    public Vehicle getItem(int position){
        return vehicles.get(position);
    }

    @Override
    public long getItemId(int position){
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup viewGroup){
        if(convertView==null){
            convertView = layoutInflater.inflate(R.layout.spinner_item, viewGroup, false);
        }
        TextView textView = convertView.findViewById(R.id.spinner_item);
        textView.setText(vehicles.get(position).getBrand()+": "+vehicles.get(position).getPlates());
        return textView;
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup viewGroup){
        if(convertView==null){
            convertView = layoutInflater.inflate(R.layout.spinner_dropdown_item, viewGroup, false);
        }
        TextView textView = convertView.findViewById(R.id.spinner_dropdown_item);
        textView.setText(vehicles.get(position).getBrand()+": "+vehicles.get(position).getPlates());
        return textView;
    }

}
