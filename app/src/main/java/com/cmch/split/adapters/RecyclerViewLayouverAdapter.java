package com.cmch.split.adapters;

/**
 * Created by CC on 13-Mar-18.
 */

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.cmch.split.R;
import com.cmch.split.TabPublishNewTrip;
import com.cmch.split.dbref.PointLocation;
import com.cmch.split.tools.SplitTools;

import java.util.ArrayList;


public class RecyclerViewLayouverAdapter extends RecyclerView.Adapter<RecyclerViewLayouverAdapter.ViewHolder> {
    private static ArrayList<PointLocation> mDataset;


    public static class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public TextView mTextView;

        public ViewHolder(View v) {
            super(v);
            mTextView = v.findViewById(R.id.recyler_Layouver_scheduleNew_textview_item);
            mTextView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                }
            });
        }
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public RecyclerViewLayouverAdapter(ArrayList<PointLocation> myDataset) {
        mDataset = myDataset;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public RecyclerViewLayouverAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        View v = TabPublishNewTrip.getInflater().inflate(R.layout.recycler_schedule_new_layouvers_item, parent, false);

        return new ViewHolder(v);
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        String shortAddress = SplitTools.getShortAddress(mDataset.get(position).getAddress());
        holder.mTextView.setText(shortAddress);
        //Etiqueta con el id del elemento actual
        holder.mTextView.setTag(position);


    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mDataset.size();
    }
}