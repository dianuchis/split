package com.cmch.split.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.cmch.split.R;

/**
 * Created by alex on 27/01/18.
 */

public class SpinnerStringAdapter extends BaseAdapter {

    Context context;
    String [] items;
    LayoutInflater layoutInflater;

    public SpinnerStringAdapter(Context context, String[] values){
        items = new String [0];
        this.context = context;
        this.layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.items = values;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup viewGroup){
        if(convertView==null){
            convertView = layoutInflater.inflate(R.layout.spinner_item, viewGroup, false);
        }
        TextView textView = convertView.findViewById(R.id.spinner_item);
        textView.setText(this.getItem(position));

        return convertView;
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup viewGroup){
        if(convertView==null){
            convertView = layoutInflater.inflate(R.layout.spinner_dropdown_item, viewGroup, false);
        }
        TextView textView = convertView.findViewById(R.id.spinner_dropdown_item);
        textView.setText(this.getItem(position));

        return convertView;
    }

    @Override
    public int getCount(){ return items.length; }

    @Override
    public String getItem(int position){ return items[position]; }

    @Override
    public long getItemId(int position){
        return position;
    }
}