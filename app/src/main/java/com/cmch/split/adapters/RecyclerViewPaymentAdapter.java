package com.cmch.split.adapters;

/**
 * Created by CC on 13-Mar-18.
 */

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.cmch.split.R;
import com.cmch.split.TabProfileOptions;
import com.cmch.split.dbref.PaymentMethod;

import java.util.List;


public class RecyclerViewPaymentAdapter extends RecyclerView.Adapter<RecyclerViewPaymentAdapter.ViewHolder> {
    private static List<PaymentMethod> mDataset;

  // private static int pos;


    public static class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public ImageView mImageView;
        public TextView mTextView;

        public ViewHolder(View v) {
            super(v);
            mTextView = v.findViewById(R.id.recyler_profile_payment_method_textview_item);
            mImageView = v.findViewById(R.id.recyler_profile_payment_method_image_item);
        }
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public RecyclerViewPaymentAdapter(List<PaymentMethod> myDataset) {
        mDataset = myDataset;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public RecyclerViewPaymentAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        View v = TabProfileOptions.getInflater().inflate(R.layout.recycler_profile_payment_method_item, parent, false);

//        if(textView.getParent()!=null)
//            ((ViewGroup)textView.getParent()).removeView(textView);
//            ((ViewGroup)imageView.getParent()).removeView(imageView);// <- fix

        return new ViewHolder(v);
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        holder.mTextView.setText(mDataset.get(position).getCardNumber());
        holder.mImageView.setImageResource(mDataset.get(position).getIdImagePayMethod());
        //Etiqueta con el id del elemento actual
        holder.mTextView.setTag(position);

    }

    @Override
    public void onViewRecycled(RecyclerViewPaymentAdapter.ViewHolder holder) {
        super.onViewRecycled(holder);
        holder.setIsRecyclable(true);

    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mDataset.size();
    }
}