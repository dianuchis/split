package com.cmch.split.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.cmch.split.ActivityAddVehicleSumary;
import com.cmch.split.R;
import com.cmch.split.dbref.Vehicle;

import java.util.List;

/**
 * Created by CC on 02-Mar-18.
 */

public class CardViewVehicleAdapter extends RecyclerView.Adapter<CardViewVehicleAdapter.ViewHolder> {
    private List<Vehicle> dataset;

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    static class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        TextView textViewHeader;
        TextView textViewModel;
        TextView textViewYear;
        TextView textViewPlates;
        TextView textViewColor;
        TextView textViewSerial;
        TextView textViewPolicy;
        TextView textViewState;
        TextView textViewCompany;
        TextView textViewSeats;
        TextView button_AddVehicle_Sumary_Delete;



        ViewHolder(View v) {
            super(v);
            textViewHeader = v.findViewById(R.id.txView_register_sumary_header);
            textViewModel = v.findViewById(R.id.txView_register_sumary_model);
            textViewYear = v.findViewById(R.id.txView_register_sumary_year);
            textViewPlates = v.findViewById(R.id.txView_register_sumary_plates);
            textViewColor = v.findViewById(R.id.txView_register_sumary_color);
            textViewSerial = v.findViewById(R.id.txView_register_sumary_serial);
            textViewPolicy = v.findViewById(R.id.txView_register_sumary_policy);
            textViewState = v.findViewById(R.id.txView_register_sumary_state);
            textViewCompany = v.findViewById(R.id.txView_register_sumary_company);
            textViewSeats = v.findViewById(R.id.txView_register_sumary_seats);
            button_AddVehicle_Sumary_Delete = v.findViewById(R.id.button_AddVehicle_Sumary_Delete);
        }
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public CardViewVehicleAdapter(List<Vehicle> myDataset) {
        dataset = myDataset;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public CardViewVehicleAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                   int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_view_add_vehicle_sumary, parent, false);

        return new ViewHolder(v);
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        holder.textViewHeader.setText(dataset.get(position).getBrand());
        holder.textViewModel.setText(dataset.get(position).getModel());
        holder.textViewYear.setText(dataset.get(position).getYear());
        holder.textViewPlates.setText(dataset.get(position).getPlates());
        holder.textViewColor.setText(dataset.get(position).getColor());
        holder.textViewSerial.setText(dataset.get(position).getSerialNumber());
        holder.textViewPolicy.setText(dataset.get(position).getInsurancePolicyNumber());
        holder.textViewState.setText(dataset.get(position).getState());
        holder.textViewCompany.setText(dataset.get(position).getInsuranceCompany());
        holder.textViewSeats.setText(dataset.get(position).getAvailableSeats());
        holder.button_AddVehicle_Sumary_Delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (v.getId()){
                    case R.id.button_AddVehicle_Sumary_Delete:
                        deleteVehicle(position);
                        break;
                }

            }
        });

    }

    @Override
    public void onViewRecycled(ViewHolder holder) {
        super.onViewRecycled(holder);
        holder.setIsRecyclable(true);
    }

    private void deleteVehicle(int position){
        dataset.remove(position);
        //Actualizamos la el reciclerView
        ActivityAddVehicleSumary.getCardViewVehicleAdapter().notifyItemRemoved(position);
        //Notificamos que cambio el dataset
        ActivityAddVehicleSumary.getCardViewVehicleAdapter().notifyDataSetChanged();
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return dataset.size();
    }
}