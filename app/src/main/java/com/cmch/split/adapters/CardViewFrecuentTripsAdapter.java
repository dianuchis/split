package com.cmch.split.adapters;

import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.cmch.split.ActivityScheduleTrip;
import com.cmch.split.FragScheduleFrecuentTripNewSchedule;
import com.cmch.split.R;
import com.cmch.split.dbref.FrecuentTrip;
import com.cmch.split.tools.SplitTools;

import java.util.ArrayList;

/**
 * Created by CC on 02-Mar-18.
 */

public class CardViewFrecuentTripsAdapter extends RecyclerView.Adapter<CardViewFrecuentTripsAdapter.ViewHolder> {
    private static ArrayList<FrecuentTrip> dataset;


    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    static class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        TextView textViewOrigin;
        TextView textViewDestin;
        TextView textViewHeader;
        TextView textViewPrice;

        ViewHolder(View v) {
            super(v);

            textViewOrigin = v.findViewById(R.id.textView_Schedule_FrecuentTrips_Origin);
            textViewDestin = v.findViewById(R.id.textView_Schedule_FrecuentTrips_Destin);
            textViewHeader = v.findViewById(R.id.textView_Schedule_FrecuentTrips_Header);
            textViewPrice = v.findViewById(R.id.textView_Schedule_FrecuentTrips_Price);

            //Evento que muestra los datos del viaje frecuente seleccionado en un nuevo fragmento.
            v.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(final View v) {
                    FragmentTransaction transaction = ActivityScheduleTrip.getFm()
                            .beginTransaction();

                    //Reemplazamos el fragmento de viajes frecuentes(RecyclerView con cardViews),
                    //por un fragmento que contiene los datos del viaje frecuente selccionado.
                    transaction.replace(R.id.root_frame_frecuent_trips,
                            FragScheduleFrecuentTripNewSchedule.newInstance(getItem((Integer)textViewHeader.getTag())));

                    //Agregamos una transicion y lo ponemos en la pila de fragments al inicio
                    transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
                    transaction.addToBackStack(null);
                    transaction.commit();
                }
            });
        }

        public FrecuentTrip getItem(int position){
            return dataset.get(position);
        }
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public CardViewFrecuentTripsAdapter(ArrayList<FrecuentTrip> myDataset) {
        dataset = myDataset;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public CardViewFrecuentTripsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                                      int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_card_view_frecuent_trips, parent, false);

         return new ViewHolder(v);
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {

        FrecuentTrip itemFrecTrip = dataset.get(position);

        String origin = SplitTools.getShortAddress(itemFrecTrip.getOrigin().asString());
        String destin = SplitTools.getShortAddress(itemFrecTrip.getDestination().asString());

//        holder.textViewOrigin.setText(origin);
//        holder.textViewDestin.setText(destin);
        holder.textViewHeader.setText(origin+" -"+destin);
        holder.textViewHeader.setTag(position);
       // holder.textViewPrice.setText("$"+itemFrecTrip.getPrice()+"MXN");

    }

    @Override
    public void onViewRecycled(ViewHolder holder) {
        super.onViewRecycled(holder);
        holder.setIsRecyclable(true);
    }

    @Override
    public long getItemId(int position) {
        return super.getItemId(position);

    }


    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return dataset.size();
    }

}

