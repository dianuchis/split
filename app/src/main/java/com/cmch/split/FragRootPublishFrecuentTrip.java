package com.cmch.split;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * Created by CC on 05-Mar-18.
 */

public class FragRootPublishFrecuentTrip extends Fragment {
    private static final String TAG = "FragRootScheduleNewTrip";



    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
		/* Inflate the layout for this fragment */
        View view = inflater.inflate(R.layout.frag_root_publish_frecuent_trips, container, false);

        FragmentTransaction transaction = getFragmentManager()
                .beginTransaction();

		 /* When this container fragment is created, we fill it with our first
		 * "real" fragment*/

        transaction.replace(R.id.root_frame_publish_frecuent_trips, new TabPublishFrecuentTrips());
        transaction.commit();

        return view;
    }

}
