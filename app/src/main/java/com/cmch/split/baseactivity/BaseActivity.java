package com.cmch.split.baseactivity;

import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.view.WindowManager;
import android.widget.Toast;

import com.cmch.split.ActivityHome;
import com.cmch.split.SplitApp;
import com.cmch.split.baseactivity.ActivitySplit;
import com.cmch.split.dbref.User;
import com.cmch.split.tools.FrbValueEventListener;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.StorageReference;


/**
 * Give common methods not in SplitActivity
 */

public class BaseActivity extends AppCompatActivity {

    protected void hideKeyBoard(){
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
    }

    //Carga datos del usuario y los vehiculos que tiene si hubiera, desde firebase.
    protected void loadUserData(){
        //Tratando de obtener la foto
        //codigo devido en varias secciones para mejorar legiilidad

        final FrbValueEventListener vehiclesValueListener = new FrbValueEventListener() {
            @Override
            public void onDateChangeOrError(DataSnapshot dataSnapshot, DatabaseError databaseError) {
                if (databaseError == null){
                    ActivitySplit.dataSnapshotVehicles = dataSnapshot;
                    startActivity(new Intent(getApplicationContext(), ActivityHome.class));
                    finish();
                }
            }
        };


        final FrbValueEventListener userValueEventListener = new FrbValueEventListener() {
            @Override
            public void onDateChangeOrError(DataSnapshot dataSnapshot, DatabaseError databaseError) {
                if (databaseError == null){
                    ActivitySplit.tmpUser = dataSnapshot.getValue(User.class);
                    DatabaseReference vehiclesDbReference = SplitApp.getFrbDatabase().child("vehicles");
                    Query vehiclesQuery =  vehiclesDbReference.orderByChild("owner").equalTo(SplitApp.getFrbUserId());
                    vehiclesQuery.addListenerForSingleValueEvent(vehiclesValueListener);
                } else{
                    Toast.makeText(getApplicationContext(), "Error on load data, please try again", Toast.LENGTH_LONG).show();
                }

            }
        };


        StorageReference userStorageReference = SplitApp.getFirebaseStorage().getReference().child("documents");
        Task<Uri> downloadTask =  userStorageReference.child(SplitApp.getFrbUserId()).child("photo.jpg").getDownloadUrl();
        downloadTask.addOnCompleteListener(new OnCompleteListener<Uri>() {
            @Override
            public void onComplete(@NonNull Task<Uri> task) {
                if (task.isSuccessful()){
                    ActivitySplit.uriProfilePhoto = task.getResult();
                }
                String userId = SplitApp.getFrbUserId();
                final DatabaseReference userDbReference = SplitApp.getFrbDatabase().child("/users").child(userId);
                userDbReference.addListenerForSingleValueEvent(userValueEventListener);

            }
        });


    }

}
