package com.cmch.split.baseactivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.util.Log;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.cmch.split.ActivityAbout;
import com.cmch.split.ActivityFaq;
import com.cmch.split.ActivityHome;
import com.cmch.split.ActivityLogin;
import com.cmch.split.ActivityMyTrips;
import com.cmch.split.ActivityProfile;
import com.cmch.split.ActivityPublishTrips;
import com.cmch.split.ActivityScheduleTrip;
import com.cmch.split.ActivityTripInProgress;
import com.cmch.split.R;
import com.cmch.split.SplitApp;
import com.cmch.split.dbref.User;
import com.cmch.split.splashscreen.ActivityInitSplit;
import com.cmch.split.tools.FrbValueEventListener;
import com.cmch.split.tools.SplitTools;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

public class ActivitySplit extends BaseActivity implements NavigationView.OnNavigationItemSelectedListener {

    DrawerLayout drawer;
    ActionBarDrawerToggle toggle;

    public static DataSnapshot dataSnapshotVehicles;

    //Usado para cargar la imagen y almacenarla como bitmap
    public static Uri uriProfilePhoto;

    //Usado para almacenar almacenar los datos del usuario en el sidebar
    public static User tmpUser;

    //Usado para almacenar mostrar los datos del usuario en el sidebar
    private ImageView imView_PersonalInf_PhotoProfile;
    private TextView textViewNameSideBar;
    private TextView textViewEmailSideBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_split);
    }

    protected void initToolBarAndDrawer(int toolbarID, int drawerID, int navigationViewID){
        Toolbar toolbar = findViewById(toolbarID);
        setSupportActionBar(toolbar);

        drawer = findViewById(drawerID);
        toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = findViewById(navigationViewID);
        navigationView.setNavigationItemSelectedListener(this);


        imView_PersonalInf_PhotoProfile = navigationView.getHeaderView(0).findViewById(R.id.image_sidebar_profilePhoto);
        //  Log.d(TAG, "foto"+ uriProfilePhoto+"");
        Picasso.get().load(uriProfilePhoto).transform(SplitTools.newInstaceCircleTransform()).into(imView_PersonalInf_PhotoProfile);
        textViewEmailSideBar = navigationView.getHeaderView(0).findViewById(R.id.textView_sidebar_profileTextEmail);
        textViewNameSideBar = navigationView.getHeaderView(0).findViewById(R.id.textView_sidebar_profileTextName);
        textViewNameSideBar.setText(SplitTools.getShortName(tmpUser.getFullName()));
        textViewEmailSideBar.setText(tmpUser.getEmail());
    }

    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.nav_home:
                Intent intentHome = new Intent(SplitApp.getAppContext(),ActivityHome.class);
                startActivity(intentHome);
                break;

            case R.id.nav_Schedule:
                Intent intentSchedule = new Intent(SplitApp.getAppContext(), ActivityScheduleTrip.class);
                startActivity(intentSchedule);
                break;

            case R.id.nav_PublishTrip:

                if(!(dataSnapshotVehicles == null)){
                    Intent intentPublish = new Intent(SplitApp.getAppContext(), ActivityPublishTrips.class);
                    startActivity(intentPublish);
                }else{
                    Toast.makeText(this, getString(R.string.Msg_SideBar_LoadCar), Toast.LENGTH_LONG).show();
                }

                break;

            case R.id.nav_MyTrips:
                Intent intentMyTrips = new Intent(SplitApp.getAppContext(), ActivityMyTrips.class);
                startActivity(intentMyTrips);
                break;

            case R.id.nav_profile:
                Intent intentMyProfile = new Intent(SplitApp.getAppContext(), ActivityProfile.class);
                startActivity(intentMyProfile);
                break;

            case R.id.nav_about:
                Intent t= new Intent(SplitApp.getAppContext(), ActivityTripInProgress.class);
                startActivity(t);
                break;

            case R.id.menuitem_SideBar_Logout:
                SplitApp.getFrbAuth().signOut();
                Intent intentLogin = new Intent(SplitApp.getAppContext(), ActivityLogin.class);
                startActivity(intentLogin);
                finish();
                break;

            case R.id.menuItem_FAQ:
                Intent intentFaq = new Intent(SplitApp.getAppContext(), ActivityFaq.class);
                startActivity(intentFaq);
                break;
        }

        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

}
