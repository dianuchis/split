package com.cmch.split.services;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.os.Binder;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.cmch.split.ActivityTripInProgress;
import com.cmch.split.R;
import com.cmch.split.SplitApp;
import com.cmch.split.tools.FineLocationNeeds;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.model.LatLng;


/**
 * Created by CC on 09-Apr-18.
 */

public class LocationService extends Service implements GoogleApiClient.ConnectionCallbacks {

    private static final String TAG = "LocationService";

    private static final String CHANNEL_ID = "2";
    private NotificationManager notificationManager;
    private NotificationCompat.Builder mBuilder;



    private static LatLng latLng;
    private static GoogleApiClient mGoogleApiClient;
    private LocationManager locationManager;


    public class ServiceBinder extends Binder{

        public LocationService getServiceLocation(){
            return LocationService.this;
        }
    }

    //Clase que devuelve el servicio de localizacion
    private IBinder binder = new ServiceBinder();


    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return binder;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        //Ejecutado cuando termina el servicio de localizacion actual
        Log.i(TAG, "Service destroyed");

        //Quita la barra de notificacion
        notificationManager.cancel(17);

    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {


        //Ejecutado cuando se inicia el servicio de localizacion de ubicacion actual
        Log.i(TAG, "Service started, thread id: "+Thread.currentThread().getId());

        //Inicializa la ApiClient de Google para conectar a los servicios de ubicacion actual
        if (mGoogleApiClient == null || !mGoogleApiClient.isConnected()){
            buildGoogleApiClient();
            mGoogleApiClient.connect();
        }

        locationManager = SplitApp.getLocationManager();


        //Inicializa la barra de notificacion mientras el servicio se ejecuta
        mBuilder = new NotificationCompat.Builder(this, CHANNEL_ID)
                .setSmallIcon(R.mipmap.ic_car)
                .setContentTitle(getString(R.string.lbl_TripProgress_Notification_Title))
                .setContentText(getString(R.string.lbl_TripProgress_Notification_textContent))
                .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                .setColor(getResources().getColor(R.color.colorPrimary));

        // Register the channel with the system
        notificationManager = ((NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE));

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {

            // Create the NotificationChannel, but only on API 26+ because
            // the NotificationChannel class is new and not in the support library
            CharSequence name = getString(R.string.notification_channel_name);
            String description = getString(R.string.notification_channel_description);
            int importance = NotificationManager.IMPORTANCE_HIGH;
            NotificationChannel channel = new NotificationChannel(CHANNEL_ID, name, importance);
            channel.setDescription(description);

            // Register the channel with the system
            notificationManager.createNotificationChannel(channel);
        }

        // Create an explicit intent for an Activity in your app
        // Abrimos el intent que inicializo el servicio
        Intent intentTripInProgress = new Intent(this, ActivityTripInProgress.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intentTripInProgress, 0);

        //Set pending intent to Notification Compat
        mBuilder.setContentIntent(pendingIntent);
        mBuilder.setAutoCancel(true);

        // notificationId is a unique int for each notification that you must define
        //Muestra la barra de notificacion.
        notificationManager.notify(17, mBuilder.build());


        //Si se cierra el servicio, el sistema crea nuevamente el servicio
        //y llama a onStarCommand con la última intent que se entrego al servicio.
        //
        return START_REDELIVER_INTENT;
    }

    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient
                .Builder(this)
                .addConnectionCallbacks(this)
                .addApi(LocationServices.API)
                .build();
    }


    private LocationListener locationListener = new LocationListener() {
        @Override
        public void onLocationChanged(Location location) {

            latLng = new LatLng(location.getLatitude(), location.getLongitude());

            //Actualiza el contenido de la notification bar
            //mBuilder.setContentText(" Current Location: " + latLng.latitude + " " + latLng.longitude);
            //notificationManager.notify(17, mBuilder.build());

            Log.i(TAG, "Thread id: "+Thread.currentThread().getId()+" Current Location: " + latLng.latitude + " " + latLng.longitude);

        }
    };

    public static LatLng getLatLng(){
        return latLng;
    }


    @Override
    public void onConnected(@Nullable Bundle bundle) {
        new FineLocationNeeds() {
            @Override
            public void toDo() {
                //LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, locationListener);
                LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, SplitApp.getLocationRequest(), locationListener);
            }
        }.execute(this);
    }

    @Override
    public void onConnectionSuspended(int i) {}
}
