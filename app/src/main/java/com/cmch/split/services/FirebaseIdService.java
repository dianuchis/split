package com.cmch.split.services;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;

import com.cmch.split.SplitApp;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

public class FirebaseIdService extends FirebaseInstanceIdService {

    private final static String TAG = "FirebaseServices";

    @Override
    public void onTokenRefresh() {
        // Get updated InstanceID token.
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        Log.d(TAG, "Refreshed token: " + refreshedToken);


        // If you want to send messages to this application instance or
        // manage this apps subscriptions on the server side, send the
        // Instance ID token to your app server.

        if(SplitApp.getFrbAuth().getCurrentUser() != null){
            sendRegistrationToServer(refreshedToken);
        }
    }

    private void sendRegistrationToServer(String token){
        //Accedemos hasta la rama de usuarios y buscamos al usuario actual
        //con la propiedad fcmRegisterToken donde guardaremos el token
        //para enviar mensajes a este dispositivo posteriorment
        //Nota: si la rama no existe crea una nueva rama.

        DatabaseReference addRegisterToken = SplitApp.getFrbDatabase().child("users").
                child(SplitApp.getFrbUserId()).child("fcmRegisterToken/");
        addRegisterToken.setValue(token);

    }

}
