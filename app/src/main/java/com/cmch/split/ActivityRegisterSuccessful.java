package com.cmch.split;

import android.app.ActivityOptions;
import android.content.Intent;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.cmch.split.splashscreen.ActivityInitSplit;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ActivityRegisterSuccessful extends AppCompatActivity {


    @BindView(R.id.btn_RegisterSuccessful_Start) Button btn_RegisterSuccessful_Start;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_successful);

        //Enlaza las vistas declaradas en la parte de arriba con el activity
        ButterKnife.bind(this);

        //Colorea a verde la barra de notificacion
        //si es una version igual o mayor a KitKat
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            getWindow().setStatusBarColor(getResources().getColor(R.color.colorPrimary));
        }

    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @OnClick(R.id.btn_RegisterSuccessful_Start)
    public void showActivityHome(View view) {
        if(view.getId() == R.id.btn_RegisterSuccessful_Start){
            startActivity(new Intent(this, ActivityInitSplit.class),
                    ActivityOptions.makeSceneTransitionAnimation(this).toBundle());
        }
    }
}
