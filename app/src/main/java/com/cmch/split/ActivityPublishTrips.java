package com.cmch.split;

import android.content.Context;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.util.AttributeSet;
import android.view.View;

import com.cmch.split.baseactivity.ActivitySplit;
import com.cmch.split.dbref.Trip;

public class ActivityPublishTrips extends ActivitySplit implements View.OnClickListener {

    private static FragmentManager fragmentManager;

    private SectionsPagerAdapter mSectionsPagerAdapter;
    public static int GET_ORIGIN_LOCATION = 0;
    public static int GET_DESTINATION_LOCATION = 1;
    public static int GET_LAYOVER_LOCATION = 2;
    private static Trip toPublish;
    private ViewPager mViewPager;


    @Override
    public View onCreateView(String name, Context context, AttributeSet attrs) {
        return super.onCreateView(name, context, attrs);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_screen_publish_trips);

        initToolBarAndDrawer(R.id.toolbar_publish, R.id.drawer_layout_PublishTrips, R.id.nav_view_publish);

        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);

        mViewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.addOnTabSelectedListener(new TabLayout.ViewPagerOnTabSelectedListener(mViewPager));

//        imView_PersonalInf_PhotoProfile = navigationView.getHeaderView(0).findViewById(R.id.image_sidebar_profilePhoto);
//        //  Log.d(TAG, "foto"+ uriProfilePhoto+"");
//        Picasso.get().load(uriProfilePhoto).transform(SplitTools.newInstaceCircleTransform()).into(imView_PersonalInf_PhotoProfile);
//        textViewEmailSideBar = navigationView.getHeaderView(0).findViewById(R.id.textView_sidebar_profileTextEmail);
//        textViewNameSideBar = navigationView.getHeaderView(0).findViewById(R.id.textView_sidebar_profileTextName);
//        textViewNameSideBar.setText(SplitTools.getShortName(tmpUser.getFullName()));
//        textViewEmailSideBar.setText(tmpUser.getEmail());
    }


    /*
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_screen_publish_trips, menu);
        return true;
    }
    */



    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
            fragmentManager = fm;
        }

        @Override
        public Fragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.
            // Return a PlaceholderFragment (defined as a static inner class below).
            switch (position) {
                case 0:
                    TabPublishNewTrip tabNew = new TabPublishNewTrip();
                    return tabNew;
                case 1:
                    return new FragRootPublishFrecuentTrip();
                default:
                    return null;
            }
        }

        @Override
        public int getCount() {
            // Show 2 total pages.
            return 2;
        }

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {

        }

    }

    public static FragmentManager getFm(){return fragmentManager;}

    public static Trip getToPublish() {
        return toPublish;
    }

    public static void setToPublish(Trip toPublish) {
        ActivityPublishTrips.toPublish = toPublish;
    }
}
