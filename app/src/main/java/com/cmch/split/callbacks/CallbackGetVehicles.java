package com.cmch.split.callbacks;

import com.google.firebase.database.DataSnapshot;

/**
 * Created by CC on 06-Mar-18.
 */

public interface CallbackGetVehicles {
    void onSuccess(DataSnapshot dataSnapshot);
    void onFail(String msg);
}
