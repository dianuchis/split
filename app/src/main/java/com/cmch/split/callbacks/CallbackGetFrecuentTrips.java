package com.cmch.split.callbacks;

import com.cmch.split.dbref.FrecuentTrip;

import org.json.JSONException;

import java.util.ArrayList;

/**
 * Created by CC on 06-Mar-18.
 */

public interface CallbackGetFrecuentTrips {
    void onSuccess(ArrayList<FrecuentTrip> frecuentTrips) throws JSONException;
    void onFail(String msg);
}
