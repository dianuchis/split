package com.cmch.split.callbacks;

/**
 * Created by CC on 06-Mar-18.
 */

public interface CallbackGetSeats {
    void onSuccess(int seats);
    void onFail(String msg);
}
