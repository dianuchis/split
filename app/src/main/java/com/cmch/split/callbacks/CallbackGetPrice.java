package com.cmch.split.callbacks;

/**
 * Created by CC on 06-Mar-18.
 */

public interface CallbackGetPrice {
    void onSuccess(String price);
    void onFail(String msg);
}
