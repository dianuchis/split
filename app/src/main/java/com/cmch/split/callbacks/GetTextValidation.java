package com.cmch.split.callbacks;

import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by CC on 06-Mar-18.
 */

public interface GetTextValidation {
    void onSuccess(ArrayList<TextView> field);
    void onFail(String msg);
}
