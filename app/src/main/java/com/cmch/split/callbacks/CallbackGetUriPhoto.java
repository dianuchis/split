package com.cmch.split.callbacks;

import android.net.Uri;

/**
 * Created by CC on 06-Mar-18.
 */

public interface CallbackGetUriPhoto {
    void onSuccess(Uri uriPhoto);
    void onFail(String msg);
}
