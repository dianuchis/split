package com.cmch.split.callbacks;

import com.cmch.split.dbref.Trip;

import java.util.ArrayList;

/**
 * Created by CC on 06-Mar-18.
 */

public interface GetAvailableTrips {
    void onSuccess(ArrayList<Trip> availableTrips);
    void onFail(String msg);
}
