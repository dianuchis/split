package com.cmch.split.callbacks;

import com.cmch.split.dbref.User;

/**
 * Created by CC on 06-Mar-18.
 */

public interface CallbackGetUser {
    void onSuccess(User user);
    void onFail(String msg);
}
