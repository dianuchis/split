package com.cmch.split;

import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.location.LocationManager;
import android.net.Uri;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.cmch.split.adapters.SpinnerImageTextAdapter;
import com.cmch.split.dbref.Country;
import com.cmch.split.dbref.Insurance;
import com.cmch.split.dbref.User;
import com.cmch.split.dbref.Vehicle;
import com.cmch.split.modelviews.ImageTextItems;
import com.cmch.split.tools.SPJSONObject;
import com.cmch.split.tools.SplitJSONRequest;
import com.cmch.split.tools.SplitTools;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ServerValue;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.gson.JsonArray;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;

/**
 * Created by alex on 01/11/17. Edited on July 18
 */

public class SplitApp extends Application {


    private final static String TAG = "SplitApp";


    private static FirebaseAuth firebaseAuth;
    private static DatabaseReference databaseReference;
    private static LocationRequest locationRequest;
    private static LocationManager locationManager;
    private static FirebaseStorage firebaseStorage;
    private static User appUser;
    private static Uri uriPhoto;


    public static String BASE_URL = "https://us-central1-fir-plit.cloudfunctions.net/app/";
    public static String CATALOGS_URL = BASE_URL + "getcatalogs";


    public static User getAppUser() {
        return appUser;
    }

    public static Uri getUriPhoto() {
        return uriPhoto;
    }

    public static void setAppUser(User appUser) {
        SplitApp.appUser = appUser;
    }

    public static void setUriPhoto(Uri uri) {
        SplitApp.uriPhoto = uri;
    }

    //Agregado
    private static int idTripReserved = 0;

    // Agregado
    private static String appColors;
    private static String[] appResultColors;

    //Agregado
    private static String appBrands;
    private static String[] appResultBrands;

    //Agregado
    private static String appYears;
    private static String[] appResultYears;

    //Agregado
    private static ArrayList<ImageTextItems> appResultCountryCodes;
    private static String[] appResultCountries;
   // private static List<Country> countries = new ArrayList<>();
    private static JSONObject jsonObjectCountries;

    //Agregado
    private static List<String> states = new ArrayList<>();

    //Agregado
    private static List<Insurance> insurances = new ArrayList<>();
    private static List<String> companies = new ArrayList<>();

    private static Context appContext;
    private static Resources resourceSplit;
    public static Resources getResourceSplit() {
        return resourceSplit;
    }

    private static RequestQueue requestQueue;

    private static String language;

    @Override
    public void onCreate(){
        super.onCreate();


        language = Locale.getDefault().getDisplayLanguage().toString().toLowerCase();

        Log.d("Master:Serevro", language);

        locationRequest = new LocationRequest();
        locationRequest.setInterval(3000);
        locationRequest.setFastestInterval(1000);
        locationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
        locationManager = (LocationManager)getSystemService(Context.LOCATION_SERVICE);

        firebaseAuth = FirebaseAuth.getInstance();
        databaseReference = FirebaseDatabase.getInstance().getReference();
        firebaseStorage = FirebaseStorage.getInstance();
        appContext = getApplicationContext();
        requestQueue = Volley.newRequestQueue(appContext);
        resourceSplit = getResources();

        appUser = new User();

        //firebaseAuth.signOut();

        /*
        if(FirebaseInstanceId.getInstance().getToken() != null){
            Log.d("FCM token: ", FirebaseInstanceId.getInstance().getToken());
        }*/

    }



    public static FirebaseAuth getFrbAuth(){
        return  firebaseAuth;
    }

    public static DatabaseReference getFrbDatabase() {return databaseReference;}

    public static FirebaseStorage getFirebaseStorage() {return  firebaseStorage;}

    public static LocationRequest getLocationRequest(){ return locationRequest;}

    public static LocationManager getLocationManager() { return locationManager;}

    public static Context getAppContext(){
        return appContext;
    }

    public static String[] getBrands(){ return appResultBrands; }

    public static String[] getColors(){ return appResultColors; }

    public static String[] getYears(){ return appResultYears; }

    public static String getFrbUserId(){
        String result = "";

        if(firebaseAuth.getCurrentUser() != null)
            result = firebaseAuth.getCurrentUser().getUid();

        return result;
    }

    public static void getFirebaseUser() {

        String userId = getFrbUserId();
        Log.d("id", userId);

        Query queryUser = databaseReference.child("/users").child(getFrbUserId());
        queryUser.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                if(dataSnapshot.exists()){
                    SplitApp.setAppUser( dataSnapshot.getValue(User.class));
                    //Log.d(TAG, dataSnapshot.toString());
                    Log.d(TAG,getAppUser().asString());
                }
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {}
        });

    }

    public static String[] getTypeId(){

        String [] appResultTypeId = new String[4];

        //agregando primer item con mensaje
        appResultTypeId[0] = getResourceSplit().getString(R.string.TypeId_Population);
        appResultTypeId[1] = getResourceSplit().getString(R.string.TypeId_Identification);
        appResultTypeId[2] = getResourceSplit().getString(R.string.TypeId_Licence);
        appResultTypeId[3] = getResourceSplit().getString(R.string.TypeId_Passport);


        return appResultTypeId;
    }

    public static String[] getGenders(){

        String [] appResultGenders = new String[3];

        //agregando primer item con mensaje
        appResultGenders[0] = getResourceSplit().getString(R.string.Gender_Population);
        appResultGenders[1] = getResourceSplit().getString(R.string.Gender_Male);
        appResultGenders[2] = getResourceSplit().getString(R.string.Gender_Female);


        return appResultGenders;
    }

    public static String[] getSeats(){

        String [] appResultSeats = new String[4];

        //agregando primer item con mensaje
        appResultSeats[0] = "1";
        appResultSeats[1] = "2";
        appResultSeats[2] = "3";
        appResultSeats[3] = "4";


        return appResultSeats;
    }

    public static String[] getBaggageTypes(){

        String [] appResultBaggageTypes = new String[4];

        //agregando primer item con mensaje
        appResultBaggageTypes[0] = getResourceSplit().getString(R.string.msg_Publish_BaggageNotPermited);
        appResultBaggageTypes[1] = "Small";
        appResultBaggageTypes[2] = "Medium";
        appResultBaggageTypes[3] = "Large";

        return appResultBaggageTypes;
    }

    public static String[] getBaggageNumber(){

        String [] appResultBaggageNumber = new String[3];

        appResultBaggageNumber[0] = "1";
        appResultBaggageNumber[1] = "2";
        appResultBaggageNumber[2] = "3";


        return appResultBaggageNumber;
    }

    public static String[] getCountries(){
        return appResultCountries;
    }

    public static ArrayList<ImageTextItems> getCountryCodes(){ return appResultCountryCodes; }

    public static String[] getStates(String countryName){
        String[] result = new String[0];

        try{
            JSONArray jsonArrayStates = jsonObjectCountries.getJSONObject(countryName).getJSONArray("states");
            int size = jsonArrayStates.length();
            result = new String[size];
            for(int i = 0; i < size; i++) {
                result[i] = jsonArrayStates.getString(i);
            }
        }catch (JSONException e){
            e.printStackTrace();
        }

        return result;
    }

    //Get Insurance Company List based on country
    public static String[] getInsuranceList(String countryName){
        String[] result = new String[0];

        try{
            JSONArray jsonArrayCompanies = jsonObjectCountries.getJSONObject(countryName).getJSONArray("insurance");
            int size = jsonArrayCompanies.length();
            result = new String[size];
            for(int i = 0; i < size; i++) {
                result[i] = jsonArrayCompanies.getString(i);
            }
        }catch (JSONException e){
            e.printStackTrace();
        }

        return result;
    }

    public static String[] getInsuranceCompanies(String countryName){

        String[] result = new String[0];

        for(Insurance item: insurances){
            if(item.getCountry().equals(countryName))
            {
                companies = item.getCompanies();
                result = new String[companies.size()];

                for(int i = 0; i < companies.size(); i++)
                {
                    result[i] = companies.get(i);
                }
            }
        }

        return result;
    }

    public static void addSplitRequest(SplitJSONRequest splitJSONRequest){
        requestQueue.add(splitJSONRequest.getRequest());
    }


    public static void setCatalogs(JSONObject jsonObject){
        //colors
        try{
            JSONArray jsonArrayColors = jsonObject.getJSONObject("colors").getJSONArray(language);
            int size = jsonArrayColors.length();
            appResultColors = new String[size];
            for(int i = 0; i < size; i++){
                appResultColors[i]=jsonArrayColors.getString(i);
            }
        }catch (JSONException e){
            e.printStackTrace();
        }

        //years
        try{
            JSONArray jsonArrayYears = jsonObject.getJSONArray("years");
            int size = jsonArrayYears.length();
            appResultYears = new String[size];
            for(int i = 0; i < size; i++){
                appResultYears[i] = jsonArrayYears.getString(i);
            }
        }catch (JSONException e){
            e.printStackTrace();
        }

        //brands
        try{
            JSONArray jsonArrayBrands = jsonObject.getJSONArray("brands");
            int size = jsonArrayBrands.length();
            appResultBrands = new String[size];
            for (int i = 0; i < size; i++){
                appResultBrands[i] = jsonArrayBrands.getString(i);
            }
        }catch (JSONException e){
            e.printStackTrace();
        }


        //countries
        try{
            jsonObjectCountries = jsonObject.getJSONObject("countries");
            List<String> countries = new ArrayList<>();
            List<String> codes = new ArrayList<>();
            appResultCountryCodes = new ArrayList<>();
            Iterator<String> iterator = jsonObjectCountries.keys();
            while (iterator.hasNext()){
                String name = iterator.next();
                countries.add(name);
                ImageTextItems tmp = new ImageTextItems();
                String code = jsonObjectCountries.getJSONObject(name).getString("code");
                tmp.setText(code);
                appResultCountryCodes.add(tmp);
            }
            appResultCountries = new String[countries.size()];
            appResultCountries = countries.toArray(appResultCountries);

            appResultCountryCodes.get(0).setImageId(R.mipmap.ic_argentina_flag);
            appResultCountryCodes.get(1).setImageId(R.mipmap.ic_mexico_flag);
            appResultCountryCodes.get(2).setImageId(R.mipmap.ic_eu_flag);


        }catch (JSONException e){
            e.printStackTrace();
        }


    }

}
